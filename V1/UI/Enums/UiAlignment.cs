﻿namespace NimbusFox.KitsuneCore.V1.UI.Enums {
    public enum UiAlignment {
        TopLeft,
        TopCenter,
        TopRight,
        MiddleLeft,
        MiddleCenter,
        MiddleRight,
        BottomLeft,
        BottomCenter,
        BottomRight
    }
}