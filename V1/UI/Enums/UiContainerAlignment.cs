﻿namespace NimbusFox.KitsuneCore.V1.UI.Enums {
    public enum UiContainerAlignment {
        Left,
        Center,
        Right
    }
}