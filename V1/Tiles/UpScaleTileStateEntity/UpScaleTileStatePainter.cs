﻿﻿using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.TileStates;

namespace NimbusFox.KitsuneCore.V1.Tiles.UpScaleTileStateEntity {
    public class UpScaleTileStatePainter : EntityPainter {
        protected override void Dispose(bool disposing) { }

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade,
            int updateSteps) { }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade) { }

        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController,
            Timestep renderTimestep) { }

        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            if (entity.Logic is UpScaleTileStateLogic logic) {
                var drawable = logic.UpScale.GetDrawable();

                var targetMatrix = matrix
                    .Translate(drawable.Offsets)
                    .RotateUnitY(MathHelper.ToRadians(logic.Rotation))
                    .Translate(-drawable.Offsets)
                    .Translate((logic.Location.ToVector3F() + logic.PositionOffset) - renderOrigin.ToVector3F());
                
                drawable.Drawable.Render(graphics, ref targetMatrix);
            }
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}