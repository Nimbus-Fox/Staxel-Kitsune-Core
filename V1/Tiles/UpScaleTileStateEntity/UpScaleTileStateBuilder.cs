﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneCore.V1.Tiles.UpScaleTileStateEntity {
    public class UpScaleTileStateBuilder : ITileStateBuilder {
        public void Dispose() {
            
        }

        public void Load() {
            
        }

        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.kitsuneCore.tileStateBuilder.upScaleTile";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return UpScaleTileStateEntityBuilder.Spawn(location, universe, tile);
        }
    }
}