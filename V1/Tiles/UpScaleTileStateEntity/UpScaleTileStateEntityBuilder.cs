﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneCore.V1.Tiles.UpScaleTileStateEntity {
    public class UpScaleTileStateEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
        public EntityPainter Instance() {
            return new UpScaleTileStatePainter();
        }

        public EntityLogic Instance(Entity entity, bool server) {
            return new UpScaleTileStateLogic(entity);
        }

        public void Load() {
            
        }

        public string Kind => KindCode;
        public static string KindCode => "nimbusfox.kitsuneCore.tileState.upScaleTile";

        public bool IsTileStateEntityKind() {
            return true;
        }
        
        public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, Tile tile) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", KindCode);
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetString("tile", tile.Configuration.Code);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}