﻿using System.Globalization;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V1.Components.Tile;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneCore.V1.Tiles.UpScaleTileStateEntity {
    public class UpScaleTileStateLogic : TileStateEntityLogic {
        public TileConfiguration TileConfig;
        protected bool UpdateClient = true;
        public UpScaleTileComponent UpScale;
        public float Rotation;
        private Blob _constructor;
        public Vector3F PositionOffset;
        
        public UpScaleTileStateLogic(Entity entity) : base(entity) { }
        
        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                EntityId.NullEntityId, out var tile)) {
                if (tile.Configuration.Code != TileConfig.Code) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _constructor = BlobAllocator.Blob(true);
            _constructor.AssignFrom(arguments);
            Entity.Physics.MakePhysicsless();
            Entity.Physics.ForcedPosition(arguments.FetchBlob("location").GetVector3I().ToTileCenterVector3D());
            Location = arguments.FetchBlob("location").GetVector3I();
            TileConfig = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));

            if (TileConfig.Components.Contains<UpScaleTileComponent>()) {
                UpScale = TileConfig.Components.Get<UpScaleTileComponent>();
            }

            Tile tile;

            if (TileConfig.AttachToBelow) {
                if (entityUniverseFacade.ReadTile(Location - new Vector3I(0, 1, 0), TileAccessFlags.SynchronousWait,
                    ChunkFetchKind.LivingWorld, Entity.Id, out tile)) {
                    PositionOffset.Y = tile.Configuration.TopOffset;
                }
            } else if (TileConfig.AttachToAbove) {
                if (entityUniverseFacade.ReadTile(Location + new Vector3I(0, 1, 0), TileAccessFlags.SynchronousWait,
                    ChunkFetchKind.LivingWorld, Entity.Id, out tile)) {
                    PositionOffset.Y = tile.Configuration.BottomOffset;
                }
            }

            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                Entity.Id, out tile)) {
                Rotation = MathHelper.ToDegrees(TileConfig.GetRotationInRadians(tile.Variant()));
            }

            NeedUpdate();
        }

        public override void Bind() { }

        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main,
            ControlState alt) { }

        public override bool CanChangeActiveItem() {
            return false;
        }

        public override bool IsPersistent() {
            return true;
        }

        public override bool IsLingering() {
            return false;
        }

        public override void KeepAlive() { }

        public override void BeingLookedAt(Entity entity) { }

        public override bool IsBeingLookedAt() {
            return false;
        }

        public override void Store() {
            base.Store();

            if (UpdateClient) {
                Entity.Blob.SetString("tile", TileConfig.Code);
                Entity.Blob.FetchBlob("location").SetVector3I(Location);
                Entity.Blob.SetString("rotation", Rotation.ToString(CultureInfo.InvariantCulture));
                Entity.Blob.FetchBlob("positionOffset").SetVector3F(PositionOffset);
                UpdateClient = false;
            }
        }

        public override void Restore() {
            base.Restore();

            if (Entity.Blob.Contains("tile")) {
                TileConfig = GameContext.TileDatabase.GetTileConfiguration(Entity.Blob.GetString("tile"));

                if (TileConfig.Components.Contains<UpScaleTileComponent>()) {
                    UpScale = TileConfig.Components.Get<UpScaleTileComponent>();
                }
            }

            if (Entity.Blob.Contains("location")) {
                Location = Entity.Blob.FetchBlob("location").GetVector3I();
            }

            if (Entity.Blob.Contains("rotation")) {
                if (float.TryParse(Entity.Blob.GetString("rotation"), out var rotation)) {
                    Rotation = rotation;
                }
            }

            if (Entity.Blob.Contains("positionOffset")) {
                PositionOffset = Entity.Blob.FetchBlob("positionOffset").GetVector3F();
            }
        }

        public void NeedUpdate() {
            UpdateClient = true;
        }

        public override void StorePersistenceData(Blob data) {
            base.StorePersistenceData(data);
            
            data.FetchBlob("constructor").AssignFrom(_constructor);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);

            if (data.Contains("constructor")) {
                Construct(data.FetchBlob("constructor"), facade);
            }
            
            NeedUpdate();
        }
    }
}