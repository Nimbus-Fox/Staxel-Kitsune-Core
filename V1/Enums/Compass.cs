﻿namespace NimbusFox.KitsuneCore.V1.Enums {
    public enum Compass {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }
}