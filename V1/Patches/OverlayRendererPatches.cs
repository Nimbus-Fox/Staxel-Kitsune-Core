﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V1.Hooks;
using NimbusFox.KitsuneCore.V1.UI.Classes;
using NimbusFox.KitsuneCore.V2.UI;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Input;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.KitsuneCore.V1.Patches {
    public static class OverlayRendererPatches {
        internal static void Initialize() {
            KitsuneUIHook.PatchController.Add(typeof(OverlayRenderer), "Draw", null, null, typeof(OverlayRendererPatches), "Draw");
            KitsuneUIHook.PatchController.Add(typeof(OverlayRenderer), "DrawTop", typeof(OverlayRendererPatches), "DrawTop");
            KitsuneUIHook.PatchController.Add(typeof(OverlayRenderer), "Update", typeof(OverlayRendererPatches), "Update");
        }

        private static bool _isClicked = false;
        private static bool _isHeld = false;

        private static void Update(Universe universe, AvatarController avatarController) {
            var input = new List<ScanCode>();

            if (ClientContext.InputSource.IsAnyScanCodeDownClick(out _)) {
                input = Helpers.GetAllKeysPressed();
            }

            var interfacePressed = new List<InterfaceLogicalButton>();

            foreach (InterfaceLogicalButton test in Enum.GetValues(typeof(InterfaceLogicalButton))) {
                if (ClientContext.InputSource.IsInterfaceDownClicked(test, false)) {
                    interfacePressed.Add(test);
                }
            }

            var mouseState = ClientContext.InputSource.GetMouseState();

            var mainClick = mouseState.LeftButton == ButtonState.Pressed || mouseState.RightButton == ButtonState.Pressed;

            if (_isClicked && mainClick) {
                _isHeld = true;
            } else {
                _isHeld = false;
            }

            _isClicked = mainClick;

            var remove = new List<UiWindow>();

            foreach (var window in new List<UiWindow>(KitsuneUIHook.Instance.Windows)) {
                window.Update(universe, avatarController, input, ClientContext.InputSource.IsControlKeyDown(),
                    input.Contains(ScanCode.LeftShift) || input.Contains(ScanCode.RightShift), interfacePressed,
                    mouseState.Vector2(), _isClicked, _isHeld);

                if (window.Remove || window.IsDisposed) {
                    remove.Add(window);
                }
            }

            remove.ForEach(x => KitsuneUIHook.Instance.Windows.Remove(x));
            remove.Clear();

            
        }

        private static void Draw(DeviceContext graphics, ref Matrix4F matrix, Entity avatar, Universe universe,
            AvatarController avatarController) {
            InitializeContentManager(graphics);
            var mouseState = ClientContext.InputSource.GetMouseState();
            try {
                foreach (var window in KitsuneUIHook.Instance.Windows) {
                    if (window.Visible && !window.AlwaysOnTop && !window.IsDisposed) {
                        window.Draw(graphics, ref matrix, avatar, universe, avatarController, mouseState.Vector2());
                    }
                }
            } catch (InvalidOperationException) {
                // stop enumeration bugs
            }
        }

        private static void DrawTop(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity avatar,
            EntityPainter avatarPainter, Universe universe, Timestep timestep) {
            InitializeContentManager(graphics);
            var mouseState = ClientContext.InputSource.GetMouseState();
            try {
                foreach (var window in KitsuneUIHook.Instance.Windows) {
                    if (window.Visible && window.AlwaysOnTop && !window.IsDisposed) {
                        window.DrawTop(graphics, ref matrix, avatar, avatarPainter, universe, timestep, mouseState.Vector2());
                    }
                }
            } catch (InvalidOperationException) {
                // stop enumeration bugs
            }
        }

        private static void InitializeContentManager(DeviceContext graphics) {
            if (KitsuneUIHook.Instance.ContentManager == null) {
                KitsuneUIHook.Instance.ContentManager = graphics.GetPrivateFieldValue<ContentManager>("Content");

                KitsuneUIHook.Instance.LoadContent(graphics.Graphics.GraphicsDevice);

                graphics.Graphics.DeviceReset += (sender, args) => {
                    KitsuneUIHook.Instance.ContentManager = null;
                };
            }
        }
    }
}
