﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using NimbusFox.KitsuneCore.V1.Hooks;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.KitsuneCore.V1.Patches {
    internal static class BlobResourceLoaderPatches {
        public static void Init() {
            KitsuneHook.KitsuneCore.PatchController.Add(typeof(BlobResourceLoader), "FetchBlob", null, null, typeof(BlobResourceLoaderPatches), "FetchBlob");
            KitsuneHook.KitsuneCore.PatchController.Add(typeof(BlobResourceLoader), "InnerFetchBlob", null, null, typeof(BlobResourceLoaderPatches), "InnerFetchBlob");
        }

        private static void FetchBlob(this ResourceManager manager, string resource, ref Blob __result) {
            if (Overrides._Overrides.ContainsKey(resource)) {
                Logger.WriteLine($"[Override API] Running overrides for {resource}");

                var replace = __result.Clone();

                foreach (var overrideFunction in Overrides._Overrides[resource]) {
                    try {
                        overrideFunction(replace);
                    } catch (Exception ex) when (!Debugger.IsAttached) {
                        Logger.LogException(new Exception("This is not a KitsuneCore Error.", ex));
                    }
                }

                replace.Delete("code");

                CleanBlob(__result, replace);
            }
        }

        private static void InnerFetchBlob(string resource, List<KeyValuePair<string, BlobEntry>> spare, ref Blob __result) {
            if (Overrides._Overrides.ContainsKey(resource)) {
                Logger.WriteLine($"[Override API] Running overrides for inherited blob {resource}");

                var replace = __result.Clone();

                foreach (var overrideFunction in Overrides._Overrides[resource]) {
                    try {
                        overrideFunction(replace);
                    } catch (Exception ex) when (!Debugger.IsAttached) {
                        Logger.LogException(new Exception("This is not a KitsuneCore Error.", ex));
                    }
                }

                replace.Delete("code");

                CleanBlob(__result, replace);
            }
        }

        private static void CleanBlob(Blob orig, Blob replace) {
            foreach (var entry in orig.KeyValueIteratable) {
                if (replace.TryGetBlobEntry(entry.Key, out var replaceEntry)) {
                    if (entry.Value.Kind != replaceEntry.Kind) {
                        if (entry.Value.Kind == BlobEntryKind.String ||
                            entry.Value.Kind == BlobEntryKind.List ||
                            entry.Value.Kind == BlobEntryKind.Blob) {
                            continue;
                        }

                        if (entry.Value.Kind == BlobEntryKind.Int) {
                            if (replaceEntry.Kind == BlobEntryKind.String) {
                                orig.SetLong(entry.Key, long.Parse(replaceEntry.GetString()));
                            } else if (replaceEntry.Kind == BlobEntryKind.Float) {
                                orig.SetLong(entry.Key, Convert.ToInt64(replaceEntry.GetDouble()));
                            }
                        } else if (entry.Value.Kind == BlobEntryKind.String) {
                            if (replaceEntry.Kind == BlobEntryKind.Float) {
                                orig.SetString(entry.Key, replaceEntry.GetDouble().ToString(CultureInfo.InvariantCulture));
                            } else if (replaceEntry.Kind == BlobEntryKind.Int) {
                                orig.SetString(entry.Key, replaceEntry.GetLong().ToString());
                            }
                        }
                    } else {
                        if (entry.Value.Kind == BlobEntryKind.Float) {
                            entry.Value.SetDouble(replaceEntry.GetDouble());
                        } else if (entry.Value.Kind == BlobEntryKind.Int) {
                            entry.Value.SetLong(replaceEntry.GetLong());
                        } else if (entry.Value.Kind == BlobEntryKind.String) {
                            entry.Value.SetString(replaceEntry.GetString());
                        } else if (entry.Value.Kind == BlobEntryKind.Bool) {
                            entry.Value.SetBool(replaceEntry.GetBool());
                        } else if (entry.Value.Kind == BlobEntryKind.List) {
                            foreach (var ent in replaceEntry.List()) {
                                entry.Value.List().Add(ent);
                            }
                        } else if (entry.Value.Kind == BlobEntryKind.Blob) {
                            CleanBlob(orig.FetchBlob(entry.Key), replace.FetchBlob(entry.Key));
                        }
                    }
                }
            }
        }
    }
}
