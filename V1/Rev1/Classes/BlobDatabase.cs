﻿using System;
using System.IO;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Rev1.Classes {
    public class BlobDatabase : V1.Classes.BlobDatabase {

        internal BlobDatabase() {

        }

        public BlobDatabase(FileStream stream, Action<string> errorLogger) : base(stream, errorLogger, false) {
            Database = BlobAllocator.Blob(true);

            DatabaseFile.Seek(0L, SeekOrigin.Begin);

            if (DatabaseFile.Length == 0) {
                NeedsStore();
                Save();
                return;
            }

            try {
                using (var bfStream = new MemoryStream(Convert.FromBase64String(DatabaseFile.ReadAllText()))) {
                    bfStream.Seek(0L, SeekOrigin.Begin);
                    Database.ReadJson((string)Bf.Deserialize(bfStream));
                }

                DatabaseFile.Seek(0, SeekOrigin.Begin);

                File.WriteAllBytes(DatabaseFile.Name + ".bak", DatabaseFile.ReadAllBytes());
            } catch {
                if (File.Exists(DatabaseFile.Name + ".bak")) {
                    Console.ForegroundColor = ConsoleColor.Red;
                    errorLogger($"{DatabaseFile.Name} was corrupt. Will revert to backup file");
                    Console.ResetColor();

                    using (var ms = new MemoryStream(File.ReadAllBytes(DatabaseFile.Name + ".bak"))) {
                        ms.Seek(0L, SeekOrigin.Begin);
                        using (var bfStream = new MemoryStream(Convert.FromBase64String(ms.ReadAllText()))) {
                            bfStream.Seek(0L, SeekOrigin.Begin);
                            Database.ReadJson((string)Bf.Deserialize(bfStream));
                        }
                    }
                }
            }

            NeedsStore();
            Save();
        }

        internal override void ForceSave() {
            DatabaseFile.SetLength(0);
            DatabaseFile.Position = 0;
            DatabaseFile.Flush(true);

            using (var stream = new MemoryStream()) {
                Bf.Serialize(stream, Database.ToString());
                stream.Seek(0L, SeekOrigin.Begin);
                DatabaseFile.WriteString(Convert.ToBase64String(stream.ToArray()));
            }

            DatabaseFile.Flush(true);
            _needsStore = false;
        }
    }
}
