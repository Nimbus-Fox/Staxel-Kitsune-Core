﻿using System;
using System.Collections.Generic;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Desealed.Staxel.EntityActions;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Effects;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.KitsuneCore.V1.Desealed.Staxel.Items {
    public class CraftItem : Item {
        private List<CraftItem> _contains;
        public bool DecomposeOnPickup;

        public CraftItem(IItemBuilderHelper builder)
          : base(builder.Kind()) {
            this.Builder = builder;
        }

        public IItemBuilderHelper Builder { get; private set; }

        public List<CraftItem> Contains {
            get {
                if (this._contains == null)
                    this._contains = new List<CraftItem>();
                return this._contains;
            }
        }

        public bool HasContains() {
            if (this._contains != null)
                return this._contains.Count > 0;
            return false;
        }

        public int ContainsCount() {
            if (this._contains == null)
                return 0;
            return this._contains.Count;
        }

        public override void Update(
          Entity entity,
          Timestep step,
          EntityUniverseFacade entityUniverseFacade) {
        }

        public CraftItem FindEdible() {
            if (this.Configuration.IsEdible)
                return this;
            if (this.HasContains()) {
                foreach (CraftItem contain in this.Contains) {
                    if (contain.FindEdible() != null)
                        return contain;
                }
            }
            return (CraftItem)null;
        }

        public override void Control(
          Entity entity,
          EntityUniverseFacade facade,
          ControlState main,
          ControlState alt) {
            if (!main.DownClick && !alt.DownClick)
                return;
            PlayerEntityLogic playerEntityLogic = entity.PlayerEntityLogic;
            Vector3I position;
            Vector3I adjacent;
            if (playerEntityLogic.ItemFacade.LookingAtTile(out position, out adjacent)) {
                if (facade.FetchTileStateEntityLogic(position, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId) is DockTileStateEntityLogic) {
                    if (!main.DownClick)
                        return;
                    if (this.FindEdible() != null && (playerEntityLogic.SelectedReaction == null || playerEntityLogic.SelectedReaction.TriggerDurationCraftManually))
                        entity.Logic.ActionFacade.NextAction(this.FindEdible().Configuration.IsDrink ? DrinkEntityAction.KindCode() : EatEntityAction.KindCode());
                    else
                        BaseEffects.PlaySound(entity, "staxel.sound.Fail", -1.0);
                } else {
                    if (!main.DownClick || this.FindEdible() == null)
                        return;
                    entity.Logic.ActionFacade.NextAction(this.FindEdible().Configuration.IsDrink ? DrinkEntityAction.KindCode() : EatEntityAction.KindCode());
                }
            } else {
                if (!main.DownClick || this.FindEdible() == null)
                    return;
                entity.Logic.ActionFacade.NextAction(this.FindEdible().Configuration.IsDrink ? DrinkEntityAction.KindCode() : EatEntityAction.KindCode());
            }
        }

        public override int GetHashCode() {
            return this.Configuration.GetHashCode();
        }

        public override bool PlacementTilePreview(
          AvatarController avatar,
          Entity entity,
          Universe universe,
          Vector3IMap<Tile> previews) {
            return false;
        }

        public override bool Same(Item item) {
            CraftItem craftItem1 = item as CraftItem;
            if (craftItem1 == null || this.Configuration != craftItem1.Configuration || this.ContainsCount() != craftItem1.ContainsCount())
                return false;
            if (this.HasContains()) {
                foreach (CraftItem contain in this.Contains) {
                    CraftItem craftItem = contain;
                    if (!craftItem1.Contains.Exists((Predicate<CraftItem>)(x => x.Same((Item)craftItem))))
                        return false;
                }
            }
            return true;
        }

        public override bool MatchesSpecification(Blob blob) {
            if (!blob.IsMaster())
                throw new Exception();
            if (blob.Contains("kind") && blob.GetString("kind") != this.Kind || blob.GetString("code") != this.Configuration.Code)
                return false;
            if (!blob.Contains("specification") || !blob.FetchBlob("specification").GetBool("wildcardContains", false)) {
                if (blob.Contains("contains")) {
                    List<BlobEntry> blobEntryList = blob.FetchList("contains");
                    if (blobEntryList.Count != this.ContainsCount())
                        return false;
                    bool[] flagArray = new bool[blobEntryList.Count];
                    for (int index1 = 0; index1 < blobEntryList.Count; ++index1) {
                        bool flag = false;
                        for (int index2 = 0; index2 < blobEntryList.Count; ++index2) {
                            if (!flagArray[index2] && this.Contains[index2].MatchesSpecification(blobEntryList[index1].Blob())) {
                                flagArray[index2] = true;
                                flag = true;
                                break;
                            }
                        }
                        if (!flag)
                            return false;
                    }
                } else if (this.HasContains())
                    return false;
            }
            return true;
        }

        protected override void AssignFrom(Item item) {
            CraftItem craftItem = (CraftItem)item;
            this.Builder = craftItem.Builder;
            this.Configuration = craftItem.Configuration;
            this.DecomposeOnPickup = craftItem.DecomposeOnPickup;
            for (int index = 0; index < craftItem.ContainsCount(); ++index) {
                CraftItem contain = craftItem.Contains[index];
                if (this.ContainsCount() == index)
                    this.Contains.Add((CraftItem)contain.Clone((Item)null));
                else
                    this.Contains[index] = (CraftItem)contain.Clone((Item)this.Contains[index]);
            }
            while (this.ContainsCount() > craftItem.ContainsCount())
                this.Contains.RemoveAt(this.ContainsCount() - 1);
        }

        public override Item Clone(Item spare) {
            return Copy();
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            base.Restore(configuration, blob);
            this.Configuration = configuration;
            this.DecomposeOnPickup = blob.GetBool("decomposeOnPickup", false) || blob.Contains("specification") && blob.GetBlob("specification").GetBool("decomposeOnPickup", false);
            if (blob.Contains("contains")) {
                this.Contains.Clear();
                if (!blob.IsMaster())
                    throw new Exception();
                foreach (BlobEntry fetch in blob.FetchList("contains"))
                    this.Contains.Add((CraftItem)GameContext.ItemDatabase.SpawnItem(fetch.Blob(), (Item)null));
            } else {
                int index = (int)blob.GetLong("additionalItemsLength", 0L);
                if (index > 0) {
                    while (this.Contains.Count < index)
                        this.Contains.Add((CraftItem)null);
                    this.Contains.RemoveRange(index, this.Contains.Count - index);
                    Blob blob1 = blob.FetchBlob("additionalItems");
                    for (int i = 0; i < index; ++i) {
                        Blob result;
                        if (!blob1.TryGetBlob(IntegerStrings.ToString(i), out result))
                            throw new Exception();
                        this.Contains[i] = (CraftItem)GameContext.ItemDatabase.SpawnItem(result, (Item)this.Contains[i]);
                    }
                } else {
                    if (!this.HasContains())
                        return;
                    this.Contains.Clear();
                }
            }
        }

        public override string GetItemCode() {
            return this.Configuration.Code;
        }

        public override void Store(Blob blob) {
            base.Store(blob);
            blob.SetLong("additionalItemsLength", (long)this.Contains.Count);
            blob.SetBool("decomposeOnPickup", this.DecomposeOnPickup);
            Blob blob1 = blob.FetchBlob("additionalItems");
            for (int i = 0; i < this.Contains.Count; ++i)
                this.Contains[i].Store(blob1.FetchBlob(IntegerStrings.ToString(i)));
        }

        public CraftItem CopyAndReplaceOneItemWithAnother(
          CraftItem itemToReplace,
          CraftItem itemToAdd) {
            CraftItem craftItem = this.CopyWithoutAnyContainingItems();
            foreach (CraftItem contain in this.Contains) {
                if (contain == itemToReplace) {
                    if (itemToAdd != null)
                        craftItem.Contains.Add(itemToAdd.Copy() as CraftItem);
                } else
                    craftItem.Contains.Add(contain.Copy() as CraftItem);
            }
            return craftItem;
        }

        public CraftItem CopyWithoutAnyContainingItems() {
            CraftItem craftItem = new CraftItem(this.Builder);
            craftItem.Configuration = this.Configuration;
            craftItem.DecomposeOnPickup = this.DecomposeOnPickup;
            return craftItem;
        }

        public override Item Copy() {
            CraftItem craftItem = this.CopyWithoutAnyContainingItems();
            foreach (CraftItem contain in this.Contains)
                craftItem.Contains.Add(contain.Copy() as CraftItem);
            return (Item)craftItem;
        }

        public override Plukit.Base.Components GetComponents() {
            return this.Configuration.Components;
        }

        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return false;
        }

        public override ItemRenderer FetchRenderer() {
            return Builder.Renderer;
        }
    }
}
