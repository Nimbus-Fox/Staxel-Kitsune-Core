﻿using System;
using Plukit.Base;
using Staxel;
using Staxel.Effects;
using Staxel.EntityActions;
using Staxel.Items;
using Staxel.Items.ItemComponents;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.KitsuneCore.V1.Desealed.Staxel.EntityActions {
    public class EatEntityAction : EntityActionDriver {

        public EatEntityAction() {
            this.RegisterState("Begin", new Action<Entity, EntityUniverseFacade>(this.LoopAnim), true, (Action<Entity, EntityUniverseFacade>)null, false);
            this.RegisterState("Loop", new Action<Entity, EntityUniverseFacade>(this.Loop), true, new Action<Entity, EntityUniverseFacade>(this.LoopUpdate), false);
            this.RegisterState("End", new Action<Entity, EntityUniverseFacade>(this.End), true, (Action<Entity, EntityUniverseFacade>)null, false);
            this.RegisterState("EndFail", new Action<Entity, EntityUniverseFacade>(this.EndFail), true, (Action<Entity, EntityUniverseFacade>)null, false);
        }

        public void LoopAnim(Entity entity, EntityUniverseFacade facade) {
            this.RunAnimation(entity, "staxel.emote.Eat.loop", "Loop");
            ConsumableComponent component;
            if (!EatEntityAction.TryGetConsumableComponent(entity, out component) || string.IsNullOrEmpty(component.Sound))
                return;
            BaseEffects.PlaySound(entity, component.Sound, -1.0);
        }

        public void LoopUpdate(Entity entity, EntityUniverseFacade facade) {
            if (!entity.Controller.ControlState(ControlAxis.Main).Down) {
                this.RunAnimation(entity, "staxel.emote.Eat.end", "EndFail");
            } else {
                ConsumableComponent component;
                if (!EatEntityAction.TryGetConsumableComponent(entity, out component))
                    return;
                Vector3D vector = entity.Logic.Heading().ToVector();
                BaseEffects.BreakTileEffect(entity, "", component.Particles, entity.Physics.Position + vector, component.Colour);
            }
        }

        private static bool TryGetConsumableComponent(Entity entity, out ConsumableComponent component) {
            component = (ConsumableComponent)null;
            CraftItem craftItem = entity.Inventory.ActiveItem().Item as CraftItem;
            if (craftItem == null)
                return false;
            CraftItem edible = craftItem.FindEdible();
            if (edible == null)
                return false;
            component = edible.Configuration.Components.GetOrDefault<ConsumableComponent>();
            return component != null;
        }

        public void Loop(Entity entity, EntityUniverseFacade facade) {
            int num = (int)entity.Logic.ActionFacade.GetActionCookie("charge", 0L) + 1;
            entity.Logic.ActionFacade.SetActionCookie("charge", (long)num);
            CraftItem craftItem = entity.Inventory.ActiveItem().Item as CraftItem;
            if (craftItem == null) {
                this.RunAnimation(entity, "staxel.emote.Eat.end", "EndFail");
            } else {
                CraftItem edible = craftItem.FindEdible();
                if (edible == null || !entity.Controller.ControlState(ControlAxis.Main).Down)
                    this.RunAnimation(entity, "staxel.emote.Eat.end", "EndFail");
                else if ((long)num >= (long)edible.Configuration.EatTime) {
                    PlayerEntityLogic playerEntityLogic = entity.PlayerEntityLogic;
                    CraftItem itemToAdd = (CraftItem)null;
                    if (edible.Configuration.LeftOver != null)
                        itemToAdd = GameContext.ItemDatabase.SpawnItem(edible.Configuration.LeftOver, (Item)null) as CraftItem;
                    if (craftItem != edible)
                        playerEntityLogic.ItemFacade.ConsumeOneActiveItemAndProduceItem(new ItemStack((Item)craftItem.CopyAndReplaceOneItemWithAnother(edible, itemToAdd), 1), facade);
                    else if (itemToAdd != null)
                        playerEntityLogic.ItemFacade.ConsumeOneActiveItemAndProduceItem(new ItemStack((Item)itemToAdd, 1), facade);
                    else
                        playerEntityLogic.ItemFacade.ConsumeOneActiveItemAndProduceItem(new ItemStack(), facade);
                    this.RunAnimation(entity, "staxel.emote.Eat.end", "End");
                } else
                    this.LoopAnim(entity, facade);
            }
        }

        public void EndFail(Entity entity, EntityUniverseFacade facade) {
            entity.Logic.ActionFacade.NoNextAction();
            this.OnCancel(entity);
        }

        public void End(Entity entity, EntityUniverseFacade facade) {
            entity.Logic.ActionFacade.NoNextAction();
            this.OnCancel(entity);
        }

        protected override void OnCancel(Entity entity) {
            base.OnCancel(entity);
            entity.Logic.ActionFacade.SetActionCookie("charge", -1L);
        }

        public override string Kind() {
            return EatEntityAction.KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.kitsunecore.v1.desealed.staxel.entityAction.Eat";
        }

        public override void Start(Entity entity, EntityUniverseFacade facade) {
            this.RunAnimation(entity, "staxel.emote.Eat.begin", "Begin");
            entity.Logic.ActionFacade.SetActionCookie("charge", 0L);
        }
    }
}
