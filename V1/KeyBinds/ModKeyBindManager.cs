﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json.Utilities.LinqBridge;
using NimbusFox.KitsuneCore.V1.Hooks;
using Plukit.Base;
using Staxel;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V1.KeyBinds {
    public static class ModKeyBindManager {
        private static readonly Dictionary<string, Keys> Bindings = new Dictionary<string, Keys>();
        private static readonly Dictionary<string, Keys> BindingsCache = new Dictionary<string, Keys>();

        private static readonly Dictionary<string, Action> Events = new Dictionary<string, Action>();
        private static readonly List<Action> EventsCache = new List<Action>();

        internal static void Update(GameTime gameTime, DeviceContext context) {
            if (ClientContext.WebOverlayRenderer.HasInputControl() ||
                ClientContext.WebOverlayRenderer.HasLiteInputControl()) {
                return;
            }

            var keyboardState = Keyboard.GetState();

            var keys = keyboardState.GetPressedKeys();

            foreach (var key in keys) {
                if (ClientContext.WebOverlayRenderer.HasInputControl() ||
                    ClientContext.WebOverlayRenderer.HasLiteInputControl()) {
                    return;
                }

                if (Bindings.ContainsValue(key)) {
                    EventsCache.Clear();
                    BindingsCache.Clear();

                    BindingsCache.AddAll(Bindings.Where(x => x.Value == key));

                    EventsCache.AddRange(Events.Where(x => BindingsCache.ContainsKey(x.Key)).ToDictionary().Values);

                    foreach (var even in EventsCache) {
                        if (ClientContext.WebOverlayRenderer.HasInputControl() ||
                            ClientContext.WebOverlayRenderer.HasLiteInputControl()) {
                            return;
                        }

                        even?.Invoke();
                    }
                }
            }
        }

        internal static void Load() {
            if (!NimbusFox.KitsuneCore.Helpers.IsClient()) {
                return;
            }
            if (KitsuneHook.KitsuneCore.ConfigDirectory.FileExists("keybinds,json")) {
                using (var fs =
                    KitsuneHook.KitsuneCore.ConfigDirectory.ObtainFileStream("keybinds.json", FileMode.Open)) {
                    var blob = fs.ReadBlob();

                    foreach (var entry in blob.KeyValueIteratable) {
                        try {
                            if (Bindings.ContainsKey(entry.Key)) {
                                Bindings[entry.Key] = (Keys) entry.Value.GetLong();
                            } else {
                                Bindings.Add(entry.Key, (Keys) entry.Value.GetLong());
                            }
                        } catch {
                            // ignore
                        }
                    }
                    
                    Blob.Deallocate(ref blob);
                }
            }

            Save();
        }

        internal static void Save() {
            if (!NimbusFox.KitsuneCore.Helpers.IsClient()) {
                return;
            }
            
            var blob = BlobAllocator.Blob(true);

            foreach (var binding in Bindings) {
                blob.SetLong(binding.Key, (long)binding.Value);
            }

            using (var fs =
                KitsuneHook.KitsuneCore.ConfigDirectory.ObtainFileStream("keybinds.json", FileMode.Create)) {
                fs.WriteBlob(blob);
                fs.Flush(true);
            }
            
            Blob.Deallocate(ref blob);
        }

        public static void RegisterKeyBind(string code, Keys defaultKey, Action func) {
            if (!NimbusFox.KitsuneCore.Helpers.IsClient()) {
                return;
            }

            if (!Bindings.ContainsKey(code)) {
                Bindings.Add(code, defaultKey);
            }

            if (!Events.ContainsKey(code)) {
                Events.Add(code, func);
            }
        }

        internal static void UpdateKeyBind(string code, Keys key) {
            if (Bindings.ContainsKey(code)) {
                Bindings[code] = key;
            }

            Save();
        }
    }
}