﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Player;

namespace NimbusFox.KitsuneCore.Interfaces {
    public interface ITailModHook : IModHook {
        void OnPlayerConnect(Entity entity);
        void OnPlayerDisconnect(Entity entity);
        void Store(Blob blob);
        void Restore(Blob blob);
    }

    public interface ITailModHookV2 : IModHookV2, ITailModHook { }

    public interface ITailModHookV3 : IModHookV3, ITailModHookV2 { }

    public interface ITailModHookV4 : IModHookV4, ITailModHookV3 { }
}
