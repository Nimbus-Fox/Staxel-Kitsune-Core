﻿using NimbusFox.KitsuneCore.V1.Animation.Entities.Logic;
using NimbusFox.KitsuneCore.V1.Animation.Entities.Painter;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Entities.Builders {
    public class AnimatedTileStateEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
        public void Load() {

        }

        public string Kind => KindCode;
        public static string KindCode => "nimbusfox.kitsunecore.tilestateentity.animationTile";

        public bool IsTileStateEntityKind() {
            return true;
        }

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new AnimatedTileEntityLogic(entity);
        }

        EntityPainter IEntityPainterBuilder.Instance() {
            return new AnimatedTilePainter();
        }

        public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, Tile tile) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", KindCode);
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetString("tile", tile.Configuration.Code);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}
