﻿using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Components;
using NimbusFox.KitsuneCore.V1.Animation.Entities.Logic;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.KitsuneCore.V1.Animation.Entities.Painter {
    public class AnimatedTilePainter : EntityPainter {

        protected override void Dispose(bool disposing) { }

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade,
            int updateSteps) {
        }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) {
            if (entity.Logic is AnimatedTileEntityLogic logic) {
                if (logic.AnimationComponent == null) {
                    if (logic.Configuration.Components.Contains<KsAnimationComponent>()) {
                        logic.AnimationComponent = logic.Configuration.Components.Get<KsAnimationComponent>();

                        if (logic.AnimationComponent.Instance) {
                            logic.AnimationComponent = logic.AnimationComponent.GetInstance();
                        }
                    }
                }
            }
        }
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity, AvatarController avatarController,
            Timestep renderTimestep) { }

        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            if (entity.Logic is AnimatedTileEntityLogic logic) {

                if (logic.AnimationComponent == null) {
                    return;
                }

                var pos = (entity.Physics.Position - renderOrigin).ToVector3F();

                var matrix1 = Matrix4F.Identity;

                var offset = Vector3F.Zero;

                foreach (var component in logic.AnimationComponent.AnimationComponents) {
                    if (component.TileCompatible) {
                        matrix1 = component.TileRenderUpdate(entity, ref matrix1, out var _offset, renderMode);
                        offset = offset + _offset;
                    }
                }

                matrix1 = Matrix4F.Multiply(
                    Matrix.CreateFromYawPitchRoll(MathHelper.ToRadians(90 * logic.Rotation), 0, 0).ToMatrix4F(), matrix1);

                matrix1 = matrix1.Translate(pos + offset).Multiply(ref matrix);

                if (logic.AnimationComponent.Rendering != null) {
                    logic.AnimationComponent.Rendering.RenderTile(graphics, ref matrix1);
                } else {
                    UpdateDrawable(logic, renderMode);
                    logic.Tile.Render(graphics, ref matrix1);
                }
            }
        }

        private static ColorReplace CR = new ColorReplace();

        private void UpdateDrawable(IAnimatedTile tile, RenderMode renderMode) {
            CR.Clear();

            foreach (var component in tile.AnimationComponent.AnimationComponents) {
                if (component.TileCompatible) {
                    component.TileColorUpdate(tile.Configuration, CR, renderMode);
                }
            }

            tile.Tile = VoxelRecolor.FetchDrawable(tile.Configuration, CR.Colors);
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}
