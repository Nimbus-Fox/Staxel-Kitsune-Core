﻿using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Components;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneCore.V1.Animation.Entities.Logic {
    public class AnimatedTileEntityLogic : TileStateEntityLogic, IAnimatedTile {

        public KsAnimationComponent AnimationComponent { get; internal set; }
        public MultipleMatrixDrawable Tile { get; set; }
        private bool _needsStore = true;
        public TileConfiguration Configuration { get; private set; }
        private Blob _constructArguments;
        public uint Rotation { get; private set; }

        public AnimatedTileEntityLogic(Entity entity) : base(entity) {
        }
        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }
        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                if (tile.Configuration != Configuration) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            Entity.Physics.MakePhysicsless();
            Entity.Physics.ForcedPosition(arguments.FetchBlob("location").GetVector3I().ToTileCenterVector3D());
            Location = arguments.FetchBlob("location").GetVector3I();
            Configuration = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));

            _constructArguments = BlobAllocator.Blob(true);
            _constructArguments.AssignFrom(arguments);
            _needsStore = true;

            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                Rotation = tile.Configuration.Rotation(tile.Variant());
            }
        }
        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override bool IsPersistent() {
            return true;
        }

        public override bool IsLingering() {
            return false;
        }

        public override void KeepAlive() { }
        public override void BeingLookedAt(Entity entity) { }
        public override bool IsBeingLookedAt() {
            return false;
        }

        public override void Store() {
            base.Store();
            if (_needsStore) {
                Entity.Blob.SetString("tile", Configuration.Code);
                Entity.Blob.FetchBlob("location").SetVector3I(Location);

                _needsStore = false;
            }
        }

        public override void Restore() {
            if (Entity.Blob.Contains("tile")) {
                Configuration = GameContext.TileDatabase.GetTileConfiguration(Entity.Blob.GetString("tile"));
            }

            if (Entity.Blob.Contains("location")) {
                Location = Entity.Blob.GetBlob("location").GetVector3I();
            }
            base.Restore();
        }

        public override void StorePersistenceData(Blob data) {
            base.StorePersistenceData(data);
            data.FetchBlob("constructor").MergeFrom(_constructArguments);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);

            if (data.Contains("constructor")) {
                Construct(data.GetBlob("constructor"), facade);
            }
        }

        public override bool IsCollidable() {
            return Configuration.Collision;
        }
    }
}
