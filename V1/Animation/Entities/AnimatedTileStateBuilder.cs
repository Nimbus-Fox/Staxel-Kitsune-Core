﻿using NimbusFox.KitsuneCore.V1.Animation.Entities.Builders;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneCore.V1.Animation.Entities {
    public class AnimatedTileStateBuilder : ITileStateBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return "nimbusfox.kitsunecore.tilestate.animationTile";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return AnimatedTileStateEntityBuilder.Spawn(location, universe, tile);
        }
    }
}
