﻿using NimbusFox.KitsuneCore.V1.Animation.Classes;
using Plukit.Base;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Interfaces {
    public interface IAnimationComponent {
        void SetRandomFrame();

        void ItemColorUpdate(Item item, ColorReplace colorReplace, RenderMode renderMode);
        Matrix4F ItemRenderUpdate(Item item, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode);

        void TileColorUpdate(TileConfiguration configuration, ColorReplace colorReplace, RenderMode renderMode);
        Matrix4F TileRenderUpdate(Entity entity, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode);

        void SubRenderColorUpdate(ColorReplace colorReplace, RenderMode renderMode);
        Matrix4F SubRenderRenderUpdate(ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode);

        bool TileCompatible { get; }
        bool ItemCompatible { get; }
        bool SubRenderCompatible { get; }
        bool HandleRendering { get; }

        void Render(DeviceContext graphics, ref Matrix4F matrix);
        void RenderIcon(DeviceContext graphics, ref Matrix4F matrix);
        void RenderInWorldFullSized(DeviceContext graphics, ref Matrix4F matrix);

        void RenderTile(DeviceContext graphics, ref Matrix4F matrix);
    }
}