﻿using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Interfaces {
    public interface IAnimationComponentBuilder {
        string Kind();
        IAnimationComponent Instance(string code, Blob config);
    }
}
