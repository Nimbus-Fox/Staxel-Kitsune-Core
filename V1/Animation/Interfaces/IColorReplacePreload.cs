﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace NimbusFox.KitsuneCore.V1.Animation.Interfaces {
    public interface IColorReplacePreload {
        List<Dictionary<Color, Color>> Preload();
    }
}