﻿using Staxel.Items;

namespace NimbusFox.KitsuneCore.V1.Animation.Interfaces {
    public interface IItemBuilderHelper : IItemBuilder {
        ItemRenderer Renderer { get; }
    }
}