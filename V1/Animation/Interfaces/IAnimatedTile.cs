﻿using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Components;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Interfaces {
    public interface IAnimatedTile {
        KsAnimationComponent AnimationComponent { get; }
        MultipleMatrixDrawable Tile { get; set; }
        TileConfiguration Configuration { get; }
        uint Rotation { get; }
    }
}