﻿using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Components;

namespace NimbusFox.KitsuneCore.V1.Animation.Interfaces {
    public interface IAnimatedItem {
        KsAnimationComponent AnimationComponent { get; }
        MultipleMatrixDrawable IconDrawable { get; set; }
        MultipleMatrixDrawable CompactDrawable { get; set; }
        MultipleMatrixDrawable SecondaryDrawable { get; set; }
    }
}