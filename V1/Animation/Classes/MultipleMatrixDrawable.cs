﻿using System;
using System.Collections.Generic;
using Plukit.Base;
using Staxel.Draw;
using Staxel.Voxel;

namespace NimbusFox.KitsuneCore.V1.Animation.Classes {
    public class MultipleMatrixDrawable : IDisposable {
        private readonly List<MatrixDrawable> _drawables;
        public Vector3I Dimensions { get; }
        public Vector3F VoxelOffsets { get; }
        private Matrix4F Matrix = Matrix4F.Identity;
        public MultipleMatrixDrawable(VoxelObject voxels) {
            _drawables = new List<MatrixDrawable>();
            Dimensions = voxels.Dimensions;
            VoxelOffsets = new Vector3F(-voxels.Dimensions.X / 2f, -voxels.Min.Y, -voxels.Dimensions.Z / 2f) / 16f;
        }

        public void Add(MatrixDrawable drawable) {
            if (!_drawables.Contains(drawable)) {
                _drawables.Add(drawable.Matrix(Matrix));
            }
        }

        public void Remove(MatrixDrawable drawable) {
            _drawables.Remove(drawable);
        }

        public void Render(DeviceContext graphics, ref Matrix4F matrix) {
            foreach (var draw in _drawables) {
                draw.Render(graphics, ref matrix);
            }
        }

        public void SetMatrix(Matrix4F matrix) {
            Matrix = matrix;
            for (var i = 0; i < _drawables.Count; i++) {
                _drawables[i] = _drawables[i].Matrix(matrix);
            }
        }
        
        public void Dispose() {
            foreach (var matrix in _drawables) {
                matrix.Dispose();
            }
            
            _drawables.Clear();
        }
    }
}
