﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace NimbusFox.KitsuneCore.V1.Animation.Classes {
    public class ColorReplace {
        private Dictionary<Color, Color> _replace = new Dictionary<Color, Color>();

        public void Replace(Color source, Color replace) {
            if (!_replace.ContainsKey(source)) {
                _replace.Add(source, replace);
            }
        }

        public Dictionary<Color, Color> Colors => new Dictionary<Color, Color>(_replace);

        internal void Clear() {
            _replace.Clear();
        }
    }
}
