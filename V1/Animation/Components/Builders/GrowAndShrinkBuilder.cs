﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class GrowAndShrinkBuilder : IAnimationComponentBuilder {
        public string Kind() {
            return "growAndShrink";
        }

        public IAnimationComponent Instance(string code, Blob config) {
            return new GrowAndShrink(config);
        }
    }
}
