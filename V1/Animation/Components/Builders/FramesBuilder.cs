﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class FramesBuilder : IAnimationComponentBuilder {
        public string Kind() {
            return "frames";
        }

        public IAnimationComponent Instance(string code, Blob config) {
            return new Frames(config);
        }
    }
}
