﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class PaintBuilder : IAnimationComponentBuilder {
        public string Kind() {
            return "paint";
        }

        public IAnimationComponent Instance(string code, Blob config) {
            return new Paint(config);
        }
    }
}
