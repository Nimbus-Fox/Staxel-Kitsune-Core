﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class KsAnimationTileComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "ksAnimation";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new KsAnimationComponent(tile.Code, config);
        }
    }
}
