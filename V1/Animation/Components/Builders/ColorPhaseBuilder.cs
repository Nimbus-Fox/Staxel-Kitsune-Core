﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class ColorPhaseBuilder : IAnimationComponentBuilder {
        public string Kind() {
            return "colorPhase";
        }

        public IAnimationComponent Instance(string code, Blob config) {
            return new ColorPhase(config);
        }
    }
}
