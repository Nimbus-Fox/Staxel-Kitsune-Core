﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class ColorPulseBuilder : IAnimationComponentBuilder {
        public string Kind() {
            return "colorPulse";
        }

        public IAnimationComponent Instance(string code, Blob config) {
            return new ColorPulse(config);
        }
    }
}
