﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class RotationBuilder : IAnimationComponentBuilder {
        public string Kind() {
            return "rotation";
        }

        public IAnimationComponent Instance(string code, Blob config) {
            return new Rotation(config);
        }
    }
}
