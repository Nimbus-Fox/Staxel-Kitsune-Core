﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class MovementBuilder : IAnimationComponentBuilder {
        public string Kind() {
            return "movement";
        }

        public IAnimationComponent Instance(string code, Blob config) {
            return new Movement(config);
        }
    }
}
