﻿using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.KitsuneCore.V1.Animation.Components.Builders {
    public class KsAnimationItemComponentBuilder : IItemComponentBuilder {
        public string Kind() {
            return "ksAnimation";
        }

        public object Instance(BaseItemConfiguration item, Blob config) {
            return new KsAnimationComponent(item.Code, config);
        }
    }
}
