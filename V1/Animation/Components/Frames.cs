﻿using System;
using System.Collections.Generic;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.KitsuneCore.V1.Animation.Components {
    public class Frames : IAnimationComponent {

        private List<(CompactVertexDrawable drawable, long milliseconds)> _frames = new List<(CompactVertexDrawable drawable, long milliseconds)>();

        private DateTime _lastIconFrame = DateTime.MinValue;
        private int _iconIndex = 0;

        public Frames(Blob config) {
            var frames = config.GetBlob("frames");

            foreach (var entry in frames.KeyValueIteratable) {
                _frames.Add((
                    VoxelLoader.LoadQb(GameContext.ContentLoader.ReadStream(entry.Key), entry.Key, Vector3I.Zero,
                        Vector3I.MaxValue).BuildVertices(), entry.Value.GetLong()));
            }
        }

        public void SetRandomFrame() {
            _iconIndex = GameContext.RandomSource.Next(0, _frames.Count - 1);
        }
        public void ItemColorUpdate(Item item, ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F ItemRenderUpdate(Item item, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public void TileColorUpdate(TileConfiguration configuration, ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F TileRenderUpdate(Entity entity, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public void SubRenderColorUpdate(ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F SubRenderRenderUpdate(ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public bool TileCompatible => false;
        public bool ItemCompatible => true;
        public bool SubRenderCompatible => false;
        public bool HandleRendering => true;

        public void Render(DeviceContext graphics, ref Matrix4F matrix) {
            if (_lastIconFrame < DateTime.Now.AddMilliseconds(-_frames[_iconIndex].milliseconds)) {
                _iconIndex++;

                if (_iconIndex >= _frames.Count) {
                    _iconIndex = 0;
                }

                _lastIconFrame = DateTime.Now;
            }

            _frames[_iconIndex].drawable.Render(graphics, ref matrix);
        }

        public void RenderIcon(DeviceContext graphics, ref Matrix4F matrix) {
            if (_lastIconFrame < DateTime.Now.AddMilliseconds(-_frames[_iconIndex].milliseconds)) {
                _iconIndex++;

                if (_iconIndex >= _frames.Count) {
                    _iconIndex = 0;
                }

                _lastIconFrame = DateTime.Now;
            }

            _frames[_iconIndex].drawable.Render(graphics, ref matrix);
        }

        public void RenderInWorldFullSized(DeviceContext graphics, ref Matrix4F matrix) {
            if (_lastIconFrame < DateTime.Now.AddMilliseconds(-_frames[_iconIndex].milliseconds)) {
                _iconIndex++;

                if (_iconIndex >= _frames.Count) {
                    _iconIndex = 0;
                }

                _lastIconFrame = DateTime.Now;
            }

            _frames[_iconIndex].drawable.Render(graphics, ref matrix);
        }

        public void RenderTile(DeviceContext graphics, ref Matrix4F matrix) {
            if (_lastIconFrame < DateTime.Now.AddMilliseconds(-_frames[_iconIndex].milliseconds)) {
                _iconIndex++;

                if (_iconIndex >= _frames.Count) {
                    _iconIndex = 0;
                }

                _lastIconFrame = DateTime.Now;
            }

            _frames[_iconIndex].drawable.Render(graphics, ref matrix);
        }
    }
}
