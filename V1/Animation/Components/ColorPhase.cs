﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json.Utilities;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Components {
    public class ColorPhase : IAnimationComponent, IColorReplacePreload {

        private Color Target { get; }
        private Color[] Colors { get; }
        private float _level;
        private Color _current;
        private Color _last;
        private long Delay { get; }
        private float Step { get; }
        private DateTime _lastRun = DateTime.MinValue;
        private Color _lerp;

        public ColorPhase(Blob config) {
            if (!config.Contains("target")) {
                throw new BlobValueException("No target color defined");
            }

            Target = config.GetColor("target");

            if (!config.Contains("colors")) {
                throw new BlobValueException("No colors list defined");
            }

            var colors = new List<Color>();

            var colorsEntry = config.KeyValueIteratable["colors"];
            if (colorsEntry.Kind == BlobEntryKind.String) {
                var bitmap =
                    BitmapLoader.LoadFromStream(GameContext.ContentLoader.ReadStream(colorsEntry.GetString()));

                for (var x = 0; x < bitmap.Size.Width; x++) {
                    for (var y = 0; y < bitmap.Size.Height; y++) {
                        var bitmapColor = bitmap.GetPixel(x, y);

                        var col = new Color(bitmapColor.R, bitmapColor.G, bitmapColor.B, bitmapColor.A);

                        if (col.A == Color.Transparent.A) {
                            continue;
                        }

                        if (!colors.Contains(col)) {
                            colors.Add(col);
                        }
                    }
                }

                bitmap.Dispose();
            } else if (colorsEntry.Kind == BlobEntryKind.List) {
                if (colorsEntry.List().Count == 0) {
                    throw new BlobValueException("colors list must not be empty and be valid color formats");
                }
                foreach (var item in colorsEntry.List()) {
                    var color = item.GetColor();

                    if (color.A == Color.Transparent.A) {
                        continue;
                    }

                    if (!colors.Contains(color)) {
                        colors.Add(color);
                    }
                }
            } else {
                throw new BlobValueException("colors must be a list or an image path");
            }

            Colors = colors.ToArray();
            colors.Clear();
            _current = Colors[1];
            _last = Colors[0];
            _lerp = _last;

            Delay = config.GetLong("delay", 20);

            Step = (float) config.GetDouble("step", 0.05);
        }

        public void SetRandomFrame() {
            var index = GameContext.RandomSource.Next(0, Colors.Length - 1);
            _level = GameContext.RandomSource.NextFloat(0, 1);

            _level = (_level / Step) * Step;

            _last = Colors[index];

            _current = Colors[index < Colors.Length - 1 ? index : 0];

            _lerp = Color.Lerp(_last, _current, _level);
        }

        public void ItemColorUpdate(Item item, ColorReplace colorReplace, RenderMode renderMode) {
            if (_lastRun < DateTime.Now.AddMilliseconds(-Delay) && renderMode == RenderMode.Normal) {
                _level += Step;
                _lastRun = DateTime.Now;

                if (_level >= 1.0) {
                    _level = 0;
                    var index = Colors.IndexOf(x => x == _current) + 1;
                    if (Colors.Length == 2) {
                        if (_current == Colors[0]) {
                            _current = Colors[1];
                            _last = Colors[0];
                        } else {
                            _current = Colors[0];
                            _last = Colors[1];
                        }
                    } else {
                        _last = _current;
                        _current = index >= Colors.Length - 1 ? Colors[0] : Colors[index];
                    }
                }

                _lerp = Color.Lerp(_last, _current, _level);
            }

            colorReplace.Replace(Target, _lerp);
        }

        public Matrix4F ItemRenderUpdate(Item item, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public void TileColorUpdate(TileConfiguration configuration, ColorReplace colorReplace, RenderMode renderMode) {
            if (_lastRun < DateTime.Now.AddMilliseconds(-Delay) && renderMode == RenderMode.Normal) {
                _level += Step;
                _lastRun = DateTime.Now;

                if (_level > 1.0) {
                    _level = 0;
                    _last = _current;
                    var index = Colors.IndexOf(x => x == _current) + 1;
                    _current = index >= Colors.Length - 1 ? Colors[0] : Colors[index];
                }

                _lerp = Color.Lerp(_last, _current, _level);
            }

            colorReplace.Replace(Target, _lerp);
        }

        public Matrix4F TileRenderUpdate(Entity entity, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public void SubRenderColorUpdate(ColorReplace colorReplace, RenderMode renderMode) {
            if (_lastRun < DateTime.Now.AddMilliseconds(-Delay) && renderMode == RenderMode.Normal) {
                _level += Step;
                _lastRun = DateTime.Now;

                if (_level > 1.0) {
                    _level = 0;
                    _last = _current;
                    var index = Colors.IndexOf(x => x == _current) + 1;
                    _current = index >= Colors.Length - 1 ? Colors[0] : Colors[index];
                }

                _lerp = Color.Lerp(_last, _current, _level);
            }

            colorReplace.Replace(Target, _lerp);
        }

        public Matrix4F SubRenderRenderUpdate(ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public bool TileCompatible => true;
        public bool ItemCompatible => true;
        public bool SubRenderCompatible => true;
        public bool HandleRendering => false;
        public void Render(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderIcon(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderInWorldFullSized(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderTile(DeviceContext graphics, ref Matrix4F matrix) { }

        public List<Dictionary<Color, Color>> Preload() {
            var output = new List<Dictionary<Color, Color>>();
            
            var last = Colors[0];
            var level = 0.0f;

            for (var i = 1; i < Colors.Length; i++) {
                var current = Colors[i];

                while (level <= 1.0f) {
                    output.Add(new Dictionary<Color, Color>{{Target, Color.Lerp(last, current, level)}});
                    level += Step;
                }

                level = 0.0f;
                last = current;
            }

            last = Colors.Last();
            while (level <= 1.0f) {
                output.Add(new Dictionary<Color, Color>(){{Target, Color.Lerp(last, Colors[0], level)}});
                level += Step;
            }

            return output;
        }
    }
}
