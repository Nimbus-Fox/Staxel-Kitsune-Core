﻿using System;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Components {
    public class Rotation : IAnimationComponent {

        private Vector3F _step;
        private Vector3F _min;
        private Vector3F _max;
        private long _delay;
        private Vector3F _current;
        private DateTime _lastRun = DateTime.MinValue;
        private bool _spin;
        private Vector3F _offset;

        private bool _addX = true;
        private bool _addY = true;
        private bool _addZ = true;

        public Rotation(Blob config) {
            if (!config.Contains("step")) {
                throw new BlobValueException("step must contain an x, y and z value");
            }

            _step = config.FetchBlob("step").GetVector3F();

            if (config.Contains("min")) {
                _min = config.FetchBlob("min").GetVector3F();

                if (_min.X > 360 || _min.X < 0) {
                    _min.X = 0;
                }

                if (_min.Y > 360 || _min.Y < 0) {
                    _min.Y = 0;
                }
            } else {
                _min = Vector3F.Zero;
            }

            if (config.Contains("max")) {
                _max = config.FetchBlob("max").GetVector3F();

                if (_max.X > 360 || _max.X < 0 || _max.X < _min.X) {
                    _max.X = 360;
                }

                if (_max.Y > 360 || _max.Y < 0 || _max.Y < _min.Y) {
                    _max.Y = 360;
                }
            } else {
                _max = new Vector3F(360, 360, 360);
            }

            _delay = Math.Abs(config.GetLong("delay", 20));

            _spin = config.GetBool("spin", false);

            if (_spin) {
                _min = Vector3F.Zero;
                _max = new Vector3F(360, 360, 360);
            }

            _current = new Vector3F(_min.X, _min.Y, _min.Z);

            if (!config.Contains("offset")) {
                _offset = new Vector3F(0.0f, 0.5f, 0.0f);
            } else {
                var offset = config.FetchBlob("offset").GetVector3F();

                _offset = new Vector3F(offset.X, offset.Y, offset.Z);
            }
        }

        public void SetRandomFrame() {
            _current.X = GameContext.RandomSource.NextFloat(_min.X, _max.X);
            _current.Y = GameContext.RandomSource.NextFloat(_min.Y, _max.Y);
            _current.Z = GameContext.RandomSource.NextFloat(_min.Z, _max.Z);

            _current.X = (_current.X / _step.X) * _step.X;
            _current.Y = (_current.Y / _step.Y) * _step.Y;
            _current.Z = (_current.Z / _step.Z) * _step.Z;
        }
        public void ItemColorUpdate(Item item, ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F ItemRenderUpdate(Item item, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay) && renderMode == RenderMode.Normal) {
                if (_addX) {
                    _current.X += _step.X;

                    if (_current.X >= _max.X) {
                        if (_spin) {
                            _current.X = 0;
                        } else {
                            _addX = false;
                            _current.X = _max.X;
                        }
                    }
                } else {
                    _current.X -= _step.X;

                    if (_current.X <= _min.X) {
                        _addX = true;
                        _current.X = _min.X;
                    }
                }

                if (_addY) {
                    _current.Y += _step.Y;

                    if (_current.Y >= _max.Y) {
                        if (_spin) {
                            _current.Y = 0;
                        } else {
                            _addY = false;
                            _current.Y = _max.Y;
                        }
                    }
                } else {
                    _current.Y -= _step.Y;

                    if (_current.Y <= _min.Y) {
                        _addY = true;
                        _current.Y = _min.Y;
                    }
                }

                if (_addZ) {
                    _current.Z += _step.Z;

                    if (_current.Z >= _max.Z) {
                        if (_spin) {
                            _current.Z = 0;
                        } else {
                            _addZ = false;
                            _current.Z = _max.Z;
                        }
                    }
                } else {
                    _current.Z -= _step.Z;

                    if (_current.Z <= _min.Z) {
                        _addZ = true;
                        _current.Z = _min.Z;
                    }
                }

                _lastRun = DateTime.Now;
            }

            var xRadian = MathHelper.ToRadians(_current.X);
            var yRadian = MathHelper.ToRadians(_current.Y);
            var zRadian = MathHelper.ToRadians(_current.Z);

            return Matrix4F.Identity.Translate(-_offset)
                .RotateUnitX(xRadian).RotateUnitY(yRadian).RotateUnitZ(zRadian)
                .Translate(_offset)
                .Multiply(ref matrix);
        }

        public void TileColorUpdate(TileConfiguration configuration, ColorReplace colorReplace, RenderMode renderMode) { }

        public Matrix4F TileRenderUpdate(Entity entity, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay) && renderMode == RenderMode.Normal) {
                if (_addX) {
                    _current.X += _step.X;

                    if (_current.X >= _max.X) {
                        if (_spin) {
                            _current.X = 0;
                        } else {
                            _addX = false;
                            _current.X = _max.X;
                        }
                    }
                } else {
                    _current.X -= _step.X;

                    if (_current.X <= _min.X) {
                        _addX = true;
                        _current.X = _min.X;
                    }
                }

                if (_addY) {
                    _current.Y += _step.Y;

                    if (_current.Y >= _max.Y) {
                        if (_spin) {
                            _current.Y = 0;
                        } else {
                            _addY = false;
                            _current.Y = _max.Y;
                        }
                    }
                } else {
                    _current.Y -= _step.Y;

                    if (_current.Y <= _min.Y) {
                        _addY = true;
                        _current.Y = _min.Y;
                    }
                }

                if (_addZ) {
                    _current.Z += _step.Z;

                    if (_current.Z >= _max.Z) {
                        if (_spin) {
                            _current.Z = 0;
                        } else {
                            _addZ = false;
                            _current.Z = _max.Z;
                        }
                    }
                } else {
                    _current.Z -= _step.Z;

                    if (_current.Z <= _min.Z) {
                        _addZ = true;
                        _current.Z = _min.Z;
                    }
                }

                _lastRun = DateTime.Now;
            }

            var xRadian = MathHelper.ToRadians(_current.X);
            var yRadian = MathHelper.ToRadians(_current.Y);
            var zRadian = MathHelper.ToRadians(_current.Z);

            return Matrix4F.Identity.Translate(-_offset)
                .RotateUnitX(xRadian).RotateUnitY(yRadian).RotateUnitZ(zRadian)
                .Translate(_offset)
                .Multiply(ref matrix);
        }

        public void SubRenderColorUpdate(ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F SubRenderRenderUpdate(ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay) && renderMode == RenderMode.Normal) {
                if (_addX) {
                    _current.X += _step.X;

                    if (_current.X >= _max.X) {
                        if (_spin) {
                            _current.X = 0;
                        } else {
                            _addX = false;
                            _current.X = _max.X;
                        }
                    }
                } else {
                    _current.X -= _step.X;

                    if (_current.X <= _min.X) {
                        _addX = true;
                        _current.X = _min.X;
                    }
                }

                if (_addY) {
                    _current.Y += _step.Y;

                    if (_current.Y >= _max.Y) {
                        if (_spin) {
                            _current.Y = 0;
                        } else {
                            _addY = false;
                            _current.Y = _max.Y;
                        }
                    }
                } else {
                    _current.Y -= _step.Y;

                    if (_current.Y <= _min.Y) {
                        _addY = true;
                        _current.Y = _min.Y;
                    }
                }

                if (_addZ) {
                    _current.Z += _step.Z;

                    if (_current.Z >= _max.Z) {
                        if (_spin) {
                            _current.Z = 0;
                        } else {
                            _addZ = false;
                            _current.Z = _max.Z;
                        }
                    }
                } else {
                    _current.Z -= _step.Z;

                    if (_current.Z <= _min.Z) {
                        _addZ = true;
                        _current.Z = _min.Z;
                    }
                }

                _lastRun = DateTime.Now;
            }

            var xRadian = MathHelper.ToRadians(_current.X);
            var yRadian = MathHelper.ToRadians(_current.Y);
            var zRadian = MathHelper.ToRadians(_current.Z);

            return Matrix4F.Identity.Translate(-_offset)
                .RotateUnitX(xRadian).RotateUnitY(yRadian).RotateUnitZ(zRadian)
                .Translate(_offset)
                .Multiply(ref matrix);
        }

        public bool TileCompatible => true;
        public bool ItemCompatible => true;
        public bool SubRenderCompatible => true;
        public bool HandleRendering => false;
        public void Render(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderIcon(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderInWorldFullSized(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderTile(DeviceContext graphics, ref Matrix4F matrix) { }
    }
}
