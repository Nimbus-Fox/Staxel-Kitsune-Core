﻿using System;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Components {
    public class GrowAndShrink : IAnimationComponent {

        private Vector3F _step;
        private Vector3F _min;
        private Vector3F _max;
        private Vector3F _current;
        private long _delay;
        private DateTime _lastRun = DateTime.MinValue;
        private Vector3F _offset;

        private bool _addX = true;
        private bool _addY = true;
        private bool _addZ = true;

        public GrowAndShrink(Blob config) {
            if (!config.Contains("steps")) {
                throw new BlobValueException("growAndShrink must have a steps property in the format of Vector3F");
            }

            _step = config.FetchBlob("steps").GetVector3F();

            if (!config.Contains("min")) {
                _min = Vector3F.One;
            } else {
                _min = config.FetchBlob("min").GetVector3F();
            }

            if (!config.Contains("max")) {
                _max = Vector3F.One;
            } else {
                _max = config.FetchBlob("max").GetVector3F();
            }

            if (!config.Contains("offset")) {
                _offset = new Vector3F(0.0f, 0.5f, 0.0f);
            } else {
                var offset = config.FetchBlob("offset").GetVector3F();

                _offset = new Vector3F(offset.X, offset.Y, offset.Z);
            }

            _current = _min;

            _delay = config.GetLong("delay", 20);
        }

        public void SetRandomFrame() {
            _current.X = GameContext.RandomSource.NextFloat(_min.X, _max.X);
            _current.Y = GameContext.RandomSource.NextFloat(_min.Y, _max.Y);
            _current.Z = GameContext.RandomSource.NextFloat(_min.Z, _max.Z);

            _current.X = (_current.X / _step.X) * _step.X;
            _current.Y = (_current.Y / _step.Y) * _step.Y;
            _current.Z = (_current.Z / _step.Z) * _step.Z;
        }
        public void ItemColorUpdate(Item item, ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F ItemRenderUpdate(Item item, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay)) {
                if (_addX) {
                    _current.X += _step.X;

                    if (_current.X >= _max.X) {
                        _addX = false;
                        _current.X = _max.X;
                    }
                } else {
                    _current.X -= _step.X;

                    if (_current.X <= _min.X) {
                        _addX = true;
                        _current.X = _min.X;
                    }
                }

                if (_addY) {
                    _current.Y += _step.Y;

                    if (_current.Y >= _max.Y) {
                        _addY = false;
                        _current.Y = _max.Y;
                    }
                } else {
                    _current.Y -= _step.Y;

                    if (_current.Y <= _min.Y) {
                        _addY = true;
                        _current.Y = _min.Y;
                    }
                }

                if (_addZ) {
                    _current.Z += _step.Z;

                    if (_current.Z >= _max.Z) {
                        _addZ = false;
                        _current.Z = _max.Z;
                    }
                } else {
                    _current.Z -= _step.Z;

                    if (_current.Z <= _min.Z) {
                        _addZ = true;
                        _current.Z = _min.Z;
                    }
                }

                _lastRun = DateTime.Now;
            }

            return Matrix4F.Identity.Translate(-_offset).Scale(_current).Translate(_offset).Multiply(ref matrix);
        }

        public void TileColorUpdate(TileConfiguration configuration, ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F TileRenderUpdate(Entity entity, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay)) {
                if (_addX) {
                    _current.X += _step.X;

                    if (_current.X >= _max.X) {
                        _addX = false;
                        _current.X = _max.X;
                    }
                } else {
                    _current.X -= _step.X;

                    if (_current.X <= _min.X) {
                        _addX = true;
                        _current.X = _min.X;
                    }
                }

                if (_addY) {
                    _current.Y += _step.Y;

                    if (_current.Y >= _max.Y) {
                        _addY = false;
                        _current.Y = _max.Y;
                    }
                } else {
                    _current.Y -= _step.Y;

                    if (_current.Y <= _min.Y) {
                        _addY = true;
                        _current.Y = _min.Y;
                    }
                }

                if (_addZ) {
                    _current.Z += _step.Z;

                    if (_current.Z >= _max.Z) {
                        _addZ = false;
                        _current.Z = _max.Z;
                    }
                } else {
                    _current.Z -= _step.Z;

                    if (_current.Z <= _min.Z) {
                        _addZ = true;
                        _current.Z = _min.Z;
                    }
                }

                _lastRun = DateTime.Now;
            }

            return Matrix4F.Identity.Translate(-_offset).Scale(_current).Translate(_offset).Multiply(ref matrix);
        }

        public void SubRenderColorUpdate(ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F SubRenderRenderUpdate(ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay)) {
                if (_addX) {
                    _current.X += _step.X;

                    if (_current.X >= _max.X) {
                        _addX = false;
                        _current.X = _max.X;
                    }
                } else {
                    _current.X -= _step.X;

                    if (_current.X <= _min.X) {
                        _addX = true;
                        _current.X = _min.X;
                    }
                }

                if (_addY) {
                    _current.Y += _step.Y;

                    if (_current.Y >= _max.Y) {
                        _addY = false;
                        _current.Y = _max.Y;
                    }
                } else {
                    _current.Y -= _step.Y;

                    if (_current.Y <= _min.Y) {
                        _addY = true;
                        _current.Y = _min.Y;
                    }
                }

                if (_addZ) {
                    _current.Z += _step.Z;

                    if (_current.Z >= _max.Z) {
                        _addZ = false;
                        _current.Z = _max.Z;
                    }
                } else {
                    _current.Z -= _step.Z;

                    if (_current.Z <= _min.Z) {
                        _addZ = true;
                        _current.Z = _min.Z;
                    }
                }

                _lastRun = DateTime.Now;
            }

            return Matrix4F.Identity.Translate(-_offset).Scale(_current).Translate(_offset).Multiply(ref matrix);
        }

        public bool TileCompatible => true;
        public bool ItemCompatible => true;
        public bool SubRenderCompatible => true;
        public bool HandleRendering => false;
        public void Render(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderIcon(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderInWorldFullSized(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderTile(DeviceContext graphics, ref Matrix4F matrix) { }
    }
}
