﻿using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Classes;
using Plukit.Base;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Components {
    public class Paint : IAnimationComponent {
        public Color Target { get; }
        public Color Replace { get; }

        public Paint(Blob config) {
            if (!config.Contains("target")) {
                throw new BlobValueException("No target color defined");
            }

            Target = config.GetColor("target");

            Replace = config.Contains("paint") ? config.GetColor("paint") : Target;
        }

        public void SetRandomFrame() { }

        public void ItemColorUpdate(Item item, ColorReplace colorReplace, RenderMode renderMode) {
            colorReplace.Replace(Target, Replace);
        }

        public Matrix4F ItemRenderUpdate(Item item, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public void TileColorUpdate(TileConfiguration configuration, ColorReplace colorReplace, RenderMode renderMode) {
            colorReplace.Replace(Target, Replace);
        }

        public Matrix4F TileRenderUpdate(Entity entity, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public void SubRenderColorUpdate(ColorReplace colorReplace, RenderMode renderMode) {
            colorReplace.Replace(Target, Replace);
        }

        public Matrix4F SubRenderRenderUpdate(ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public bool TileCompatible => true;
        public bool ItemCompatible => true;
        public bool SubRenderCompatible => true;
        public bool HandleRendering => false;
        public void Render(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderIcon(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderInWorldFullSized(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderTile(DeviceContext graphics, ref Matrix4F matrix) { }
    }
}
