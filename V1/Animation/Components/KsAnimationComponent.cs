﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Hooks;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Animation.Components {
    public class KsAnimationComponent {

        public Vector3F CollisionBox { get; } = new Vector3F(0.5F, 0.5F, 0.5F);
        public Vector3F Offset { get; } = Vector3F.Zero;

        private readonly List<IAnimationComponent> _animationComponents = new List<IAnimationComponent>();

        public IReadOnlyList<IAnimationComponent> AnimationComponents => _animationComponents;

        public readonly IAnimationComponent Rendering;

        private Blob Config { get; }

        private string Code { get; }

        public bool Instance { get; }

        public KsAnimationComponent(string code, Blob config) {
            if (config.Contains("animations")) {
                foreach (var entry in config.GetList("animations")) {
                    if (entry.Kind == BlobEntryKind.Blob) {
                        var current = entry.Blob();

                        if (current.Contains("type")) {
                            if (KitsuneAnimationHook.Builders.Any(x => x.Value.Kind() == current.GetString("type"))) {
                                var component = KitsuneAnimationHook.Builders
                                    .First(x => x.Value.Kind() == current.GetString("type")).Value.Instance(code, current);
                                _animationComponents.Add(component);

                                if (component.HandleRendering) {
                                    Rendering = component;
                                }

                                if (current.GetBool("randomStart", false)) {
                                    component.SetRandomFrame();
                                }
                            }
                        }
                    }
                }
            }

            Instance = config.GetBool("instance", false);

            if (Instance) {
                Code = code;
                Config = config.Clone();
            }
        }

        public KsAnimationComponent GetInstance() {
            if (Instance) {
                return new KsAnimationComponent(Code, Config);
            } else {
                return null;
            }
        }
    }
}
