﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Components {
    public class ColorPulse : IAnimationComponent, IColorReplacePreload {
        private Color Target { get; }

        private int DarkRange { get; }
        private int LightRange { get; }

        private long Delay { get; }

        private Color Paint { get; }

        private int IconRange { get; set; }
        private int CompactRange { get; set; }
        private int SecondaryRange { get; set; }

        private bool _iconAdd = true;
        private bool _compactAdd = true;
        private bool _secondaryAdd = true;

        private DateTime _lastIconRun = DateTime.MinValue;
        private DateTime _lastCompactRun = DateTime.MinValue;
        private DateTime _lastSecondaryRun = DateTime.MinValue;

        public ColorPulse(Blob config) {
            if (!config.Contains("target")) {
                throw new BlobValueException("No target color defined");
            }

            Target = config.GetColor("target");

            Paint = config.Contains("paint") ? config.GetColor("paint") : Target;

            DarkRange = (int)config.GetLong("darkRange", -20);
            LightRange = (int)config.GetLong("lightRange", 20);
            Delay = config.GetLong("delay", 20);
            IconRange = DarkRange;
            CompactRange = DarkRange;
            SecondaryRange = DarkRange;
        }

        private void ProgressIconRange() {
            if (_iconAdd) {
                IconRange++;
                if (IconRange >= LightRange) {
                    IconRange = LightRange;
                    _iconAdd = false;
                }
            } else {
                IconRange--;
                if (IconRange <= DarkRange) {
                    IconRange = DarkRange;
                    _iconAdd = true;
                }
            }
        }

        private bool CanIconRun() {
            return _lastIconRun <= DateTime.Now.AddMilliseconds(-Delay);
        }

        private void UpdateIconDelay() {
            _lastIconRun = DateTime.Now;
        }

        public void SetRandomFrame() {
            CompactRange = GameContext.RandomSource.Next(DarkRange, LightRange);
            IconRange = GameContext.RandomSource.Next(DarkRange, LightRange);
            SecondaryRange = GameContext.RandomSource.Next(DarkRange, LightRange);
        }

        public void ItemColorUpdate(Item item, ColorReplace colorReplace, RenderMode renderMode) {
            var r = 0;
            var g = 0;
            var b = 0;
            r = Paint.R + IconRange;
            g = Paint.G + IconRange;
            b = Paint.B + IconRange;
            if (CanIconRun() && renderMode == RenderMode.Normal) {
                UpdateIconDelay();
                ProgressIconRange();
            }

            var newColor = new Color(
                r > 255 ? 255 : r < 0 ? 0 : r,
                g > 255 ? 255 : g < 0 ? 0 : g,
                b > 255 ? 255 : b < 0 ? 0 : b
            );

            colorReplace.Replace(Target, newColor);
        }

        public Matrix4F ItemRenderUpdate(Item item, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public void TileColorUpdate(TileConfiguration configuration, ColorReplace colorReplace, RenderMode renderMode) {
            var r = 0;
            var g = 0;
            var b = 0;
            r = Paint.R + IconRange;
            g = Paint.G + IconRange;
            b = Paint.B + IconRange;
            if (CanIconRun()) {
                UpdateIconDelay();
                ProgressIconRange();
            }

            var newColor = new Color(
                r > 255 ? 255 : r < 0 ? 0 : r,
                g > 255 ? 255 : g < 0 ? 0 : g,
                b > 255 ? 255 : b < 0 ? 0 : b
            );

            colorReplace.Replace(Target, newColor);
        }
        public Matrix4F TileRenderUpdate(Entity entity, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public void SubRenderColorUpdate(ColorReplace colorReplace, RenderMode renderMode) {
            var r = 0;
            var g = 0;
            var b = 0;
            r = Paint.R + IconRange;
            g = Paint.G + IconRange;
            b = Paint.B + IconRange;
            if (CanIconRun()) {
                UpdateIconDelay();
                ProgressIconRange();
            }

            var newColor = new Color(
                r > 255 ? 255 : r < 0 ? 0 : r,
                g > 255 ? 255 : g < 0 ? 0 : g,
                b > 255 ? 255 : b < 0 ? 0 : b
            );

            colorReplace.Replace(Target, newColor);
        }
        public Matrix4F SubRenderRenderUpdate(ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            offset = Vector3F.Zero;
            return matrix;
        }

        public bool TileCompatible => true;
        public bool ItemCompatible => true;
        public bool SubRenderCompatible => true;
        public bool HandleRendering => false;
        public void Render(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderIcon(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderInWorldFullSized(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderTile(DeviceContext graphics, ref Matrix4F matrix) { }

        public List<Dictionary<Color, Color>> Preload() {
            var output = new List<Dictionary<Color, Color>>();

            for (var i = DarkRange; i <= LightRange; i++) {
                var item = new Dictionary<Color, Color>();
                var r = Paint.R + i;
                var g = Paint.G + i;
                var b = Paint.B + i;

                var newColor = new Color(
                    r > 255 ? 255 : r < 0 ? 0 : r,
                    g > 255 ? 255 : g < 0 ? 0 : g,
                    b > 255 ? 255 : b < 0 ? 0 : b
                );
                item.Add(Target, newColor);

                output.Add(item);
            }

            return output;
        }
    }
}
