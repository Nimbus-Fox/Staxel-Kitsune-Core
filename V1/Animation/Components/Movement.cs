﻿using System;
using System.Collections.Generic;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Animation.Components {
    public class Movement : IAnimationComponent {

        private Vector3F _current;
        private List<Vector3F> _destinations { get; } = new List<Vector3F> {new Vector3F(0, 0, 0)};
        private int _index = 0;
        private float _step;
        private DateTime _lastRun = DateTime.MinValue;
        private long _delay;

        public Movement(Blob config) {
            if (config.Contains("destinations")) {
                var list = config.GetList("destinations");

                foreach (var item in list) {
                    _destinations.Add(item.Blob().GetVector3F());
                }
            }

            _step = (float) config.GetDouble("step", 0.005);

            _delay = config.GetLong("delay", 20);

            _current = _destinations[0];
        }

        public void SetRandomFrame() {
            _index = GameContext.RandomSource.Next(0, _destinations.Count - 1);

            _current = _index == _destinations.Count - 1 ? _destinations[0] : _destinations[_index + 1];
        }

        public void ItemColorUpdate(Item item, ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F ItemRenderUpdate(Item item, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay) && renderMode == RenderMode.Normal) {
                if (_current != _destinations[_index]) {

                    _current = _current + new Vector3F(
                                   _current.X < _destinations[_index].X ? _step : _current.X > _destinations[_index].X ? -_step : 0,
                                   _current.Y < _destinations[_index].Y ? _step : _current.Y > _destinations[_index].Y ? -_step : 0,
                                   _current.Z < _destinations[_index].Z ? _step : _current.Z > _destinations[_index].Z ? -_step : 0
                               );

                    if (_destinations[_index].X - _current.X > -_step &&
                        _destinations[_index].X - _current.X < _step) {
                        _current.X = _destinations[_index].X;
                    }

                    if (_destinations[_index].Y - _current.Y > -_step &&
                        _destinations[_index].Y - _current.Y < _step) {
                        _current.Y = _destinations[_index].Y;
                    }

                    if (_destinations[_index].Z - _current.Z > -_step &&
                        _destinations[_index].Z - _current.Z < _step) {
                        _current.Z = _destinations[_index].Z;
                    }

                    _lastRun = DateTime.Now;
                } else {
                    _index++;

                    if (_index >= _destinations.Count) {
                        _index = 0;
                    }
                }
            }

            offset = _current;
            return matrix;
        }

        public void TileColorUpdate(TileConfiguration configuration, ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F TileRenderUpdate(Entity entity, ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay) && renderMode == RenderMode.Normal) {
                if (_current != _destinations[_index]) {

                    _current = _current + new Vector3F(
                                   _current.X < _destinations[_index].X ? _step : _current.X > _destinations[_index].X ? -_step : 0,
                                   _current.Y < _destinations[_index].Y ? _step : _current.Y > _destinations[_index].Y ? -_step : 0,
                                   _current.Z < _destinations[_index].Z ? _step : _current.Z > _destinations[_index].Z ? -_step : 0
                    );

                    if (_destinations[_index].X - _current.X > -_step &&
                        _destinations[_index].X - _current.X < _step) {
                        _current.X = _destinations[_index].X;
                    }

                    if (_destinations[_index].Y - _current.Y > -_step &&
                        _destinations[_index].Y - _current.Y < _step) {
                        _current.Y = _destinations[_index].Y;
                    }

                    if (_destinations[_index].Z - _current.Z > -_step &&
                        _destinations[_index].Z - _current.Z < _step) {
                        _current.Z = _destinations[_index].Z;
                    }

                    _lastRun = DateTime.Now;
                } else {
                    _index++;

                    if (_index >= _destinations.Count) {
                        _index = 0;
                    }
                }
            }

            offset = _current;

            return matrix;
        }

        public void SubRenderColorUpdate(ColorReplace colorReplace, RenderMode renderMode) { }
        public Matrix4F SubRenderRenderUpdate(ref Matrix4F matrix, out Vector3F offset, RenderMode renderMode) {
            if (_lastRun < DateTime.Now.AddMilliseconds(-_delay) && renderMode == RenderMode.Normal) {
                if (_current != _destinations[_index]) {

                    _current = _current + new Vector3F(
                                   _current.X < _destinations[_index].X ? _step : _current.X > _destinations[_index].X ? -_step : 0,
                                   _current.Y < _destinations[_index].Y ? _step : _current.Y > _destinations[_index].Y ? -_step : 0,
                                   _current.Z < _destinations[_index].Z ? _step : _current.Z > _destinations[_index].Z ? -_step : 0
                               );

                    if (_destinations[_index].X - _current.X > -_step &&
                        _destinations[_index].X - _current.X < _step) {
                        _current.X = _destinations[_index].X;
                    }

                    if (_destinations[_index].Y - _current.Y > -_step &&
                        _destinations[_index].Y - _current.Y < _step) {
                        _current.Y = _destinations[_index].Y;
                    }

                    if (_destinations[_index].Z - _current.Z > -_step &&
                        _destinations[_index].Z - _current.Z < _step) {
                        _current.Z = _destinations[_index].Z;
                    }

                    _lastRun = DateTime.Now;
                } else {
                    _index++;

                    if (_index >= _destinations.Count) {
                        _index = 0;
                    }
                }
            }

            offset = _current;
            return matrix;
        }

        public bool TileCompatible => true;
        public bool ItemCompatible => true;
        public bool SubRenderCompatible => true;
        public bool HandleRendering => false;
        public void Render(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderIcon(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderInWorldFullSized(DeviceContext graphics, ref Matrix4F matrix) { }
        public void RenderTile(DeviceContext graphics, ref Matrix4F matrix) { }
    }
}
