﻿using System;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Rendering;

namespace NimbusFox.KitsuneCore.V1.Animation.Items.Renderers {
    public class AnimatedCraftItemRenderer : ItemRenderer {
        public override void Render(Item item, DeviceContext graphics, ref Matrix4F itemMatrix, Vector3D playerPosition, Vector3D renderOrigin,
            ref Matrix4F matrix, Timestep renderTimestep, string animation, float animationCycle, RenderMode renderMode) {
            if (_isDisposed) {
                throw new ObjectDisposedException(nameof(AnimatedCraftItemRenderer));
            }

            if (item.Configuration.Disposed) {
                throw new ObjectDisposedException("ItemConfiguration");
            }

            var vector3F = (playerPosition - renderOrigin).ToVector3F();
            var matrix4F = Matrix4F.CreateTranslation(item.Configuration.UsageOffset);
            matrix4F = matrix4F.Multiply(ref item.Configuration.RotationMatrix);
            matrix4F = matrix4F.Scale(item.Configuration.InHandScale);
            matrix4F = matrix4F.Multiply(ref itemMatrix);
            matrix4F = matrix4F.Translate(vector3F);
            var matrix1 = matrix4F.Multiply(ref matrix);

            if (item is IAnimatedItem animatedItem) {
                var matrix2 = Matrix4F.Identity;

                var offset = Vector3F.Zero;

                foreach (var component in animatedItem.AnimationComponent.AnimationComponents) {
                    if (component.ItemCompatible) {
                        matrix2 = component.ItemRenderUpdate(item, ref matrix2, out var _offset, renderMode);
                        offset = offset + _offset;
                    }
                }

                matrix2 = matrix2.Translate(offset).Multiply(ref matrix1);

                if (animatedItem.AnimationComponent.Rendering?.HandleRendering == true) {
                    animatedItem.AnimationComponent.Rendering.Render(graphics, ref matrix2);
                } else {
                    UpdateDrawable(item, renderMode);
                    animatedItem.IconDrawable?.Render(graphics, ref matrix2);
                }
            } else {
                base.Render(item, graphics, ref itemMatrix, playerPosition, renderOrigin, ref itemMatrix,
                    renderTimestep, animation, animationCycle, renderMode);
            }
        }

        public override void RenderIcon(Item item, DeviceContext graphics, ref Matrix4F itemMatrix) {
            if (_isDisposed) {
                throw new ObjectDisposedException(nameof(AnimatedCraftItemRenderer));
            }

            if (item is IAnimatedItem animatedItem) {
                var matrix1 = Matrix4F.Identity;

                var offset = Vector3F.Zero;

                foreach (var component in animatedItem.AnimationComponent.AnimationComponents) {
                    if (component.ItemCompatible) {
                        matrix1 = component.ItemRenderUpdate(item, ref matrix1, out var _offset, RenderMode.Normal);
                        offset = offset + _offset;
                    }
                }

                matrix1 = matrix1.Translate(offset).Multiply(ref itemMatrix);

                if (animatedItem.AnimationComponent.Rendering?.HandleRendering == true) {
                    animatedItem.AnimationComponent.Rendering.RenderIcon(graphics, ref matrix1);
                } else {
                    UpdateDrawable(item, RenderMode.Normal);
                    animatedItem.IconDrawable?.Render(graphics, ref matrix1);
                }
            } else {
                base.RenderIcon(item, graphics, ref itemMatrix);
            }
        }

        public override void RenderInWorldFullSized(Item item, DeviceContext graphics, ref Matrix4F matrix, bool compact, bool secondary) {
            if (_isDisposed) {
                throw new ObjectDisposedException(nameof(AnimatedCraftItemRenderer));
            }

            if (item is IAnimatedItem animatedItem) {
                var matrix1 = Matrix4F.Identity;

                var offset = Vector3F.Zero;

                foreach (var component in animatedItem.AnimationComponent.AnimationComponents) {
                    if (component.ItemCompatible) {
                        matrix1 = component.ItemRenderUpdate(item, ref matrix1, out var _offset, RenderMode.Normal);
                        offset = offset + _offset;
                    }
                }

                matrix1 = matrix1.Translate(offset).Multiply(ref matrix);

                if (animatedItem.AnimationComponent.Rendering?.HandleRendering == true) {
                    animatedItem.AnimationComponent.Rendering.RenderInWorldFullSized(graphics, ref matrix1);
                } else {
                    UpdateDrawable(item, RenderMode.Normal);
                    animatedItem.IconDrawable?.Render(graphics, ref matrix1);
                }
            } else {
                base.RenderInWorldFullSized(item, graphics, ref matrix, compact, secondary);
            }
        }
        private static readonly ColorReplace Cr = new ColorReplace();

        public void UpdateDrawable(Item item, RenderMode renderMode) {
            if (item is IAnimatedItem animatedItem) {
                Cr.Clear();
                foreach (var component in animatedItem.AnimationComponent.AnimationComponents) {
                    if (component.ItemCompatible) {
                        component.ItemColorUpdate(item, Cr, renderMode);
                    }
                }
                animatedItem.IconDrawable = VoxelRecolor.FetchDrawable(item, Cr.Colors, false, false);
            }
        }
    }
}
