﻿using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Components;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;
using Staxel.Items;
using CraftItem = NimbusFox.KitsuneCore.V1.Desealed.Staxel.Items.CraftItem;

namespace NimbusFox.KitsuneCore.V1.Animation.Items {
    public class AnimatedCraftItem : CraftItem, IAnimatedItem {
        public AnimatedCraftItem(IItemBuilderHelper builder) : base(builder) {
        }
        public KsAnimationComponent AnimationComponent { get; protected set; }
        public MultipleMatrixDrawable IconDrawable { get; set; }
        public MultipleMatrixDrawable CompactDrawable { get; set; }
        public MultipleMatrixDrawable SecondaryDrawable { get; set; }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            if (configuration.Components.Contains<KsAnimationComponent>()) {
                AnimationComponent = configuration.Components.Get<KsAnimationComponent>();

                if (AnimationComponent.Instance) {
                    AnimationComponent = AnimationComponent.GetInstance();
                }
            }
            base.Restore(configuration, blob);
        }
    }
}
