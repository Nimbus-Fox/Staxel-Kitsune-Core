﻿using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Animation.Items.Renderers;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.KitsuneCore.V1.Animation.Items.Builders {
    public class AnimatedCraftItemBuilder : IItemBuilderHelper {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }

        public void Load() {
            Renderer = new AnimatedCraftItemRenderer();
        }

        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            if (spare is AnimatedCraftItem) {
                if (spare.Configuration != null) {
                    spare.Restore(configuration, blob);
                    return spare;
                }
            }

            var bottle = new AnimatedCraftItem(this);
            bottle.Restore(configuration, blob);
            return bottle;
        }

        public string Kind() {
            return "nimbusfox.kitsunecore.animated.item.craftItem";
        }

        public ItemRenderer Renderer { get; set; }
    }
}
