﻿using System;
using System.Diagnostics;
using NimbusFox.KitsuneCore.V1.Hooks;
using NimbusFox.KitsuneCore.V1.Managers;
using NimbusFox.KitsuneCore.V1.Patches;
using Plukit.Base;
using Staxel.Server;
using WorldManager = NimbusFox.KitsuneCore.V1.Managers.WorldManager;

namespace NimbusFox.KitsuneCore.V1 {
    public class KitsuneCore {
        [Obsolete("Exception Manager is now redundant", true)]
        public ExceptionManager ExceptionManager { get; }
        public WorldManager WorldManager { get; }
        // ReSharper disable once MemberCanBeMadeStatic.Global
        public UserManager UserManager => KitsuneHook.UserManager;
        public DirectoryManager SaveDirectory { get; }
        public DirectoryManager ModDirectory { get; }
        public DirectoryManager ModsDirectory { get; }
        public DirectoryManager ConfigDirectory { get; }
        public DirectoryManager ContentDirectory { get; }
        // ReSharper disable once MemberCanBeMadeStatic.Global
        public ServerMainLoop ServerMainLoop => KitsuneHook.ServerMainLoop;

        public PatchController PatchController => _patchController ??
                                                  (_patchController = new PatchController(_patchControllerId));

        private PatchController _patchController;
        private readonly string _patchControllerId;

        private readonly string _author;
        private readonly string _mod;
        private readonly string _version;

        public bool IsLocalServer => KitsuneHook.ServerMainLoop != null && KitsuneHook.ServerMainLoop.isLocal();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="author"></param>
        /// <param name="mod">Must match your mod directory name</param>
        /// <param name="modVersion"></param>
        public KitsuneCore(string author, string mod, string modVersion = null) {
            _author = author;
            _mod = mod;
            _version = modVersion;
            WorldManager = new WorldManager();
            SaveDirectory = new DirectoryManager(author, mod);
            if (!modVersion.IsNullOrEmpty()) {
                SaveDirectory = SaveDirectory.FetchDirectoryNoParent(modVersion);
            }
            ModDirectory = new DirectoryManager(mod) { ContentFolder = true };
            ModsDirectory = new DirectoryManager { ContentFolder = true }.FetchDirectoryNoParent("content");
            ModsDirectory = ModsDirectory.FetchDirectoryNoParent("mods");
            ConfigDirectory = new DirectoryManager().FetchDirectoryNoParent("modConfigs").FetchDirectoryNoParent(mod);
            if (!modVersion.IsNullOrEmpty()) {
                ConfigDirectory = ConfigDirectory.FetchDirectoryNoParent(modVersion);
            }
            ContentDirectory = new DirectoryManager { ContentFolder = true }.FetchDirectoryNoParent("content");
            _patchControllerId = $"{_author}.{_mod}";
        }

        public void LogError(string text) {
            Console.ForegroundColor = ConsoleColor.Red;
            Logger.WriteLine($"[Error]{_author}.{_mod}.{_version}: {text}");
            Console.ResetColor();
        }

        [Obsolete("Unused function", true)]
        public void Log(string text, ConsoleColor color = default) {
        }

        public bool CanThrowException() {
            return Helpers.IsContentBuilder() || Debugger.IsAttached || ConfigDirectory.FileExists("throw.flag") ||
                   ConfigDirectory.FileExists("throw.flag.txt");
        }
    }
}
