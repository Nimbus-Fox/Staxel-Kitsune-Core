﻿using System;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Classes {
    [Serializable]
    public class VectorCubeI {
        public Vector3I Start { get; private set; }
        public Vector3I End { get; private set; }

        public VectorCubeI(Vector3I start, Vector3I end) {
            Start = Vector3I.Min(start, end);
            End = Vector3I.Max(start, end);
        }

        private VectorCubeI() { }

        public bool IsInside(Vector3I position) {
            return Start.X < position.X
                   && Start.Y < position.Y
                   && Start.Z < position.Z
                   && End.X > position.X
                   && End.Y > position.Y
                   && End.Z > position.Z;
        }

        public bool Contains(Vector3I position) {
            return Start.X <= position.X
                   && Start.Y <= position.Y
                   && Start.Z <= position.Z
                   && End.X >= position.X
                   && End.Y >= position.Y
                   && End.Z >= position.Z;
        }

        public bool IsInside(Vector3D position) {
            return Start.X < position.X
                   && Start.Y < position.Y
                   && Start.Z < position.Z
                   && End.X > position.X
                   && End.Y > position.Y
                   && End.Z > position.Z;
        }

        public bool Contains(Vector3D position) {
            return Start.X <= position.X
                   && Start.Y <= position.Y
                   && Start.Z <= position.Z
                   && End.X >= position.X
                   && End.Y >= position.Y
                   && End.Z >= position.Z;
        }

        public bool IsSide(Vector3I position) {
            return position.X >= Start.X
                   && position.X <= End.X
                   && position.Z >= Start.Z
                   && position.Z <= End.Z
                   && (Start.X == position.X
                || Start.Z == position.Z
                || End.X == position.X
                || End.Z == position.Z);
        }

        public long GetTileCount() {
            var x = End.X - Start.X;
            var y = End.Y - Start.Y;
            var z = End.Z - Start.Z;

            if (x == 0 && y == 0 && z == 0) {
                return 0;
            }

            return (x == 0 ? 1 : x) * (y == 0 ? 1 : y) * (z == 0 ? 1 : z);
        }

        public VectorCubeI GetOuterRegions() {
            return new VectorCubeI {
                Start = new Vector3I(Start.X - 1, Start.Y - 1, Start.Z - 1),
                End = new Vector3I(End.X + 1, End.Y + 1, End.Z + 1)
            };
        }
    }
}
