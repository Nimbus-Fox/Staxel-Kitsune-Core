﻿using System;
using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Voxel;


namespace NimbusFox.KitsuneCore.V1.Classes {
    public class UpScaleDrawable : IDisposable {
        public readonly MatrixDrawable Drawable;
        public readonly Vector3F Offsets;

        internal UpScaleDrawable(CompactVertexDrawable drawable, VoxelObject voxels, int scale) {
            var scaling = 16f / scale;
            Drawable = drawable.Matrix();

            VertexDrawableScaler.DetermineMinMax(drawable, out var min, out var max);

            Offsets = new Vector3F(-(voxels.Dimensions.X * scaling) / 2f, -min.Y * scaling,
                          -(voxels.Dimensions.Z * scaling) / 2f) / 16f;

            var offsets = new Vector3F(-(voxels.Dimensions.X) / 2f, -min.Y,
                              -(voxels.Dimensions.Z) / 2f) / 16f;

            Drawable = Drawable.Translate(offsets)
                .Scale(VertexDrawableScaler.DetermineScale(new Vector3F(16f, 16f, 16f), drawable, offsets))
                .Translate(-Offsets);
        }

        public void Dispose() {
            Drawable.Dispose();
        }
    }
}