﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.Timers;
using NimbusFox.KitsuneCore.V1.Classes.BlobRecord;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Classes {
    public class BlobDatabase : IDisposable {
        protected Blob Database;
        protected bool _needsStore;
        protected FileStream DatabaseFile;
        protected BinaryFormatter Bf = new BinaryFormatter();
        private Timer _timer;

        public void Dispose() {
            _timer?.Stop();
            _timer?.Dispose();
            ForceSave();
            Blob.Deallocate(ref Database);
            DatabaseFile.Dispose();
        }

        internal BlobDatabase() {

        }

        public BlobDatabase(FileStream stream, Action<string> errorLogger) {
            DatabaseFile = stream;
            Database = BlobAllocator.Blob(true);

            DatabaseFile.Seek(0L, SeekOrigin.Begin);

            try {
                Database.ReadJson(DatabaseFile.ReadAllText());

                DatabaseFile.Seek(0, SeekOrigin.Begin);

                File.WriteAllBytes(DatabaseFile.Name + ".bak", DatabaseFile.ReadAllBytes());
            } catch {
                if (File.Exists(DatabaseFile.Name + ".bak")) {
                    Console.ForegroundColor = ConsoleColor.Red;
                    errorLogger($"{DatabaseFile.Name} was corrupt. Will revert to backup file");
                    Console.ResetColor();

                    using (var ms = new MemoryStream(File.ReadAllBytes(DatabaseFile.Name + ".bak"))) {
                        Database.ReadJson(ms.ReadAllText());
                    }
                }
            }

            NeedsStore();
            Save();
        }

        public BlobDatabase(FileStream stream, Action<string> errorLogger, bool runBase = true) {
            DatabaseFile = stream;
            if (runBase) {
                Database = BlobAllocator.Blob(true);

                DatabaseFile.Seek(0L, SeekOrigin.Begin);

                try {
                    Database.ReadJson(DatabaseFile.ReadAllText());

                    DatabaseFile.Seek(0, SeekOrigin.Begin);

                    File.WriteAllBytes(DatabaseFile.Name + ".bak", DatabaseFile.ReadAllBytes());
                } catch {
                    if (File.Exists(DatabaseFile.Name + ".bak")) {
                        Console.ForegroundColor = ConsoleColor.Red;
                        errorLogger($"{DatabaseFile.Name} was corrupt. Will revert to backup file");
                        Console.ResetColor();

                        using (var ms = new MemoryStream(File.ReadAllBytes(DatabaseFile.Name + ".bak"))) {
                            Database.ReadJson(ms.ReadAllText());
                        }
                    }
                }

                NeedsStore();
                Save();
            }
        }

        public bool RecordExists(Guid guid) {
            return guid == Guid.Empty || Database.Contains(guid.ToString());
        }

        public T CreateRecord<T>() where T : BaseRecord {
            var guid = Guid.NewGuid();
            if (!typeof(BaseRecord).IsAssignableFrom(typeof(T))) {
                throw new BlobDatabaseRecordTypeException("The type given for T does not inherit BaseRecord");
            }

            if (RecordExists(guid)) {
                throw new BlobDatabaseRecordException("A record already exists with this guid");
            }
            NeedsStore();
            return (T)Activator.CreateInstance(typeof(T), new object[] { this, Database.FetchBlob(guid.ToString()), guid });
        }

        public T GetRecord<T>(Guid guid) where T : BaseRecord {
            if (!typeof(BaseRecord).IsAssignableFrom(typeof(T))) {
                throw new BlobDatabaseRecordTypeException("The type given for T does not inherit BaseRecord");
            }

            if (!RecordExists(guid)) {
                throw new BlobDatabaseRecordException("No record with this guid exists");
            }

            return (T)Activator.CreateInstance(typeof(T), this, Database.FetchBlob(guid.ToString()), guid);
        }

        public void RemoveRecord(Guid guid) {
            if (RecordExists(guid)) {
                Database.Delete(guid.ToString());
                NeedsStore();
            }
        }

        public T OverwriteRecord<T>(Guid guid, BaseRecord newRecord) where T : BaseRecord {
            if (!typeof(BaseRecord).IsAssignableFrom(typeof(T))) {
                throw new BlobDatabaseRecordTypeException("The type given for T does not inherit BaseRecord");
            }

            var record = RecordExists(guid) ? CreateRecord<T>() : GetRecord<T>(guid);

            record.Load(newRecord.CopyBlob());

            NeedsStore();
            Save();

            return record;
        }

        public void NeedsStore() {
            _needsStore = true;
        }

        public IReadOnlyList<T> SearchRecords<T>(Expression<Func<T, bool>> expression) where T : BaseRecord {
            var output = new List<T>();
            var func = expression.Compile();
            foreach (var key in Database.KeyValueIteratable.Keys) {
                var current = Database.GetBlob(key);

                if (current.Contains("_type")) {
                    if (current.GetString("_type") == typeof(T).FullName) {
                        var record = GetRecord<T>(Guid.Parse(key));

                        if (func(record)) {
                            output.Add(record);
                        }
                    }
                }
            }

            return output;
        }

        public void Save() {
            if (_needsStore) {
                _timer?.Stop();
                _timer?.Dispose();
                _timer = new Timer(1000) { AutoReset = false };
                _timer.Start();
                _timer.Elapsed += (sender, args) => {
                    ForceSave();
                    _timer?.Stop();
                };
            }
        }

        internal virtual void ForceSave() {
            DatabaseFile.SetLength(0);
            DatabaseFile.Position = 0;
            DatabaseFile.Flush(true);

            DatabaseFile.WriteString(Database.ToString());

            DatabaseFile.Flush(true);
            _needsStore = false;
        }
    }
}
