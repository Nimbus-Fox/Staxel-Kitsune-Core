﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Timers;
using Plukit.Base;
namespace NimbusFox.KitsuneCore.V1.Classes {
    public class BlobFile : IDisposable {
        protected readonly bool Binary;
        protected readonly FileStream FileStream;
        protected readonly Blob Blob;
        private Timer _timer;
        protected readonly BinaryFormatter Bf = new BinaryFormatter();

        protected bool IsStream => FileStream == null;

        protected BlobFile(FileStream stream, bool binary = false) {
            Blob = BlobAllocator.Blob(true);
            if (stream == null) {
                return;
            }

            FileStream = stream;
            Binary = binary;
            FileStream.Seek(0, SeekOrigin.Begin);

            if (FileStream.Length == 0) {
                if (File.Exists(FileStream.Name + ".bak")) {
                    if (binary) {
                        using (var ms = new MemoryStream(File.ReadAllBytes(FileStream.Name + ".bak"))) {
                            ms.Seek(0L, SeekOrigin.Begin);
                            using (var bfStream = new MemoryStream(Convert.FromBase64String(ms.ReadAllText()))) {
                                bfStream.Seek(0L, SeekOrigin.Begin);
                                Blob.ReadJson((string)Bf.Deserialize(bfStream));
                            }
                        }
                    } else {
                        Blob.ReadJson(File.ReadAllText(FileStream.Name + ".bak"));
                    }

                    Logger.WriteLine($"{FileStream.Name}.bak was loaded");
                } else {
                    ForceSave();
                }
                return;
            }

            try {
                if (binary) {
                    using (var bfStream = new MemoryStream(Convert.FromBase64String(FileStream.ReadAllText()))) {
                        bfStream.Seek(0L, SeekOrigin.Begin);
                        Blob.ReadJson((string)Bf.Deserialize(bfStream));
                    }
                } else {
                    Blob.ReadJson(FileStream.ReadAllText());
                }

                stream.Seek(0, SeekOrigin.Begin);

                File.WriteAllBytes(stream.Name + ".bak", stream.ReadAllBytes());
            } catch {
                Console.ForegroundColor = ConsoleColor.Red;
                Logger.WriteLine($"{FileStream.Name} was corrupt. Default values will be loaded");
                Console.ResetColor();
                try {
                    if (File.Exists(FileStream.Name + ".bak")) {
                        if (binary) {
                            using (var ms = new MemoryStream(File.ReadAllBytes(FileStream.Name + ".bak"))) {
                                ms.Seek(0L, SeekOrigin.Begin);
                                using (var bfStream = new MemoryStream(Convert.FromBase64String(ms.ReadAllText()))) {
                                    bfStream.Seek(0L, SeekOrigin.Begin);
                                    Blob.ReadJson((string)Bf.Deserialize(bfStream));
                                }
                            }
                        } else {
                            Blob.ReadJson(File.ReadAllText(FileStream.Name + ".bak"));
                        }

                        Logger.WriteLine($"{FileStream.Name}.bak was loaded");
                    }
                } catch {
                    // ignore
                }
            }
        }

        public void Load(Blob blob) {
            Blob.MergeFrom(blob);
            Save();
        }

        public void Dispose() {
            ForceSave();
            _timer?.Stop();
            _timer?.Dispose();
            FileStream?.Dispose();
        }

        public void Save() {
            _timer?.Stop();
            _timer?.Dispose();
            _timer = new Timer(5000) { AutoReset = false };
            _timer.Start();
            _timer.Elapsed += (sender, args) => {
                ForceSave();
            };
        }

        protected void ForceSave() {
            if (FileStream == null) {
                return;
            }
            FileStream.SetLength(0);
            FileStream.Position = 0;
            if (Binary) {
                using (var stream = new MemoryStream()) {
                    Bf.Serialize(stream, Blob.ToString());
                    stream.Seek(0L, SeekOrigin.Begin);
                    FileStream.WriteString(Convert.ToBase64String(stream.ToArray()));
                }
            } else {
                Blob.SaveJsonStream(FileStream);
            }
            FileStream.Flush(true);
        }

        public Blob CopyBlob() {
            return Blob.Clone();
        }
    }
}
