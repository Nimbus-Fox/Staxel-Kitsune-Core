﻿using System;
using System.Timers;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Classes.BlobRecord {
    public class BaseRecord {
        [JsonIgnore]
        private readonly BlobDatabase _database;
        [JsonIgnore]
        protected readonly Blob _blob;

        public Guid ID { get; }

        private string _type {
            get => _blob.GetString("_type", "");
            set => _blob.SetString("_type", value);
        }

        protected BaseRecord(BlobDatabase database, Blob blob, Guid id) {
            _database = database;
            _blob = blob;
            ID = id;
        }

        protected void Save() {
            _type = GetType().DeclaringType?.FullName ?? GetType().FullName;
            _database.NeedsStore();
            _database.Save();
        }

        public void Load(Blob blob) {
            _blob.MergeFrom(blob);
            Save();
        }

        public Blob CopyBlob() {
            return _blob.Clone();
        }
    }
}
