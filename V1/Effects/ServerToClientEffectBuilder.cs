﻿using Plukit.Base;
using Staxel.Effects;
using Staxel.Logic;

namespace NimbusFox.KitsuneCore.V1.Effects {
    internal class ServerToClientEffectBuilder : IEffectBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.kitsunecore.effect.serverToClient";
        }

        public IEffect Instance(Timestep step, Entity entity, EntityPainter painter, EntityUniverseFacade facade, Blob data,
            EffectDefinition definition, EffectMode mode) {
            return new ServerToClientEffect(data);
        }
    }
}
