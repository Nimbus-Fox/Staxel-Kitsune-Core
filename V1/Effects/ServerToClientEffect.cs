﻿using NimbusFox.KitsuneCore.V1.Hooks;
using Plukit.Base;
using Staxel.Draw;
using Staxel.Effects;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.KitsuneCore.V1.Effects {
    internal class ServerToClientEffect : IEffect {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public bool Completed() {
            return true;
        }

        public ServerToClientEffect(Blob data) {
            KitsuneHook.StartRestore(data);
        }

        public void Render(Entity entity, EntityPainter painter, Timestep renderTimestep, DeviceContext graphics, ref Matrix4F matrix,
            Vector3D renderOrigin, Vector3D position, RenderMode renderMode) { }

        public void Stop() { }
        public void Pause() { }
        public void Resume() { }
    }
}
