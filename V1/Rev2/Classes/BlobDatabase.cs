﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Rev2.Classes {
    public class BlobDatabase : V1.Rev1.Classes.BlobDatabase {
        private readonly bool _binary;

        /// <inheritdoc />
        public BlobDatabase(FileStream stream, bool binary, Action<string> errorLogger) {
            _binary = binary;

            Database = BlobAllocator.Blob(true);

            DatabaseFile = stream;

            DatabaseFile.Seek(0L, SeekOrigin.Begin);

            if (DatabaseFile.Length == 0) {
                NeedsStore();
                Save();
                return;
            }

            try {
                if (_binary) {
                    using (var gzStream = new GZipStream(DatabaseFile, CompressionMode.Decompress, true)) {
                        Database.ReadJson(gzStream.ReadAllText());
                    }
                } else {
                    Database.ReadJson(DatabaseFile.ReadAllText());
                }

                DatabaseFile.Seek(0L, SeekOrigin.Begin);

                File.WriteAllBytes(DatabaseFile.Name + ".bak", DatabaseFile.ReadAllBytes());
            } catch {
                if (!File.Exists(DatabaseFile.Name + ".bak")) {
                    NeedsStore();
                    Save();
                    return;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                errorLogger($"{DatabaseFile.Name} was corrupt. Will revert to backup file");

                using (var ms = new MemoryStream(File.ReadAllBytes(DatabaseFile.Name + ".bak"))) {
                    ms.Seek(0L, SeekOrigin.Begin);
                    if (_binary) {
                        using (var gzStream = new GZipStream(ms, CompressionMode.Decompress, true)) {
                            Database.ReadJson(gzStream.ReadAllText());
                        }
                    } else {
                        Database.ReadJson(ms.ReadAllText());
                    }
                }
            }

            NeedsStore();
            Save();
        }

        /// <inheritdoc />
        internal override void ForceSave() {
            DatabaseFile.SetLength(0);
            DatabaseFile.Position = 0;

            if (_binary) {
                    using (var gzStream = new GZipStream(DatabaseFile, CompressionLevel.Fastest, true)) {
                        var bytes = Encoding.UTF8.GetBytes(Database.ToString());
                        gzStream.Write(bytes, 0, bytes.Length);
                        gzStream.Flush();
                    }
            } else {
                DatabaseFile.WriteString(Database.ToString());
            }

            DatabaseFile.Flush(true);
            _needsStore = false;
        }
    }
}
