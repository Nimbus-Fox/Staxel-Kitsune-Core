﻿namespace NimbusFox.KitsuneCore.V1 {
    public static partial class Constants {
        public static partial class Fonts {
            public const string MyFirstCrush12 = "nimbusfox.ui.font.myfirstcrush.12";
            public const string MyFirstCrush24 = "nimbusfox.ui.font.myfirstcrush.24";
            public const string MyFirstCrush36 = "nimbusfox.ui.font.myfirstcrush.36";
        }

        public static partial class Backgrounds {
            public const string Button = "nimbusfox.ui.background.button";
            public const string Dark = "nimbusfox.ui.background.dark";
            public const string TextInput = "nimbusfox.ui.background.textInput";
        }

        public static partial class Images {
            public const string ColorWheel = "nimbusfox.ui.images.colorWheel";
        }
    }
}
