﻿using System;
using Jurassic;
using Jurassic.Library;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Jurassic {
    public class BlobInstance : ObjectInstance {
        private readonly Blob _blob;

        public BlobInstance(ScriptEngine engine, Blob blob) : base(engine) {
            _blob = blob;
            PopulateFunctions();
        }

        [JSFunction(Name = "setString")]
        public void SetString(string key, string value) {
            _blob.SetString(key, value);
        }

        [JSFunction(Name = "setLong")]
        public void SetLong(string key, int value) {
            _blob.SetLong(key, value);
        }

        [JSFunction(Name = "setDouble")]
        public void SetDouble(string key, double value) {
            _blob.SetDouble(key, value);
        }

        [JSFunction(Name = "setBool")]
        public void SetBool(string key, bool value) {
            _blob.SetBool(key, value);
        }

        [JSFunction(Name = "getString")]
        public string GetString(string key) {
            return _blob.GetString(key);
        }

        [JSFunction(Name = "getLong")]
        public int GetLong(string key) {
            return Convert.ToInt32(_blob.GetLong(key));
        }

        [JSFunction(Name = "getDouble")]
        public double GetDouble(string key) {
            return _blob.GetDouble(key);
        }

        [JSFunction(Name = "getBool")]
        public bool GetBool(string key) {
            return _blob.GetBool(key);
        }

        [JSFunction(Name = "fetchBlob")]
        public BlobInstance FetchBlob(string key) {
            return new BlobInstance(Engine, _blob.FetchBlob(key));
        }

        [JSFunction(Name = "containsKey")]
        public bool Contains(string key) {
            return _blob.Contains(key);
        }

        [JSFunction(Name = "delete")]
        public void Delete(string key) {
            _blob.Delete(key);
        }
    }
}
