﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using Harmony;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using NimbusFox.KitsuneCore.V1.Enums;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Input;
using Staxel.Items;
using Staxel.Tiles;
using Staxel.Voxel;
using Color = Microsoft.Xna.Framework.Color;

namespace NimbusFox.KitsuneCore.V1 {
    public static partial class Extensions {
        public static Item MakeItem(this Tile tile) {
            return tile.Configuration.MakeItem();
        }

        public static Item MakeItem(this TileConfiguration tile, string kind = "staxel.item.placer") {
            var itemBlob = BlobAllocator.Blob(true);
            itemBlob.SetString("kind", kind);
            itemBlob.SetString("tile", tile.Code);
            var item = GameContext.ItemDatabase.SpawnItemStack(itemBlob, null);
            Blob.Deallocate(ref itemBlob);

            if (item.IsNull()) {
                return Item.NullItem;
            }

            return item.Item;
        }

        public static void SetObject(this Blob blob, string key, object obj) {
            blob.FetchBlob(key).SetObject(obj);
        }

        public static void SetObject(this Blob blob, object obj) {
            var tempBlob = BlobAllocator.Blob(true);
            tempBlob.ReadJson(JsonConvert.SerializeObject(obj));
            blob.MergeFrom(tempBlob);
            Blob.Deallocate(ref tempBlob);
        }

        public static T GetObject<T>(this Blob blob, string key, T _default) where T : class {
            var value = blob.GetObject<T>(key);

            if (value == null) {
                return _default;
            }

            return value;
        }

        public static T GetObject<T>(this Blob blob, string key) where T : class {
            if (!blob.Contains(key)) {
                return null;
            }

            try {
                var tempBlob = blob.FetchBlob(key);

                return JsonConvert.DeserializeObject<T>(tempBlob.ToString());
            } catch when (!Debugger.IsAttached) {
                return null;
            }
        }

        public static T GetObject<T>(this Blob blob, T _default) where T : class {
            var value = blob.GetObject<T>();

            if (value == null) {
                return _default;
            }

            return value;
        }

        public static T GetObject<T>(this Blob blob) where T : class {
            try {
                return JsonConvert.DeserializeObject<T>(blob.ToString());
            } catch when (!Debugger.IsAttached) {
                return null;
            }
        }

        public static T GetPrivatePropertyValue<T>(this object parentObject, string field) {
            return (T)AccessTools.Property(parentObject.GetType(), field)?.GetValue(parentObject);
        }

        public static void SetPrivatePropertyValue(this object parentObject, string field, object value) {
            AccessTools.Property(parentObject.GetType(), field)?.SetValue(parentObject, value);
        }

        public static T GetPrivateFieldValue<T>(this object parentObject, string field) {
            return (T)AccessTools.Field(parentObject.GetType(), field)?.GetValue(parentObject);
        }

        public static void SetPrivateFieldValue(this object parentObject, string field, object value) {
            AccessTools.Field(parentObject.GetType(), field)?.SetValue(parentObject, value);
        }

        public static T GetPrivatePropertyValue<T>(this object parentObject, string field, Type type) {
            return (T)AccessTools.Property(type, field)?.GetValue(parentObject);
        }

        public static void SetPrivatePropertyValue(this object parentObject, string field, object value, Type type) {
            AccessTools.Property(type, field)?.SetValue(parentObject, value);
        }

        public static T GetPrivateFieldValue<T>(this object parentObject, string field, Type type) {
            return (T)AccessTools.Field(type, field)?.GetValue(parentObject);
        }

        public static void SetPrivateFieldValue(this object parentObject, string field, object value, Type type) {
            AccessTools.Field(type, field)?.SetValue(parentObject, value);
        }

        public static void RunPrivateVoid(this object parentObject, string method) {
            AccessTools.Method(parentObject.GetType(), method).Invoke(parentObject, new object[0]);
        }

        public static Vector2 Vector2(this MouseState mousestate) {
            return new Vector2(mousestate.X, mousestate.Y);
        }

        public static char? GetPressedKey(this IEnumerable<ScanCode> keys) {
            var reverseKeys = keys.Reverse();
            foreach (var key in reverseKeys) {
                var text = Enum.GetName(typeof(ScanCode), key);
                if (text?.Length <= 2 && text.Length > 0) {
                    var ch = text.Length == 2 && text[0] == 'D' ? text[1] : text[0];
                    if (char.IsLetterOrDigit(ch) || char.IsSymbol(ch)) {
                        return ch;
                    }
                }
            }

            return null;
        }

        public static ScanCode? GetDirectionKey(this IEnumerable<ScanCode> keys) {
            foreach (var key in keys) {
                if (key == ScanCode.Left ||
                    key == ScanCode.Right ||
                    key == ScanCode.Up ||
                    key == ScanCode.Down) {
                    return key;
                }
            }

            return null;
        }

        public static Compass GetDirection(this Heading input) {
            if (input.X < 0.75 && input.X > -0.75) {
                return Compass.NORTH;
            }

            if (input.X < -0.75 && input.X > -2.25) {
                return Compass.EAST;
            }

            if (input.X < 2.25 && input.X > 0.75) {
                return Compass.WEST;
            }

            return Compass.SOUTH;
        }

        public static byte FetchByte(this Blob blob, string key, byte _default = 0) {
            if (blob.Contains(key)) {
                var val = blob.GetLong(key, _default);

                if (byte.TryParse(val.ToString(), out var byt)) {
                    return byt;
                }
            }

            return _default;
        }

        public static int FetchInt(this Blob blob, string key, int _default = 0) {
            if (blob.Contains(key)) {
                var val = blob.GetLong(key, _default);

                if (int.TryParse(val.ToString(), out var byt)) {
                    return byt;
                }
            }

            return _default;
        }

        public static Guid FetchGuid(this Blob blob, string key) {
            return blob.FetchGuid(key, Guid.Empty);
        }

        public static Guid FetchGuid(this Blob blob, string key, Guid _default) {
            if (blob.Contains(key)) {
                var val = blob.GetString(key, _default.ToString());

                if (Guid.TryParse(val, out var byt)) {
                    return byt;
                }
            }
            return _default;
        }

        public static void SetGuid(this Blob blob, string key, Guid value) {
            blob.SetString(key, value.ToString());
        }

        public static void SetList(this Blob blob, string key, List<string> value) {
            if (blob.Contains(key)) {
                blob.Delete(key);
            }

            blob.PrepareStringList(key, value.Count);

            for (var i = 0; i < value.Count; i++) {
                blob.SetStringList(key, i, value[i]);
            }
        }

        public static List<string> FetchList(this Blob blob, string key, List<string> _default) {
            if (blob.Contains(key)) {
                if (blob.KeyValueIteratable[key].Kind == BlobEntryKind.List) {
                    return blob.GetStringList(key).ToList();
                }
            }
            return _default;
        }

        public static IReadOnlyDictionary<string, string> GetQuery(this string text) {
            var data = new Dictionary<string, string>();

            if (text.Contains("?")) {
                var queries = HttpUtility.ParseQueryString(text.Split('?')[1]);
                foreach (var key in queries.AllKeys) {
                    if (!data.ContainsKey(key)) {
                        data.Add(key.ToLower(), "");
                    }
                    data[key] = queries.Get(key);
                }
            }

            return data;
        }

        public static Color GetColor(this Blob blob, string key) {
            if (!blob.Contains(key)) {
                return Color.White;
            }

            return blob.KeyValueIteratable[key].GetColor();
        }

        public static Color GetColor(this Blob blob) {
            var r = blob.GetLong("R", Color.White.R);
            var g = blob.GetLong("G", Color.White.G);
            var b = blob.GetLong("B", Color.White.B);
            var a = blob.GetLong("A", Color.White.A);

            return new Color(r, g, b, a);
        }

        public static Color GetColor(this BlobEntry entry) {
            if (entry.Kind == BlobEntryKind.Blob) {
                var current = entry.Blob();

                return current.GetColor();
            }

            if (entry.Kind == BlobEntryKind.Int) {
                return uint.TryParse(entry.GetLong().ToString(), out var col) ? ColorMath.FromRgba(col) : Color.Transparent;
            }

            if (entry.Kind == BlobEntryKind.String) {
                var text = entry.GetString();

                if (text.Length <= 6) {
                    return ColorMath.ParseString(text);
                }

                var file = text;
                var x = 0;
                var y = 0;

                if (file.Contains('?')) {
                    var stringQuery = text.GetQuery();
                    file = file.Split('?')[0];

                    if (stringQuery.ContainsKey("x")) {
                        int.TryParse(stringQuery["x"], out x);
                    }

                    if (stringQuery.ContainsKey("y")) {
                        int.TryParse(stringQuery["y"], out y);
                    }
                }

                var bitmap = BitmapLoader.LoadFromStream(GameContext.ContentLoader.ReadStream(file));

                var col = bitmap.GetPixel(x, y);

                return new Color(col.R, col.G, col.B, col.A);
            }

            return Color.White;
        }

        public static void SetColor(this Blob blob, Color color) {
            blob.SetLong("R", color.R);
            blob.SetLong("G", color.G);
            blob.SetLong("B", color.B);
            blob.SetLong("A", color.A);
        }

        public static void SetColor(this Blob blob, string key, Color color) {
            SetColor(blob.FetchBlob(key), color);
        }

        public static void SaveQB(this VoxelObject qb, Stream stream) {
            stream.WriteByte(1);
            stream.WriteByte(1);
            stream.WriteByte(0);
            stream.WriteByte(0);

            stream.WriteUInt(0);
            stream.WriteUInt(0);
            stream.WriteUInt(1);
            stream.WriteUInt(0);
            stream.WriteUInt(1);

            stream.WriteString("main");

            stream.WriteInt(qb.Dimensions.X);
            stream.WriteInt(qb.Dimensions.Y);
            stream.WriteInt(qb.Dimensions.Z);
            stream.WriteInt(0);
            stream.WriteInt(0);
            stream.WriteInt(0);

            void Action(Color color, int run) {
                if (run <= 0) {
                    return;
                }

                var num = 1;
                if (run > 2) {
                    stream.WriteUInt(2);
                    stream.WriteUInt((uint)run);
                } else {
                    num = run;
                }

                for (var index = 0; index < num; index++) {
                    stream.WriteUInt(color.PackedValue);
                }
            }

            var color1 = Color.White;
            var num2 = 0;

            for (var num1 = qb.Dimensions.Z - 1; num1 >= 0; num1--) {
                for (var index = 0; index < qb.Dimensions.X * qb.Dimensions.Y; index++) {
                    var num3 = index % qb.Dimensions.X;
                    var num4 = index / qb.Dimensions.X;

                    var transparent =
                        qb.ColorData[num3 + num1 * qb.Dimensions.X + num4 * qb.Dimensions.X * qb.Dimensions.Z];
                    if (((int) transparent.PackedValue & -16777216) == 0) {
                        transparent = Color.Transparent;
                    } else {
                        transparent.PackedValue |= 4278190080U;
                    }

                    if (transparent != color1) {
                        Action(color1, num2);
                        num2 = 0;
                    }

                    color1 = transparent;
                    num2++;
                }
                Action(color1, num2);
                num2 = 0;
                stream.WriteUInt(6);
            }
        }
    }
}
