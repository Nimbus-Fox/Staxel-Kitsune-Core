﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Components;
using NimbusFox.KitsuneCore.V1.Hooks;
using NimbusFox.KitsuneCore.V1.Managers;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.KitsuneCore.V1 {
    public static class VoxelRecolor {
        private static readonly Dictionary<string, MultipleMatrixDrawable> MatrixCache = new Dictionary<string, MultipleMatrixDrawable>();
        private static readonly Dictionary<string, MatrixDrawable> VoxelCache = new Dictionary<string, MatrixDrawable>();

        private static readonly DirectoryManager CacheDirectory =
            KitsuneHook.KitsuneCore.ConfigDirectory.FetchDirectoryNoParent("renderCache");

        public static MultipleMatrixDrawable FetchDrawable(TileConfiguration configuration, Dictionary<Color, Color> colors, bool cache = false) {
            var matrix = configuration.Icon.Matrix();

            var component = configuration.Components.Select<VoxelRecolorComponent>().FirstOrDefault();

            if (component == default(VoxelRecolorComponent)) {
                return null;
            }

            foreach (var col in component.ColorCoords.Keys) {
                if (!colors.ContainsKey(col)) {
                    colors.Add(col, col);
                }
            }

            var key = $"{component.Id}.{JsonConvert.SerializeObject(colors)}";

            if (MatrixCache.ContainsKey(key)) {
                return MatrixCache[key];
            }

            var bundleCache = GameContext.AssetBundleManager.FindByExtension(".render");

            var blob = BlobAllocator.Blob(true);

            var stream = GameContext.ContentLoader.ReadStream(configuration.Source);

            stream.Seek(0L, SeekOrigin.Begin);
            blob.LoadJsonStream(stream);

            var voxels = VoxelLoader.LoadQb(GameContext.ContentLoader.ReadStream(blob.GetString("voxels")),
                blob.GetString("voxels"), Vector3I.Zero, Vector3I.MaxValue);

            var current = new MultipleMatrixDrawable(voxels);

            foreach (var color in colors) {
                loop:
                try {
                    var c = $"{component.Id}.{color.Key.PackedValue}.{color.Value.PackedValue}";
                    var o = $"{component.Id}.{color.Key.PackedValue}";
                    var cacheFile = $"{c}.render";
                    var origFile = $"{o}.qb";

                    if (VoxelCache.ContainsKey(c)) {
                        current.Add(VoxelCache[c]);
                        continue;
                    }

                    if (bundleCache.Any(x => x.Contains(cacheFile))) {
                        VoxelCache.Add(c,
                            (MatrixDrawable) Drawable.ReadFromStream(
                                GameContext.ContentLoader.ReadStream(bundleCache.First(x => x.Contains(cacheFile)))));
                        current.Add(VoxelCache[c]);
                    } else if (CacheDirectory.FileExists(cacheFile)) {
                        using (var ms = new MemoryStream()) {
                            FileStream fs = null;
                            read:
                            try {
                                fs = CacheDirectory.ObtainFileStream(cacheFile, FileMode.Open);
                            } catch {
                                goto read;
                            }
                            fs.Seek(0L, SeekOrigin.Begin);
                            fs.CopyTo(ms);
                            ms.Seek(0L, SeekOrigin.Begin);
                            VoxelCache.Add(c, (MatrixDrawable) Drawable.ReadFromStream(ms));
                            current.Add(VoxelCache[c]);
                            fs.Close();
                            fs.Dispose();
                        }
                    } else {
                        tileRedo:
                        if (CacheDirectory.FileExists(origFile)) {
                            using (var ms = new MemoryStream()) {
                                FileStream fs = null;
                                read:
                                try {
                                    fs = CacheDirectory.ObtainFileStream(origFile, FileMode.Open);
                                } catch {
                                    goto read;
                                }
                                fs.Seek(0L, SeekOrigin.Begin);
                                fs.CopyTo(ms);
                                ms.Seek(0L, SeekOrigin.Begin);
                                fs.Close();
                                fs.Dispose();

                                var qb = VoxelLoader.LoadQb(ms, origFile, Vector3I.Zero, Vector3I.MaxValue);

                                var coords = component.ColorCoords[color.Key];

                                foreach (var coord in coords) {
                                    qb.Write(coord.X, coord.Y, coord.Z, color.Value);
                                }

                                var dr = qb.BuildVertices().Matrix();
                                dr.Rotate(MathHelper.ToRadians(180), Vector3F.UnitY);

                                VoxelCache.Add(c, dr);
                                current.Add(VoxelCache[c]);

                                var prevCache = cache;

                                if (!cache) {
                                    cache = !CacheDirectory.FileExists(cacheFile);
                                }

                                if (cache) {
                                    using (var mms = new MemoryStream()) {
                                        fs = CacheDirectory.ObtainFileStream(cacheFile, FileMode.Create);
                                        dr.WriteToStream(mms);
                                        mms.Seek(0L, SeekOrigin.Begin);
                                        mms.CopyTo(fs);
                                        fs.Flush(true);
                                        fs.Close();
                                        fs.Dispose();
                                    }
                                }

                                cache = prevCache;
                            }
                        } else {
                            var vo = new VoxelObject(voxels.Dimensions, voxels.Offset, voxels.Min, voxels.Max,
                                new Color[voxels.ColorData.Length], true);

                            var coords = component.ColorCoords[color.Key];

                            foreach (var coord in coords) {
                                vo.Write(coord.X, coord.Y, coord.Z, color.Key);
                            }

                            using (var ms = new MemoryStream()) {
                                vo.SaveQB(ms);
                                ms.Seek(0L, SeekOrigin.Begin);

                                var fs = CacheDirectory.ObtainFileStream(origFile, FileMode.Create);
                                ms.CopyTo(fs);
                                fs.Flush(true);
                                fs.Close();
                                fs.Dispose();
                            }

                            goto tileRedo;
                        }
                    }
                } catch (IOException) {
                    goto loop;
                }
            }

            if (Helpers.IsClient() && matrix != null) {
                var voxelOffsets = new Vector3F(-voxels.Dimensions.X / 2f, -voxels.Min.Y, -voxels.Dimensions.Z / 2f);
                current.SetMatrix(Matrix4F.Identity.Translate(voxelOffsets / 16f).RotateUnitY180().Translate(-voxelOffsets / 16f));
            }
            
            MatrixCache.Add(key, current);

            return MatrixCache[key];
        }

        public static MultipleMatrixDrawable FetchDrawable(Item item, Dictionary<Color, Color> colors, bool secondary, bool compact, bool cache = false) {

            var matrix = item.Configuration.Icon as MatrixDrawable;

            var component = item.Configuration.Components.Select<VoxelRecolorComponent>().FirstOrDefault();

            if (component == default(VoxelRecolorComponent)) {
                return null;
            }

            var key = component.Id;

            key += JsonConvert.SerializeObject(colors);

            if (MatrixCache.ContainsKey(key)) {
                return MatrixCache[key];
            }

            var bundleCache = GameContext.AssetBundleManager.FindByExtension(".render");

            var voxels = component.Voxels;

            var current = new MultipleMatrixDrawable(voxels);

            foreach (var col in component.ColorCoords.Keys) {
                if (!colors.ContainsKey(col)) {
                    colors.Add(col, col);
                }
            }

            foreach (var color in colors) {
                loop:
                try {
                    var c = $"{component.Id}";
                    var o = $"{component.Id}.{color.Key.PackedValue}";

                    c += $".{color.Key.PackedValue}.{color.Value.PackedValue}";

                    var cacheFile = $"{c}.render";
                    var origFile = $"{o}.qb";

                    if (VoxelCache.ContainsKey(c)) {
                        current.Add(VoxelCache[c]);
                        continue;
                    }

                    if (bundleCache.Any(x => x.Contains(cacheFile))) {
                        VoxelCache.Add(c, (MatrixDrawable) Drawable.ReadFromStream(
                            GameContext.ContentLoader.ReadStream(bundleCache.First(x => x.Contains(cacheFile)))));
                        current.Add(VoxelCache[c]);
                    } else if (CacheDirectory.FileExists(cacheFile)) {
                        using (var ms = new MemoryStream()) {
                            FileStream fs = null;
                            read:
                            try {
                                fs = CacheDirectory.ObtainFileStream(cacheFile, FileMode.Open);
                            } catch {
                                goto read;
                            }
                            fs.Seek(0L, SeekOrigin.Begin);
                            fs.CopyTo(ms);
                            ms.Seek(0L, SeekOrigin.Begin);
                            VoxelCache.Add(c, (MatrixDrawable) Drawable.ReadFromStream(ms));
                            current.Add(VoxelCache[c]);
                            fs.Close();
                            fs.Dispose();
                        }
                    } else {
                        itemRedo:
                        if (CacheDirectory.FileExists(origFile)) {
                            using (var ms = new MemoryStream()) {
                                FileStream fs = null;
                                read:
                                try {
                                    fs = CacheDirectory.ObtainFileStream(origFile, FileMode.Open);
                                } catch {
                                    goto read;
                                }
                                fs.Seek(0L, SeekOrigin.Begin);
                                fs.CopyTo(ms);
                                ms.Seek(0L, SeekOrigin.Begin);
                                fs.Close();
                                fs.Dispose();

                                var qb = VoxelLoader.LoadQb(ms, origFile, Vector3I.Zero, Vector3I.MaxValue);

                                var coords = component.ColorCoords[color.Key];

                                foreach (var coord in coords) {
                                    qb.Write(coord.X, coord.Y, coord.Z, color.Value);
                                }

                                var dr = qb.BuildVertices().Matrix();
                                dr.Rotate(MathHelper.ToRadians(180), Vector3F.UnitY);

                                VoxelCache.Add(c, dr);
                                current.Add(VoxelCache[c]);

                                if (cache) {
                                    using (var mms = new MemoryStream()) {
                                        fs = CacheDirectory.ObtainFileStream(cacheFile, FileMode.Create);
                                        dr.WriteToStream(mms);
                                        mms.Seek(0L, SeekOrigin.Begin);
                                        mms.CopyTo(fs);
                                        fs.Flush(true);
                                        fs.Close();
                                        fs.Dispose();
                                    }
                                }
                            }
                        } else {
                            var vo = new VoxelObject(voxels.Dimensions, voxels.Offset, voxels.Min, voxels.Max,
                                new Color[voxels.ColorData.Length], true);

                            var coords = component.ColorCoords[color.Key];

                            foreach (var coord in coords) {
                                vo.Write(coord.X, coord.Y, coord.Z, color.Key);
                            }

                            using (var ms = new MemoryStream()) {
                                vo.SaveQB(ms);
                                ms.Seek(0L, SeekOrigin.Begin);

                                var fs = CacheDirectory.ObtainFileStream(origFile, FileMode.Create);
                                ms.CopyTo(fs);
                                fs.Flush(true);
                                fs.Close();
                                fs.Dispose();
                            }

                            goto itemRedo;
                        }
                    }
                } catch (IOException) {
                    goto loop;
                }
            }

            if (Helpers.IsClient() && matrix != null) {
                current.SetMatrix(matrix.MatrixValue);
            }

            MatrixCache.Add(key, current);

            return MatrixCache[key];
        }
    }
}
