﻿using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.KitsuneCore.V1.Components.Item {
    class VoxelRecolorItemComponentBuilder : IItemComponentBuilder {
        public string Kind() {
            return "voxelMapping";
        }

        public object Instance(BaseItemConfiguration item, Blob config) {
            return new VoxelRecolorComponent(item, config);
        }
    }
}
