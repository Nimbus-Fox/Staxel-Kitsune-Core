﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using NimbusFox.KitsuneCore.V1.Hooks;
using NimbusFox.KitsuneCore.V1.Managers;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;
using Staxel.Tiles;
using Staxel.Tiles.TileComponents;
using Staxel.Voxel;

namespace NimbusFox.KitsuneCore.V1.Components {
    public class VoxelRecolorComponent : ITileComponent {
        public string Tile { get; }
        public IReadOnlyDictionary<Color, List<Vector3I>> ColorCoords;
        public VoxelObject Voxels;
        public string Id;
        private static DirectoryManager _cacheDirectory;

        public VoxelRecolorComponent(BaseItemConfiguration conf, Blob config) {
            if (_cacheDirectory == null) {
                _cacheDirectory = KitsuneHook.KitsuneCore.ConfigDirectory.FetchDirectory("voxelCache");
            }

            if (config.Contains("tile")) {
                Tile = config.GetString("tile");
            }

            if (config.Contains("qbSource")) {
                Voxels = VoxelLoader.LoadQb(GameContext.ContentLoader.ReadStream(config.GetString("qbSource")),
                    config.GetString("qbSource"), Vector3I.Zero, Vector3I.MaxValue);
            }

            Id = config.GetString("id", conf.Code);

            if (_cacheDirectory.FileExists(Id + ".cache")) {
                var colors = JsonConvert.DeserializeObject<Dictionary<uint, List<Vector3I>>>(_cacheDirectory.ReadFile(Id + ".cache"));
                
                ColorCoords = colors.ToDictionary(color => ColorMath.FromRgba(color.Key), color => color.Value);;
            } else {
                var data = new Dictionary<Color, List<Vector3I>>();

                Helpers.VectorLoop(Vector3I.Zero, Voxels.Size, (x, y, z) => {
                    try {
                        var pos = new Vector3I(x, y, z);
                        var color = Voxels.ColorData[Helpers.GetVoxelIndex(pos, Voxels.Size)];

                        if (color.A == Color.Transparent.A) {
                            return;
                        }

                        if (!data.ContainsKey(color)) {
                            data.Add(color, new List<Vector3I>());
                        }

                        if (!data[color].Contains(pos)) {
                            data[color].Add(pos);
                        }
                    } catch {
                        // ignore
                    }
                });

                ColorCoords = data;

                if (_cacheDirectory.FileExists(Id + ".cache")) {
                    return;
                }

                var colors = new Dictionary<uint, List<Vector3I>>();

                foreach (var col in data) {
                    colors.Add(col.Key.PackedValue, col.Value);
                }

                try {
                    _cacheDirectory.WriteFile(Id + ".cache", JsonConvert.SerializeObject(colors));
                } catch (IOException) {
                    // ignore
                }
            }
        }

        public VoxelRecolorComponent(TileConfiguration conf, Blob config) {
            if (_cacheDirectory == null) {
                _cacheDirectory = KitsuneHook.KitsuneCore.ConfigDirectory.FetchDirectory("voxelCache");

                if (Helpers.IsContentBuilder()) {
                    foreach (var file in _cacheDirectory.Files) {
                        _cacheDirectory.DeleteFile(file);
                    }
                }
            }

            if (config.Contains("tile")) {
                Tile = config.GetString("tile");
            }

            if (config.Contains("qbSource")) {
                Voxels = VoxelLoader.LoadQb(GameContext.ContentLoader.ReadStream(config.GetString("qbSource")),
                    config.GetString("qbSource"), Vector3I.Zero, Vector3I.MaxValue);
            }

            Id = config.GetString("id", conf.Code);

            if (_cacheDirectory.FileExists(Id + ".cache")) {
                var colors = JsonConvert.DeserializeObject<Dictionary<uint, List<Vector3I>>>(_cacheDirectory.ReadFile(Id + ".cache"));
                var cords = new Dictionary<Color, List<Vector3I>>();
                foreach (var color in colors) {
                    cords.Add(ColorMath.FromRgba(color.Key), color.Value);
                }

                ColorCoords = cords;
            } else {
                var data = new Dictionary<Color, List<Vector3I>>();

                Helpers.VectorLoop(Vector3I.Zero, Voxels.Size, (x, y, z) => {
                    try {
                        var pos = new Vector3I(x, y, z);
                        var color = Voxels.ColorData[Helpers.GetVoxelIndex(pos, Voxels.Size)];

                        if (color.A == Color.Transparent.A) {
                            return;
                        }

                        if (!data.ContainsKey(color)) {
                            data.Add(color, new List<Vector3I>());
                        }

                        if (!data[color].Contains(pos)) {
                            data[color].Add(pos);
                        }
                    } catch {
                        // ignore
                    }
                });

                ColorCoords = data;

                var colors = new Dictionary<uint, List<Vector3I>>();

                foreach (var col in data) {
                    colors.Add(col.Key.PackedValue, col.Value);
                }

                _cacheDirectory.WriteFile(Id + ".cache", JsonConvert.SerializeObject(colors));
            }
        }

        public void Check() { }
    }
}
