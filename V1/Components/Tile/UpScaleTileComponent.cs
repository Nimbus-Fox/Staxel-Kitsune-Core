﻿using System.Collections.Generic;
using System.IO;
using NimbusFox.KitsuneCore.V1.Classes;
using NimbusFox.KitsuneCore.V1.Hooks;
using NimbusFox.KitsuneCore.V1.Tiles.UpScaleTileStateEntity;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.KitsuneCore.V1.Components.Tile {
    public class UpScaleTileComponent {
        private readonly IReadOnlyDictionary<int, UpScaleDrawable> UpScales;
        private int _highest = 16;
        public UpScaleTileComponent(Blob config, TileConfiguration tile) {
            var scales = new Dictionary<int, UpScaleDrawable>();
            
            if (config.GetBool("override", true)) {
                tile.UseCustomRendering = true;
                tile.TileStateKind = UpScaleTileStateBuilder.KindCode();
                tile.PrefetchTileState = true;
            }

            if (NimbusFox.KitsuneCore.Helpers.IsServer()) {
                UpScales = scales;
                return;
            }
            
            foreach (var scale in config.KeyValueIteratable) {
                if (int.TryParse(scale.Key, out var iScale)) {
                    if ((iScale % 16) == 0) {
                        if (scale.Value.Kind == BlobEntryKind.String) {
                            var data = GameContext.ContentLoader.ReadStream(scale.Value.GetString());

                            var voxels = VoxelLoader.LoadQb(data, scale.Value.GetString(), Vector3I.Zero,
                                Vector3I.MaxValue);

                            if (iScale > _highest) {
                                _highest = iScale;
                            }

                            CompactVertexDrawable drawable;

                            var fileName = $"{tile.Code}.{iScale}.cache";

                            if (UpScaleHook._scaleCache.FileExists(fileName)) {
                                using (var fs = UpScaleHook._scaleCache.ObtainFileStream(fileName, FileMode.Open)) {
                                    var buffer = fs.ReadAllBytes();
                                    drawable = CompactVertexDrawable.InstanceFromCompressedStream(buffer, 0,
                                        buffer.Length);
                                }
                            } else {
                                drawable = voxels.BuildVertices();

                                using (var fs = UpScaleHook._scaleCache.ObtainFileStream(fileName, FileMode.Create)) {
                                    using (var ms = new MemoryStream()) {
                                        drawable.CompressedWriteToStream(ms, true);
                                        ms.Seek(0L, SeekOrigin.Begin);
                                        ms.CopyTo(fs);
                                    }
                                    
                                    fs.Flush(true);
                                }
                            }
                            
                            scales.Add(iScale, new UpScaleDrawable(drawable, voxels, iScale));
                        }
                    }
                }
            }

            UpScales = scales;
        }

        public UpScaleDrawable GetDrawable() { 
            return UpScales[_highest];
        }
    }
}