﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Components.Tile {
    public class VoxelRecolorTileComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "voxelMapping";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new VoxelRecolorComponent(tile, config);
        }
    }
}
