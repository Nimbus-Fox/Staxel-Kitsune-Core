﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Components.Tile {
    public class UpScaleTileComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "upScaleTile";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new UpScaleTileComponent(config, tile);
        }
    }
}