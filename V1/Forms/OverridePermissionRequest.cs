﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using NimbusFox.KitsuneCore.V1.Hooks;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V1.Forms {
    public partial class OverridePermissionRequest : Form {
        public readonly Dictionary<string, bool> Permissions;
        private readonly FileStream _data;

        private static bool _first = true;

        public OverridePermissionRequest(IEnumerable<string> paths) {
            _data = KitsuneHook.KitsuneCore.ConfigDirectory.ObtainFileStream("overridePermissions.dat",
                FileMode.OpenOrCreate, FileAccess.ReadWrite);

            Visible = false;

            try {
                Permissions = JsonConvert.DeserializeObject<Dictionary<string, bool>>(GetJsonData(_data)) ??
                              new Dictionary<string, bool>();
            } catch {
                Permissions = new Dictionary<string, bool>();
            }

            var mods = paths.Select(x => {
                var parts = x.Split('/');

                return parts[1];
            });

            if (Helpers.IsServer()) {
                Close();
                return;
            }

            if (Helpers.IsContentBuilder()) {
                if (!_first) {
                    Close();
                    return;
                }

                _first = false;
            }

            if (Helpers.IsClient()) {
                if (mods.All(x => Permissions.ContainsKey(x))) {
                    Close();
                    return;
                }
            }

            InitializeComponent();

            foreach (var mod in mods) {
                if (!lstMods.Items.Contains(mod)) {
                    lstMods.Items.Add(mod, Permissions.ContainsKey(mod) && Permissions[mod]);
                }
            }

            if (lstMods.Items.Count <= 0) {
                Close();
                return;
            }

            Show();

            Visible = true;
        }

        private void BtnGrant_Click(object sender, EventArgs e) {
            Permissions.Clear();

            for (var i = 0; i < lstMods.Items.Count; i++) {
                Permissions.Add(lstMods.Items[i].ToString(), lstMods.GetItemChecked(i));
            }

            SetJsonData(JsonConvert.SerializeObject(Permissions), _data);
            _data.Flush(true);
            Close();
        }

        private static string GetJsonData(Stream data) {
            data.Seek(0, SeekOrigin.Begin);

            var sb = new StringBuilder();

            while (data.Position < data.Length) {
                sb.Append((char)data.ReadUIntBE());
            }

            return sb.ToString();
        }

        private static void SetJsonData(string json, Stream data) {
            data.SetLength(0);
            data.Seek(0, SeekOrigin.Begin);

            foreach (var ch in json) {
                data.WriteUIntBE(ch);
            }
        }

        private void OverridePermissionRequest_FormClosing(object sender, FormClosingEventArgs e) {
            _data?.Close();
            _data?.Dispose();
        }
    }
}
