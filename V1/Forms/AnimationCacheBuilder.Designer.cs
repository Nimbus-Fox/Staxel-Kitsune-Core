﻿namespace NimbusFox.KitsuneCore.V1.Forms {
    partial class AnimationCacheBuilder {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.prgrssModel = new System.Windows.Forms.ProgressBar();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblColor = new System.Windows.Forms.Label();
            this.prgrssColor = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // prgrssModel
            // 
            this.prgrssModel.Location = new System.Drawing.Point(12, 29);
            this.prgrssModel.Name = "prgrssModel";
            this.prgrssModel.Size = new System.Drawing.Size(301, 23);
            this.prgrssModel.TabIndex = 0;
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(12, 9);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(35, 13);
            this.lblModel.TabIndex = 1;
            this.lblModel.Text = "label1";
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.Location = new System.Drawing.Point(12, 55);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(35, 13);
            this.lblColor.TabIndex = 2;
            this.lblColor.Text = "label1";
            // 
            // prgrssColor
            // 
            this.prgrssColor.Location = new System.Drawing.Point(12, 71);
            this.prgrssColor.Name = "prgrssColor";
            this.prgrssColor.Size = new System.Drawing.Size(301, 23);
            this.prgrssColor.TabIndex = 3;
            // 
            // AnimationCacheBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 107);
            this.ControlBox = false;
            this.Controls.Add(this.prgrssColor);
            this.Controls.Add(this.lblColor);
            this.Controls.Add(this.lblModel);
            this.Controls.Add(this.prgrssModel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AnimationCacheBuilder";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kitsune Cache Builder";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar prgrssModel;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.ProgressBar prgrssColor;
    }
}