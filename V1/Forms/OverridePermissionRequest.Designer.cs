﻿namespace NimbusFox.KitsuneCore.V1.Forms {
    partial class OverridePermissionRequest {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lstMods = new System.Windows.Forms.CheckedListBox();
            this.lblText = new System.Windows.Forms.Label();
            this.btnGrant = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstMods
            // 
            this.lstMods.FormattingEnabled = true;
            this.lstMods.Location = new System.Drawing.Point(12, 35);
            this.lstMods.Name = "lstMods";
            this.lstMods.Size = new System.Drawing.Size(317, 169);
            this.lstMods.TabIndex = 0;
            // 
            // lblText
            // 
            this.lblText.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblText.Location = new System.Drawing.Point(0, 0);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(341, 31);
            this.lblText.TabIndex = 1;
            this.lblText.Text = "The following mods would like to run override scripts.\r\nKeep in mind these script" +
    "s may run the same risks as coded mods.";
            // 
            // btnGrant
            // 
            this.btnGrant.BackColor = System.Drawing.Color.Green;
            this.btnGrant.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnGrant.Location = new System.Drawing.Point(0, 216);
            this.btnGrant.Name = "btnGrant";
            this.btnGrant.Size = new System.Drawing.Size(341, 23);
            this.btnGrant.TabIndex = 2;
            this.btnGrant.Text = "Grant Permission";
            this.btnGrant.UseVisualStyleBackColor = false;
            this.btnGrant.Click += new System.EventHandler(this.BtnGrant_Click);
            // 
            // OverridePermissionRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(341, 239);
            this.ControlBox = false;
            this.Controls.Add(this.btnGrant);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.lstMods);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "OverridePermissionRequest";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Override Script Permissions";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OverridePermissionRequest_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox lstMods;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Button btnGrant;
    }
}