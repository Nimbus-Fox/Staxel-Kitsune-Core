﻿using System.Windows.Forms;

namespace NimbusFox.KitsuneCore.V1.Forms {
    public partial class AnimationCacheBuilder : Form {
        public AnimationCacheBuilder() {
            InitializeComponent();

            lblColor.Text = "";
            lblModel.Text = "";
        }

        public void SetColorMax(int max) {
            ResetColorValue();
            prgrssColor.Maximum = max;
        }

        public void ResetColorValue() {
            prgrssColor.Value = 0;
        }

        public void ProgressColor() {
            prgrssColor.Value += 1;
        }

        public int GetColorProgress() {
            return prgrssColor.Value;
        }

        public void SetColorText(string text) {
            lblColor.Text = text;
        }

        public void SetModelMax(int max) {
            ResetColorValue();
            prgrssModel.Maximum = max;
        }

        public void ResetModelValue() {
            prgrssModel.Value = 0;
        }

        public void ProgressModel() {
            prgrssModel.Value += 1;
        }

        public int GetModelProgress() {
            return prgrssModel.Value;
        }

        public void SetModelText(string text) {
            lblModel.Text = text;
        }
    }
}
