﻿using System.Linq;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using NimbusFox.KitsuneCore.V1.Hooks;
using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1 {
    public static class ChunkLoader {

        public static void AddChunk(Vector3I pos) {
            var key = new ChunkKey(pos);

            AddChunk(key);
        }

        public static void AddChunk(Vector3D pos) {
            var key = new ChunkKey(pos);

            AddChunk(key);
        }

        public static void AddChunk(ChunkKey key) {
            if (!ChunkLoaderHook.Instance.Chunks.Any(x => x.Contains(key.X, key.Y, key.Z))) {
                Logger.WriteLine($"[Kitsune ChunkLoader] Adding Chunk located at X:{key.X} Y:{key.Y} Z:{key.Z}");
                ChunkLoaderHook.Instance.Chunks.Add(key);
                KitsuneHook.KitsuneCore.SaveDirectory.WriteFile("chunks.db", JsonConvert.SerializeObject(ChunkLoaderHook.Instance.Chunks));
            }
        }

        public static void RemoveChunk(Vector3I pos) {
            var key = new ChunkKey(pos);

            RemoveChunk(key);
        }

        public static void RemoveChunk(Vector3D pos) {
            var key = new ChunkKey(pos);

            RemoveChunk(key);
        }

        public static void RemoveChunk(ChunkKey key) {
            if (ChunkLoaderHook.Instance.Chunks.Any(x => x.Contains(key.X, key.Y, key.Z))) {
                Logger.WriteLine($"[Kitsune ChunkLoader] Removing Chunk located at X:{key.X} Y:{key.Y} Z:{key.Z} from chunk loader");
                ChunkLoaderHook.Instance.Chunks.Remove(
                    ChunkLoaderHook.Instance.Chunks.First(x => x.Contains(key.X, key.Y, key.Z))
                );
                KitsuneHook.KitsuneCore.SaveDirectory.WriteFile("chunks.db", JsonConvert.SerializeObject(ChunkLoaderHook.Instance.Chunks));
            }
        }
    }
}
