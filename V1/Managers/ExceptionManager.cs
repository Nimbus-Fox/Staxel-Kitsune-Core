﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;

namespace NimbusFox.KitsuneCore.V1.Managers {
    public class ExceptionManager {
        private string _modVersion;
        private readonly DirectoryManager _errorsDir;
        private readonly string _author;
        private readonly string _mod;

        internal ExceptionManager(string author, string mod, string modVersion) {
            _errorsDir = new DirectoryManager().FetchDirectoryNoParent("modErrors").FetchDirectoryNoParent(mod);
            _modVersion = modVersion;
            _author = author;
            _mod = mod;
        }

        public void HandleException(Exception ex, Dictionary<string, object> extras = null) {
            var filename = DateTime.Now.Ticks;

            _errorsDir.WriteFile($"{filename}.{_modVersion ?? ""}error", JsonConvert.SerializeObject(ex, Formatting.Indented));

            if (extras != null) {
                if (extras.Any()) {
                    _errorsDir.WriteFile($"{filename}.{_modVersion ?? ""}data", JsonConvert.SerializeObject(extras, Formatting.Indented));
                }
            }

            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
        }
    }
}
