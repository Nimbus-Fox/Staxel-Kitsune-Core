﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V1.KeyBinds;
using NimbusFox.KitsuneCore.V2.UI;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Hooks {
    public class ModSettingsHook : IModHookV4 {
        public void Dispose() {
        }

        public ModSettingsHook() {
            var settingsWindow = new UIWindow();
            settingsWindow.Hide();
            settingsWindow.Container.SetBackground();
            settingsWindow.CloseOnEscape = false;
            settingsWindow.OnHide += () => { ClientContext.WebOverlayRenderer.ReleaseInputControl(); };

            var text = new UILabel(Position.MiddleMiddle);
            
            text.SetText("Hello world");
            text.Color = Color.Orange;
            text.SetFont();
            
            settingsWindow.Container.AddChild(text);
            
            ModKeyBindManager.RegisterKeyBind("nimbusfox.kitsuneCore.settingsMenu", Keys.OemCloseBrackets, () => {
                settingsWindow.Show();
                ClientContext.WebOverlayRenderer.AcquireInputControl();
            });
        }

        public void GameContextInitializeInit() {
        }

        public void GameContextInitializeBefore() {
            
        }

        public void GameContextInitializeAfter() {
            
        }

        public void GameContextDeinitialize() {
            
        }

        public void GameContextReloadBefore() {
            
        }

        public void GameContextReloadAfter() {
            
        }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            
        }

        public void UniverseUpdateAfter() {
            
        }

        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() {
            
        }

        public void ClientContextInitializeBefore() {
            
        }

        public void ClientContextInitializeAfter() {
            
        }

        public void ClientContextDeinitialize() {
            
        }

        public void ClientContextReloadBefore() {
            
        }

        public void ClientContextReloadAfter() {
            
        }

        public void CleanupOldSession() {
            
        }

        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}