﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V1.Animation.Components;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Classes;
using NimbusFox.KitsuneCore.V1.Components;
using NimbusFox.KitsuneCore.V1.Forms;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Hooks {
    internal class KitsuneAnimationHook : IModHookV4 {
        private static bool _delete = true;
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void GameContextInitializeInit() { }

        internal static readonly Dictionary<string, IAnimationComponentBuilder> Builders = new Dictionary<string, IAnimationComponentBuilder>();

        public void GameContextInitializeBefore() {
            Builders.Clear();
            foreach (var type in Helpers.GetTypesUsingBase<IAnimationComponentBuilder>()) {
                var builder = (IAnimationComponentBuilder)Activator.CreateInstance(type);
                if (Builders.ContainsKey(builder.Kind())) {
                    throw new ExistingAnimationKindException($"{builder.Kind()} already exists please check with the mod maker of: {type.FullName}");
                }

                Builders.Add(builder.Kind(), builder);
            }
            if (Helpers.IsContentBuilder() && _delete) {
                KitsuneHook.KitsuneCore.ConfigDirectory.DeleteDirectory("voxelCache", true);
                KitsuneHook.KitsuneCore.ConfigDirectory.DeleteDirectory("renderCache", true);
                _delete = false;
            }
        }

        private static AnimationCacheBuilder _form;
        private static int _total = 0;

        private static readonly Dictionary<Color, List<Color>> Replace = new Dictionary<Color, List<Color>>();

        public void GameContextInitializeAfter() {
            if (Helpers.IsServer()) {
                return;
            }
            HandleTileCache();
            HandleItemCache();
        }

        private void HandleItemCache() {
            try {
                _form = new AnimationCacheBuilder();
                _form.Show();
                _form.Hide();
            } catch {
                _form = null;
            }
            var items = GameContext.ItemDatabase.SearchByComponent<KsAnimationComponent>()
                .Where(x => x.Components.Select<VoxelRecolorComponent>().Any());

            _form?.SetModelMax(items.Count());

            foreach (var item in items) {
                var allow = new List<Color>();
                _form?.ProgressModel();
                Replace.Clear();
                Logger.WriteLine($"[KSAnimation] Generating/Loading cache for {item.Code}");
                _form?.SetModelText($"{item.Code} ({_form?.GetModelProgress():N0}/{items.Count():N0})");
                foreach (var anim in item.Components.Get<KsAnimationComponent>().AnimationComponents) {
                    if (anim is IColorReplacePreload pre) {
                        var output = pre.Preload();
                        if (output != null) {
                            if (output.Count > 0) {
                                foreach (var it in output) {
                                    foreach (var color in it) {
                                        if (!Replace.ContainsKey(color.Key)) {
                                            Replace.Add(color.Key, new List<Color>());
                                            Replace[color.Key].Add(color.Value);
                                            allow.Add(color.Key);
                                        } else if (Replace.ContainsKey(color.Key) && allow.Contains(color.Key)) {
                                            if (!Replace[color.Key].Contains(color.Value)) {
                                                Replace[color.Key].Add(color.Value);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (Replace.Count != 0) {
                    _total = 0;

                    foreach (var col in Replace) {
                        _total += col.Value.Count;
                    }

                    _form?.SetColorMax(_total);
                    _form?.Show();

                    var itemm = Helpers.MakeItem(item.Code);

                    foreach (var col in Replace) {
                        foreach (var colo in col.Value) {
                            _form?.ProgressColor();
                            _form?.SetColorText($"{_form?.GetColorProgress():N0} / {_total:N0}");
                            if (_form != null) {
                                Application.DoEvents();
                            }

                            var dic = new Dictionary<Color, Color> { { col.Key, colo } };

                            VoxelRecolor.FetchDrawable(itemm, dic, false, false, Helpers.IsContentBuilder());
                            VoxelRecolor.FetchDrawable(itemm, dic, true, false, Helpers.IsContentBuilder());
                            VoxelRecolor.FetchDrawable(itemm, dic, false, true, Helpers.IsContentBuilder());
                        }
                    }
                }
            }
            _form?.Close();
            _form?.Dispose();
        }

        private void HandleTileCache() {
            try {
                _form = new AnimationCacheBuilder();
                _form.Show();
                _form.Hide();
            } catch {
                _form = null;
            }

            var tiles = GameContext.TileDatabase.AllMaterials().Where(x =>
                x.Components.Select<VoxelRecolorComponent>().Any() &&
                x.Components.Select<KsAnimationComponent>().Any());

            _form?.SetModelMax(tiles.Count());

            foreach (var tile in tiles) {
                var allow = new List<Color>();
                _form?.ProgressModel();
                Replace.Clear();
                Logger.WriteLine($"[KSAnimation] Generating/Loading cache for {tile.Code}");
                _form?.SetModelText($"{tile.Code} ({_form?.GetModelProgress():N0}/{tiles.Count():N0})");
                foreach (var anim in tile.Components.Get<KsAnimationComponent>().AnimationComponents) {
                    if (anim is IColorReplacePreload pre) {
                        var output = pre.Preload();
                        if (output != null) {
                            if (output.Count > 0) {
                                foreach (var it in output) {
                                    foreach (var color in it) {
                                        if (!Replace.ContainsKey(color.Key)) {
                                            Replace.Add(color.Key, new List<Color>());
                                            Replace[color.Key].Add(color.Value);
                                            allow.Add(color.Key);
                                        } else if (Replace.ContainsKey(color.Key) && allow.Contains(color.Key)) {
                                            if (!Replace[color.Key].Contains(color.Value)) {
                                                Replace[color.Key].Add(color.Value);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (Replace.Count != 0) {
                    _total = 0;

                    foreach (var col in Replace) {
                        _total += col.Value.Count;
                    }

                    _form?.SetColorMax(_total);
                    _form?.Show();

                    foreach (var col in Replace) {
                        foreach (var colo in col.Value) {
                            _form?.ProgressColor();
                            _form?.SetColorText($"{_form?.GetColorProgress():N0} / {_total:N0}");
                            if (_form != null) {
                                Application.DoEvents();
                            }

                            var dic = new Dictionary<Color, Color> { { col.Key, colo } };

                            VoxelRecolor.FetchDrawable(tile, dic, Helpers.IsContentBuilder());
                        }
                    }
                }
            }
            _form?.Close();
            _form?.Dispose();
        }

        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }

        public void ClientContextInitializeAfter() {

        }

        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
