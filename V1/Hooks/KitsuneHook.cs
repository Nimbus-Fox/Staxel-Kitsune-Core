﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneCore.Interfaces;
using NimbusFox.KitsuneCore.V1.Effects;
using NimbusFox.KitsuneCore.V1.KeyBinds;
using NimbusFox.KitsuneCore.V1.Managers;
using NimbusFox.KitsuneCore.V1.Patches;
using Plukit.Base;
using Staxel;
using Staxel.Effects;
using Staxel.EntityStorage;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Server;
using Staxel.Tiles;

namespace NimbusFox.KitsuneCore.V1.Hooks {
    internal class KitsuneHook : ITailModHookV4 {

        internal static UserManager UserManager;
        internal static Universe Universe;
        internal static ServerMainLoop ServerMainLoop;
        internal static KitsuneCore KitsuneCore;
        internal static Entity _settingEntity;

        static KitsuneHook() {
            if (KitsuneCore == null) {
                KitsuneCore = new KitsuneCore("NimbusFox", "KitsuneCore");
                
                KitsuneCore.PatchController.Add(typeof(PlayerPersistence), "SaveAllPlayerDataOnConnect", null, null, typeof(KitsuneHook), nameof(OnConnect));
                KitsuneCore.PatchController.Add(typeof(PlayerPersistence), "SaveDisconnectingPlayer", null, null, typeof(KitsuneHook), nameof(OnDisconnect));
                BlobResourceLoaderPatches.Init();
            }
        }

        internal static void OnConnect(Entity entity) {
            if (entity == null) {
                return;
            }
            foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable>("_modHooks")) {
                if (modInstance.GetPrivateFieldValue<object>("_instance") is ITailModHook mod) {
                    mod.OnPlayerConnect(entity);
                }
            }
        }

        private static readonly List<Entity> Disconnects = new List<Entity>();

        internal static void OnDisconnect(Entity entity) {
            if (entity == null) {
                return;
            }
            if (!Disconnects.Contains(entity)) {
                Disconnects.Add(entity);
                return;
            }

            foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable>("_modHooks")) {
                if (modInstance.GetPrivateFieldValue<object>("_instance") is ITailModHook mod) {
                    mod.OnPlayerDisconnect(entity);
                }
            }

            Disconnects.Remove(entity);
        }

        public void Dispose() {
            _settingEntity?.Dispose();
            _settingEntity = null;
            
            ModKeyBindManager.Save();
        }

        public void GameContextInitializeInit() {
        }
        public void GameContextInitializeBefore() {

        }

        public void GameContextInitializeAfter() {

        }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }

        public void GameContextReloadAfter() {
        }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            Universe = universe;
            if (UserManager == null) {
                UserManager = new UserManager();
            }

            if (ServerMainLoop == null) {
                ServerMainLoop =
                    ServerContext.VillageDirector?.UniverseFacade?
                        .GetPrivateFieldValue<ServerMainLoop>("_serverMainLoop");
            }
        }

        public void UniverseUpdateAfter() {
            if (Universe.Server) {
                var blob = BlobAllocator.Blob(true);
                var hasVal = false;
                foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable>("_modHooks")) {
                    if (modInstance.GetPrivateFieldValue<object>("_instance") is ITailModHook mod) {
                        if (!blob.Contains(mod.GetType().FullName)) {
                            var current = blob.FetchBlob(mod.GetType().FullName);
                            mod.Store(current);
                            var val = current?.IsEmpty();
                            if (val == null || val.Value) {
                                blob.Delete(mod.GetType().FullName);
                                continue;
                            }

                            hasVal = true;
                        }
                    }
                }

                if (hasVal) {
                    var entity = KitsuneCore.WorldManager.Universe.AllEntities().First().Value;

                    entity.Effects.Trigger(new EffectTrigger(ServerToClientEffectBuilder.KindCode(), blob));
                }

                Blob.Deallocate(ref blob);
            }
        }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }

        public void CleanupOldSession() {
            _settingEntity?.Dispose();
            _settingEntity = null;
        }

        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }

        public void OnPlayerLoadAfter(Blob blob) { }
        public void OnPlayerSaveBefore(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerSaveAfter(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerConnect(Entity entity) {

        }
        public void OnPlayerDisconnect(Entity entity) { }
        public void Store(Blob blob) { }
        public void Restore(Blob blob) { }

        public static void StartRestore(Blob blob) {
            foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable>("_modHooks")) {
                if (modInstance.GetPrivateFieldValue<object>("_instance") is ITailModHook mod) {
                    if (blob.Contains(mod.GetType().FullName)) {
                        mod.Restore(blob.GetBlob(mod.GetType().FullName));
                    }
                }
            }
        }
    }
}
