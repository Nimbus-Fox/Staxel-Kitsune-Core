﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Jurassic;
using Jurassic.Library;
using NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json;
using NimbusFox.KitsuneCore.V1.Forms;
using NimbusFox.KitsuneCore.V1.Hooks;
using NimbusFox.KitsuneCore.V1.Jurassic;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;
using Logger = Plukit.Base.Logger;

public static class Overrides {

    internal static readonly ConcurrentDictionary<string, List<Action<Blob>>> _Overrides = new ConcurrentDictionary<string, List<Action<Blob>>>();

    public static void AddOverride(string path, Action<Blob> overrideFunction) {
        if (!_Overrides.ContainsKey(path)) {
            try {
                while (!_Overrides.TryAdd(path, new List<Action<Blob>>())) { }
            } catch {
                // ignore
            }
        }

        _Overrides[path].Add(overrideFunction);
    }

    public static void Log(string message) {
        Logger.WriteLine("[Override API Script] " + message);
    }

    public static bool ModExists(string modName) {
        return KitsuneHook.KitsuneCore.ModsDirectory.DirectoryExists(modName);
    }
}

namespace NimbusFox.KitsuneCore.V1.Hooks {

    public class OverrideHook : IModHookV4 {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }

        private static bool _first = true;

        private static ScriptEngine _jsEngine;

        public void GameContextInitializeInit() {
            if (_jsEngine == null) {
                _jsEngine = new ScriptEngine();

                _jsEngine.SetGlobalFunction("addOverride", new Action<string, FunctionInstance>((path, function) => {
                    Overrides.AddOverride(path, blob => { function.Call(null, new BlobInstance(_jsEngine, blob)); });
                }));
                _jsEngine.SetGlobalFunction("log", new Action<string>(Overrides.Log));
                _jsEngine.SetGlobalFunction("modExists", new Func<string, bool>(Overrides.ModExists));
            }

            Overrides._Overrides.Clear();

            var paths = GameContext.AssetBundleManager.FindByExtension(".overridecs").ToList();

            paths.AddRange(GameContext.AssetBundleManager.FindByExtension(".overridejs"));

            var overrides = new OverridePermissionRequest(paths);

            if (overrides.Visible) {
                Application.Run(overrides);
            }

            foreach (var path in paths) {
                var parts = path.Split('/');

                if (!overrides.Permissions.ContainsKey(parts[1])) {
                    continue;
                }

                if (!overrides.Permissions[parts[1]]) {
                    continue;
                }

                try {
                    var stream = GameContext.ContentLoader.ReadStream(path);

                    var scriptData = stream.ReadAllText();

                    try {
                        Thread scriptThread;
                        if (path.EndsWith(".overridecs")) {
                            var script = Helpers.CompileCSharpScript(scriptData, @"private void AddOverride(string path, System.Action<Blob> overrideFunction) {
    Overrides.AddOverride(path, overrideFunction);
}

private void Log(string message) {
    Overrides.Log(message);
}

private void ModExists(string modName) {
    Overrides.ModExists(modName);
}");
                            scriptThread = new Thread(() => { script.Init(); });
                        } else if (path.EndsWith(".overridejs")) {
                            var functionGuid = $"script{Guid.NewGuid().ToString().Replace("-", "")}";
                            scriptThread = new Thread(() => {
                                _jsEngine.Execute($@"function {functionGuid}() {{
    {scriptData}
}}
{functionGuid}();");
                            });
                            
                        } else {
                            continue;
                        }

                        var started = DateTime.Now;

                        scriptThread.Start();

                        while (scriptThread.ThreadState == ThreadState.Running &&
                               (DateTime.Now - started).TotalSeconds <= 3) { }

                        if (scriptThread.ThreadState == ThreadState.Running) {
                            scriptThread.Abort();
                        }
                    } catch (Exception ex) when (!KitsuneHook.KitsuneCore.CanThrowException()) {
                        Logger.LogException(new Exception($"{path} failed to execute correctly", ex));
                    }

                } catch (Exception ex) when (!KitsuneHook.KitsuneCore.CanThrowException()) {
                    Logger.LogException(new Exception($"{path} failed to compile correctly", ex));
                }
            }

            var count = 0;

            foreach (var c in Overrides._Overrides.Values) {
                count += c.Count;
            }

            if (count > 0) {
                Logger.WriteLine($"[Override API] loaded {count} overrides");
            }

            if (Helpers.IsContentBuilder() && _first) {
                _first = false;
                var stream = KitsuneHook.KitsuneCore.ConfigDirectory.ObtainFileStream("modelCache.dat",
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);

                stream.Seek(0, SeekOrigin.Begin);

                List<string> files;

                try {
                    files = JsonConvert.DeserializeObject<List<string>>(stream.ReadString()) ?? new List<string>();
                } catch {
                    files = new List<string>();
                }

                foreach (var file in files) {
                    var cachePath = Path.Combine(KitsuneHook.KitsuneCore.ContentDirectory.GetPath(), file) + ".cache";
                    if (File.Exists(cachePath)) {
                        Logger.WriteLine($"[Override API] Rebuilding cache for {file}");
                        File.Delete(cachePath);
                    }
                }

                files = Overrides._Overrides.Keys.ToList();

                foreach (var file in files) {
                    var cachePath = Path.Combine(KitsuneHook.KitsuneCore.ContentDirectory.GetPath(), file) + ".cache";
                    if (File.Exists(cachePath)) {
                        Logger.WriteLine($"[Override API] Rebuilding cache for {file}");
                        File.Delete(cachePath);
                    }
                }

                stream.SetLength(0);
                stream.Seek(0, SeekOrigin.Begin);
                stream.WriteString(JsonConvert.SerializeObject(files));
                stream.Flush(true);
                stream.Close();
                stream.Dispose();
            }
        }

        public void GameContextInitializeBefore() {

        }

        public void GameContextInitializeAfter() {

        }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
