﻿﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V1.Components.Tile;
 using NimbusFox.KitsuneCore.V1.Managers;
 using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.KitsuneCore.V1.Hooks {
    public class UpScaleHook : IModHookV4 {

        internal static DirectoryManager _scaleCache;
        
        public void Dispose() {
        }

        public void GameContextInitializeInit() {
        }

        public void GameContextInitializeBefore() {
            _scaleCache = KitsuneHook.KitsuneCore.ConfigDirectory.FetchDirectoryNoParent("scaleCache");
            while (!KitsuneHook.KitsuneCore.ConfigDirectory.DirectoryExists("scaleCache")) {
                _scaleCache = KitsuneHook.KitsuneCore.ConfigDirectory.FetchDirectoryNoParent("scaleCache");
            }

            if (NimbusFox.KitsuneCore.Helpers.IsContentBuilder()) {
                foreach (var file in _scaleCache.Files) {
                    _scaleCache.DeleteFile(file);
                }
            }
        }

        public void GameContextInitializeAfter() {
        }

        public void GameContextDeinitialize() {
        }

        public void GameContextReloadBefore() {
        }

        public void GameContextReloadAfter() {
        }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
        }

        public void UniverseUpdateAfter() {
        }

        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() {
        }

        public void ClientContextInitializeBefore() {
        }

        public void ClientContextInitializeAfter() {
        }

        public void ClientContextDeinitialize() {
        }

        public void ClientContextReloadBefore() {
        }

        public void ClientContextReloadAfter() {
        }

        public void CleanupOldSession() {
        }

        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}