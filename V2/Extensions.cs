﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harmony;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Color = Microsoft.Xna.Framework.Color;
using Enumerable = NimbusFox.KitsuneCore.Dependencies.Newtonsoft.Json.Utilities.LinqBridge.Enumerable;

namespace NimbusFox.KitsuneCore {
    public static partial class Extensions {
        public static Vector2 ToVector2(this MouseState mouse) {
            return new Vector2(mouse.X, mouse.Y);
        }

        public static Vector2 ToVector2(this Size size) {
            return new Vector2(size.Width, size.Height);
        }

        public static Texture2D ToTexture2D(this Bitmap image, DeviceContext context) {
            using (var ms = new MemoryStream()) {
                image.Save(ms, ImageFormat.Png);
                ms.Seek(0, SeekOrigin.Begin);
                return Texture2D.FromStream(context.Graphics.GraphicsDevice, ms);
            }
        }

        public static void SetColor(this Color[] colors, Vector2I size, Vector2I pos, Color color) {
            colors[(pos.Y * size.X) + pos.X] = color;
        }

        public static Color GetColor(this Color[] colors, Vector2I size, Vector2I pos) {
            return colors[(pos.Y * size.X) + pos.X];
        }

        public static object RunStaticVoid(this Type parentType, string method, params object[] arguments) {
            return parentType.RunStaticVoid<object>(method, arguments);
        }

        public static T RunStaticVoid<T>(this Type parentType, string method, params object[] arguments) {
            return (T)AccessTools.Method(parentType, method, arguments.Select(x => x?.GetType()).ToArray())
                .Invoke(null, arguments);
        }

        public static void Paint(this Texture2D canvas, Vector2I size, Color color, Vector2I location) {
            var canvasColors = new Color[canvas.Width * canvas.Height];
            var canvasSize = new Vector2I(canvas.Width, canvas.Height);
            canvas.GetData(canvasColors);
            Helpers.VectorLoop(location, (location + size) - Vector2I.One, loc => {
                if (loc.X > canvas.Width - 1 || loc.Y > canvas.Height - 1 ||
                    loc.X < 0 || loc.Y < 0) {
                    return;
                }

                canvasColors.SetColor(canvasSize, loc, color);
            });
            canvas.SetData(canvasColors);
        }

        public static void Paint(this Texture2D canvas, Texture2D toPaint, bool paintTransparent = false) {
            canvas.Paint(toPaint, Vector2I.Zero, paintTransparent);
        }

        public static void Paint(this Texture2D canvas, Texture2D toPaint, Vector2I location, bool paintTransparent = false) {
            if (canvas.Width < toPaint.Width || canvas.Height < toPaint.Height) {
                throw new Exception("The object you want to paint must be smaller or the same size as the canvas texture");
            }

            var canvasColors = new Color[canvas.Width * canvas.Height];
            var canvasSize = new Vector2I(canvas.Width, canvas.Height);
            canvas.GetData(canvasColors);

            var toPaintColors = new Color[toPaint.Width * toPaint.Height];
            var toPaintSize = new Vector2I(toPaint.Width, toPaint.Height);
            toPaint.GetData(toPaintColors);

            Helpers.VectorLoop(Vector2I.Zero, toPaintSize - Vector2I.One, loc => {
                var coords = loc + location;

                if (coords.X > canvas.Width - 1 || coords.Y > canvas.Height - 1 ||
                    coords.X < 0 || coords.Y < 0) {
                    return;
                }

                var col = toPaintColors.GetColor(toPaintSize, loc);

                if (!paintTransparent) {
                    if (col.A <= 10) {
                        return;
                    }
                } 

                canvasColors.SetColor(canvasSize, coords, col);
            });

            canvas.SetData(canvasColors);
        }

        public static void Clear(this Texture2D canvas, Color color) {
            var colors = new Color[canvas.Width * canvas.Height];

            for (var i = 0; i < colors.Length; i++) {
                colors[i] = color;
            }

            canvas.SetData(colors);
        }

        public static Color SwapRedAndBlueChannels(this Color color) {
            return new Color(color.B, color.G, color.R, color.A);
        }

        public static void HandleInherits(this Blob blob, bool combineLists) {
            if (!blob.Contains("__inherits")) {
                return;
            }

            if (blob.KeyValueIteratable["__inherits"].Kind != BlobEntryKind.String) {
                return;
            }
            
            HandleMerge(ref blob, blob.GetString("__inherits"), combineLists, (blb, combine) => {
                blb.HandleInherits(combine);
            });
        }

        private static void HandleMerge(ref Blob blob, string file, bool combineLists, Action<Blob, bool> parentLoop) {
            using (var fs = GameContext.ContentLoader.ReadStream(file)) {
                var parentBlob = BlobAllocator.Blob(true);
                parentBlob.ReadJson(fs.ReadAllText());

                parentLoop(parentBlob, combineLists);
                
                parentBlob.MergeFrom(blob, combineLists);

                var keys = blob.KeyValueIteratable.Keys;

                foreach (var key in keys) {
                    blob.Delete(key);
                }
                
                blob.AssignFrom(parentBlob);
                
                Blob.Deallocate(ref parentBlob);
            }
        }

        public static void HandleCustomInherits(this Blob blob, bool combineLists, string inheritKey = "__inherits") {
            if (!blob.Contains(inheritKey)) {
                return;
            }

            if (blob.KeyValueIteratable[inheritKey].Kind != BlobEntryKind.String &&
                blob.KeyValueIteratable[inheritKey].Kind != BlobEntryKind.List) {
                return;
            }

            string[] inherits;

            if (blob.KeyValueIteratable[inheritKey].Kind == BlobEntryKind.String) {
                inherits = new[] {blob.KeyValueIteratable[inheritKey].GetString()};
            } else {
                inherits = blob.KeyValueIteratable[inheritKey].List()
                    .Where(x => x.Kind == BlobEntryKind.String).Select(x => x.GetString()).ToArray();
            }

            foreach (var inherit in inherits) {
                HandleMerge(ref blob, inherit, combineLists, (blb, combine) => {
                    blb.HandleCustomInherits(combineLists, inheritKey);
                });
            }
        }
    }
}
