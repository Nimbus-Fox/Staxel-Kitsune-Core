﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using Plukit.Base;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.Classes {
    public class TextureRectangleDrawable : IDisposable {
        private readonly UIPicture _texture;
        private RenderTarget2D _rotation;
        private TextureVertexDrawable _drawable;
        private SpriteBatch _spriteBatch;

        public TextureRectangleDrawable(Vector2 size, UIPicture texture) : this(size, Color.White, texture) { }

        public TextureRectangleDrawable(Vector2 size, Color color, UIPicture texture) {
            _texture = texture;
            _drawable = Helpers.CreateRectangleDrawable(size, color,
                new Vector4F(0, 0, 1, 1f));
        }

        public void Render(DeviceContext context, ref Matrix4F matrix) {
            context.PushRenderState();
            context.PushShader();
            context.SetShader(context.GetShader("NameTag"));
            context.SetTexture(_texture.GetTexture(context, true));
            _drawable.Render(context, ref matrix);
            context.PopShader();
            context.PopRenderState();
        }

        public void Render(DeviceContext context, float radians, ref Matrix4F matrix) {
            var texture = _texture.GetTexture(context, true);
            if (_spriteBatch == null) {
                _spriteBatch = new SpriteBatch(context.Graphics.GraphicsDevice);
            }

            if (_rotation == null) {
                _rotation = new RenderTarget2D(context.Graphics.GraphicsDevice, texture.Width, texture.Height);
            }
            
            var data = new Color[texture.Width * texture.Height];
            var renderTargets = context.Graphics.GraphicsDevice.GetRenderTargets();
            context.Graphics.GraphicsDevice.SetRenderTarget(_rotation);
            context.PushRenderState();
            context.PushShader();
            context.SetShader(context.GetShader("NameTag"));
            _spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.LinearWrap,
                DepthStencilState.None, RasterizerState.CullCounterClockwise, null);
            _rotation.GetData(data);
            for (var index = 0; index < data.Length; index++) {
                data[index] = Color.Transparent;
            }
            _rotation.SetData(data);
            _spriteBatch.Draw(texture, new Vector2(texture.Width / 2f, texture.Height / 2f), null,
                Color.White, radians, new Vector2(texture.Width / 2f,  texture.Height / 2f), 1f,
                SpriteEffects.None, 0.0f);
            _spriteBatch.End();
            context.PopShader();
            context.PopRenderState();
            context.Graphics.GraphicsDevice.SetRenderTargets(renderTargets);
            context.PushRenderState();
            context.PushShader();
            context.SetShader(context.GetShader("NameTag"));
            context.SetTexture(_rotation);
            _drawable.Render(context, ref matrix);
            context.PopShader();
            context.PopRenderState();
        }

        /// <inheritdoc />
        public void Dispose() {
            _texture?.Dispose();
            _drawable?.Dispose();
            _rotation?.Dispose();
        }

        public void Redraw() {
            _texture?.Dispose();
            _rotation?.Dispose();
            _rotation = null;
        }
    }
}