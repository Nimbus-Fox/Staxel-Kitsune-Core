﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Staxel.Draw;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using NimbusFox.KitsuneCore.V2.UI.Enums;

namespace NimbusFox.KitsuneCore.V2.UI {
    public abstract class UIElement : IDisposable {
        protected readonly List<UIElement> ChildElements = new List<UIElement>();
        private UIWindow _window { get; set; }
        public UIWindow Window {
            get => _window;
            set {
                _window = value;
                foreach (var child in ChildElements) {
                    child.Window = value;
                }
            }
        }
        public UIElement Parent { get; internal set; }
        public Position ElementPosition { get; set; }
        protected ScreenOffsets Offsets = new ScreenOffsets();
        protected bool BaseOffSetReset = true;
        public UIRectangle Bounds { get; protected set; }
        public Vector2 ScrollOffset = Vector2.Zero;

        protected UIElement(Position elementPosition) {
            ElementPosition = elementPosition;
        }

        public virtual UIRectangle GetBounds(DeviceContext context) {
            var pos = GetPos(context);
            var size = GetSize(context);

            var offsets = Parent?.Offsets ?? Window.Offsets;

            switch (ElementPosition) {
                default:
                    pos += offsets.Top.Left;
                    break;
                case Position.TopMiddle:
                    pos += offsets.Top.Middle;
                    break;
                case Position.TopRight:
                    pos += offsets.Top.Right;
                    break;
                case Position.MiddleLeft:
                    pos += offsets.Middle.Left;
                    break;
                case Position.MiddleMiddle:
                    pos += offsets.Middle.Middle;
                    break;
                case Position.MiddleRight:
                    pos += offsets.Middle.Right;
                    break;
                case Position.BottomLeft:
                    pos += offsets.Bottom.Left;
                    break;
                case Position.BottomMiddle:
                    pos += offsets.Bottom.Middle;
                    break;
                case Position.BottomRight:
                    pos += offsets.Bottom.Right;
                    break;
            }

            pos -= Parent?.ScrollOffset ?? Vector2.Zero;

            return new UIRectangle(pos, size);
        }

        public virtual Vector2 GetSize(DeviceContext context) {
            var size = Vector2.Zero;

            foreach (var child in ChildElements) {
                if ((child.Window != Window || child.Parent != this) && child is UISpacer == false) {
                    continue;
                }
                var childSize = child.GetSize(context);

                if (childSize.X > size.X) {
                    size.X = childSize.X;
                }

                size.Y += childSize.Y;
            }

            return size;
        }

        public virtual void AddChild(UIElement element) {
            ChildElements.Add(element);
            element.Window = Window;
            element.Parent = this;
        }

        public virtual void RemoveChild(UIElement element) {
            ChildElements.Remove(element);
            element.Window = null;
            element.Parent = null;
        }

        private List<UIElement> _elementCache = new List<UIElement>();

        public virtual void Update(GameTime gameTime, KeyboardState keyboard, MouseState mouse, UIRectangle bounds, DeviceContext context) {
            if (BaseOffSetReset) {
                Offsets.Reset();
            }
            
            _elementCache.Clear();
            _elementCache = new List<UIElement>(ChildElements);
            Bounds = GetBounds(context);

            foreach (var child in _elementCache) {
                if ((child.Window != Window || child.Parent != this) && child is UISpacer == false) {
                    continue;
                }
                child.Update(gameTime, keyboard, mouse, Bounds, context);
                UpdateOffsets(child.GetSize(context), child.ElementPosition);
            }
        }

        public virtual void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor, Color? color = null) {
            var col = color ?? Color.White;
            foreach (var child in ChildElements) {
                if (child.Window != Window || child.Parent != this && child is UISpacer == false) {
                    continue;
                }
                child.Draw(context, spriteBatch, Bounds, scissor, col);
            }
        }

        /// <inheritdoc />
        public virtual void Dispose() {
            foreach (var child in ChildElements) {
                if (child.Window != Window || child.Parent != this && child is UISpacer == false) {
                    continue;
                }
                child.Dispose();
            }

            ChildElements.Clear();
            Parent = null;
            Window = null;
        }

        public virtual void Initialize(DeviceContext context) {
            foreach (var child in ChildElements) {
                if (child.Window != Window || child.Parent != this && child is UISpacer == false) {
                    continue;
                }
                child.Initialize(context);
            }
        }

        protected Vector2 GetPos(DeviceContext context) {
            var size = GetSize(context);
            var vector = Parent?.GetBounds(context) ?? Window.Bounds;
            switch (ElementPosition) {
                default:
                    return vector.TopLeft;
                case Position.TopMiddle:
                    return new Vector2(vector.Middle.X - (size.X / 2), vector.TopLeft.Y);
                case Position.TopRight:
                    return new Vector2(vector.BottomRight.X - size.X, vector.TopLeft.Y);
                case Position.MiddleLeft:
                    return new Vector2(vector.TopLeft.X, vector.Middle.Y - (size.Y / 2));
                case Position.MiddleMiddle:
                    return new Vector2(vector.Middle.X - (size.X / 2), vector.Middle.Y - (size.Y / 2));
                case Position.MiddleRight:
                    return new Vector2(vector.BottomRight.X - size.X, vector.Middle.Y - (size.Y / 2));
                case Position.BottomLeft:
                    return new Vector2(vector.TopLeft.X, vector.BottomRight.Y - size.Y);
                case Position.BottomMiddle:
                    return new Vector2(vector.Middle.X - (size.X / 2), vector.BottomRight.Y - size.Y);
                case Position.BottomRight:
                    return new Vector2(vector.BottomRight.X - size.X, vector.BottomRight.Y - size.Y);
            }
        }

        internal void SetWindow(UIWindow window) {
            Window = window;
        }

        protected virtual void UpdateOffsets(Vector2 size, Position elementPosition) {
            switch (elementPosition) {
                default:
                    Offsets.Top.Left.Y += size.Y;
                    break;
                case Position.TopMiddle:
                    Offsets.Top.Middle.Y += size.Y;
                    break;
                case Position.TopRight:
                    Offsets.Top.Right.Y += size.Y;
                    break;
                case Position.MiddleLeft:
                    Offsets.Middle.Left.Y += size.Y;
                    break;
                case Position.MiddleMiddle:
                    Offsets.Middle.Middle.Y += size.Y;
                    break;
                case Position.MiddleRight:
                    Offsets.Middle.Right.Y += size.Y;
                    break;
                case Position.BottomLeft:
                    Offsets.Bottom.Left.Y += size.Y;
                    break;
                case Position.BottomMiddle:
                    Offsets.Bottom.Middle.Y += size.Y;
                    break;
                case Position.BottomRight:
                    Offsets.Bottom.Right.Y += size.Y;
                    break;
            }
        }
    }
}
