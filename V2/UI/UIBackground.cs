﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using NimbusFox.KitsuneCore.V2.UI.Elements;

namespace NimbusFox.KitsuneCore.V2.UI {
    public sealed class UIBackground {
        /// <summary>
        /// 0 = TopLeft
        /// 1 = TopMiddle
        /// 2 = TopRight
        /// 3 = MiddleLeft
        /// 4 = MiddleMiddle
        /// 5 = MiddleRight
        /// 6 = BottomLeft
        /// 7 = BottomMiddle
        /// 8 = BottomRight
        /// </summary>
        private readonly Rectangle[] _pictures = new Rectangle[9];

        private readonly UIPicture _mainImage;

        private Vector2I _size = Vector2I.Zero;

        public Vector2 TopLeftOffset { get; } = Vector2.Zero;
        public Vector2 BottomRightOffset { get; } = Vector2.Zero;
        public Vector2 TopLeftPadding { get; } = Vector2.Zero;
        public Vector2 BottomRightPadding { get; } = Vector2.Zero;

        internal UIBackground(Blob data) {
            _mainImage = new UIPicture(data.GetString("resource"));

            _size = new Vector2I((int)data.GetLong("width"), (int)data.GetLong("height"));

            var list = data.GetList("locations");
            
            for (var i = 0; i < _pictures.Length; i++) {
                var item = list[i].Blob().GetVector2I();
                _pictures[i] = new Rectangle(item.X, item.Y, _size.X, _size.Y);
            }

            if (data.Contains("topLeftOffset")) {
                TopLeftOffset = data.GetBlob("topLeftOffset").GetVector2F().ToVector2();
            }

            if (data.Contains("bottomRightOffset")) {
                BottomRightOffset = data.GetBlob("bottomRightOffset").GetVector2F().ToVector2();
            }

            if (data.Contains("topLeftPadding")) {
                TopLeftPadding = data.GetBlob("topLeftPadding").GetVector2F().ToVector2();
            }

            if (data.Contains("bottomRightPadding")) {
                BottomRightPadding = data.GetBlob("bottomRightPadding").GetVector2F().ToVector2();
            }
        }

        public void Draw(DeviceContext context, Rectangle bounds, SpriteBatch spriteBatch) {
            Draw(context, bounds, spriteBatch, Color.White);
        }

        public void Draw(DeviceContext context, Rectangle bounds, SpriteBatch spriteBatch, Color color) {
            var stretch = Vector2.Zero;

            var minSize = GetMinSize(context);

            if (minSize.X < bounds.Width) {
                stretch.X = bounds.Width - minSize.X;
            }

            if (minSize.Y < bounds.Height) {
                stretch.Y = bounds.Height - minSize.Y;
            }

            spriteBatch.Draw(_mainImage.GetTexture(context), new Vector2(bounds.X, bounds.Y), _pictures[0], color);
            if (stretch.X > 0) {
                spriteBatch.Draw(_mainImage.GetTexture(context),
                    new UIRectangle(new Vector2(bounds.X + _pictures[0].Width, bounds.Y),
                        new Vector2(stretch.X, _size.Y)).ToRectangle(), _pictures[1], color);
            }

            spriteBatch.Draw(_mainImage.GetTexture(context),
                new Vector2(bounds.X + _pictures[0].Width + stretch.X, bounds.Y), _pictures[2], color);

            if (stretch.Y > 0) {
                spriteBatch.Draw(_mainImage.GetTexture(context),
                    new UIRectangle(new Vector2(bounds.X, bounds.Y + _pictures[0].Height),
                        new Vector2(_size.X, stretch.Y)).ToRectangle(), _pictures[3], color);

                if (stretch.X > 0) {
                    spriteBatch.Draw(_mainImage.GetTexture(context),
                        new UIRectangle(
                            new Vector2(bounds.X + _pictures[3].Width,
                                bounds.Y + _pictures[1].Height), stretch).ToRectangle(), _pictures[4], color);
                }

                spriteBatch.Draw(_mainImage.GetTexture(context),
                    new UIRectangle(
                            new Vector2(bounds.X + _pictures[3].Width + stretch.X,
                                bounds.Y + _pictures[2].Height), new Vector2(_size.X, stretch.Y))
                        .ToRectangle(), _pictures[5], color);
            }

            spriteBatch.Draw(_mainImage.GetTexture(context),
                new Vector2(bounds.X, bounds.Y + _pictures[2].Height + stretch.Y), _pictures[6], color);

            if (stretch.X > 0) {
                spriteBatch.Draw(_mainImage.GetTexture(context),
                    new UIRectangle(new Vector2(bounds.X + _pictures[6].Width, bounds.Y + _pictures[1].Height + stretch.Y),
                        new Vector2(stretch.X, _size.Y)).ToRectangle(), _pictures[7], color);
            }

            spriteBatch.Draw(_mainImage.GetTexture(context),
                new Vector2(bounds.X + _pictures[6].Width + stretch.X, bounds.Y + _pictures[2].Height + stretch.Y), _pictures[8], color);
        }

        public Vector2 GetMinSize(DeviceContext context) {
            return new Vector2(_pictures[0].Width + _pictures[2].Width + TopLeftPadding.X + BottomRightPadding.X,
                _pictures[0].Height + _pictures[6].Height + TopLeftPadding.Y + BottomRightPadding.Y);
        }
    }
}
