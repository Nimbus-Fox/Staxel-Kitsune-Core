﻿using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.KitsuneCore.V2.UI {
    public struct UIRectangle {
        public Vector2 TopLeft;
        public Vector2 Middle;
        public Vector2 BottomRight;
        private readonly Vector2 _size;
        public Vector2 Size => new Vector2(_size.X, _size.Y);
        public UIRectangle(Vector2 topLeft, Vector2 size) {
            TopLeft = topLeft;
            Middle = new Vector2(topLeft.X + (size.X / 2), topLeft.Y + (size.Y / 2));
            BottomRight = topLeft + size;
            _size = size;
        }

        public Rectangle ToRectangle() {
            var topLeft = Vector2I.Round(TopLeft.ToVector2F());
            var bottomRight = Vector2I.Round(BottomRight.ToVector2F());
            return new Rectangle(
                topLeft.X,
                topLeft.Y,
                bottomRight.X - topLeft.X,
                bottomRight.Y - topLeft.Y);
        }

        private bool Contains(Vector2 location) {
            return TopLeft.X <= location.X
                   && TopLeft.Y <= location.Y
                   && BottomRight.X >= location.X
                   && BottomRight.Y >= location.Y;
        }

        public bool Contains(Vector2 location, Rectangle scissor) {
            return scissor.X <= location.X
                   && scissor.Y <= location.Y
                   && scissor.X + scissor.Width >= location.X
                   && scissor.Y + scissor.Height >= location.Y
                   && Contains(location);
        }

        public static UIRectangle operator +(UIRectangle rect, Vector2 size) {
            return new UIRectangle(rect.TopLeft, size + rect._size);
        }

        public static UIRectangle operator -(UIRectangle rect, Vector2 size) {
            return new UIRectangle(rect.TopLeft, size - rect._size);
        }
    }
}
