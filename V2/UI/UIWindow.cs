﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Staxel.Draw;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using NimbusFox.KitsuneCore.V2.UI.Enums;

namespace NimbusFox.KitsuneCore.V2.UI {
    public sealed class UIWindow : IDisposable {
        public bool Closed { get; private set; }
        internal bool Initialized { get; private set; }
        private SpriteBatch _spriteBatch;
        public UIContainer Container { get; }
        private byte _opacity = byte.MaxValue;
        private bool _fadeOut;
        private TimeSpan _updateInterval;
        private byte _increments;
        private TimeSpan _currentInterval = TimeSpan.Zero;
        private Vector2 _marginTopLeft = Vector2.Zero;
        private Vector2 _marginBottomRight = Vector2.Zero;
        private readonly Position _screenPos;
        internal UIRectangle Bounds { get; private set; }
        public bool FullScreen;
        public ScreenOffsets Offsets { get; } = new ScreenOffsets();
        public bool IsActive { get; internal set; }
        public bool Visible { get; private set; } = true;
        public bool CloseOnEscape { get; set; } = true;
        public bool AdjustToScreen { get; set; } = false;

        public event Action OnClose;
        public event Action OnHide;
        public event Action OnShow;

        public UIWindow(Position screenPosition = Position.MiddleMiddle) {
            UIManager.Windows.Add(this);
            _screenPos = screenPosition;
            Container = new UIContainer(this);
        }

        internal void Initialize(DeviceContext context) {
            Initialized = true;
            _spriteBatch = new SpriteBatch(context.Graphics.GraphicsDevice);

            Container.Initialize(context);
        }

        /// <inheritdoc />
        public void Dispose() {
            Close();
            Container.Dispose();
        }

        public void Close() {
            if (!Closed) {
                OnClose?.Invoke();
            }
            Closed = true;
        }

        internal void Update(KeyboardState keyboard, MouseState mouse, GameTime gameTime, DeviceContext context) {
            if (_fadeOut) {
                _currentInterval += gameTime.ElapsedGameTime;
                ProcessFadeOut();
            }

            if (Closed) {
                return;
            }

            if (AdjustToScreen) {
                var viewPort = context.Graphics.GraphicsDevice.Viewport;
                Container.MinSize.X = viewPort.X / 4f * 3;
                Container.MinSize.Y = viewPort.Y / 4f * 3;
            }

            if (keyboard.IsKeyDown(Keys.Escape)) {
                if (CloseOnEscape) {
                    Close();
                } else {
                    Hide();
                }
            }

            Offsets.Reset();

            Offsets.Top.Left += _marginTopLeft;
            Offsets.Top.Middle.Y += _marginTopLeft.Y;
            Offsets.Top.Right += new Vector2(-_marginBottomRight.X, _marginTopLeft.Y);
            Offsets.Middle.Left.X += _marginTopLeft.X;
            Offsets.Middle.Right.X += _marginBottomRight.X;
            Offsets.Bottom.Left += new Vector2(-_marginTopLeft.X, -_marginBottomRight.Y);
            Offsets.Bottom.Middle.Y += -_marginBottomRight.Y;
            Offsets.Bottom.Right += -_marginBottomRight;

            Bounds = new UIRectangle(GetPos(context), GetSize(context));

            Container.Update(gameTime, keyboard, mouse, Bounds, context);
        }

        internal void Draw(DeviceContext context) {
            if (!Visible || Closed) {
                return;
            }

            _spriteBatch.GraphicsDevice.RasterizerState.ScissorTestEnable = true;

            _spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
            var color = Color.White;
            color.A = _opacity;

            Container.Draw(context, _spriteBatch, Bounds, context.Graphics.GraphicsDevice.ScissorRectangle, color);
            _spriteBatch.End();
        }

        public void FadeOut(TimeSpan updateInterval, byte increments) {
            if (updateInterval.Ticks > 0 && increments > 0) {
                _fadeOut = true;
                _updateInterval = updateInterval;
                _increments = increments;
            }
        }

        private void ProcessFadeOut() {
            if (_opacity == 0) {
                Dispose();
                return;
            }

            if (_currentInterval < _updateInterval) {
                return;
            }

            _currentInterval -= _updateInterval;

            if (_increments > _opacity) {
                _opacity = 0;
            } else {
                _opacity -= _increments;
            }
        }

        internal Vector2 GetSize(DeviceContext context) {
            var size = Vector2.Zero;

            if (FullScreen) {
                var viewPort = context.Graphics.GraphicsDevice.Viewport;
                size.X = viewPort.Width;
                size.Y = viewPort.Height;
                return size;
            }

            var childSize = Container.GetWindowSize(context);

            if (size.X < childSize.X) {
                size.X = childSize.X;
            }

            if (size.Y < childSize.Y) {
                size.Y = childSize.Y;
            }

            return size;
        }

        public Vector2 GetPos(DeviceContext context) {
            if (FullScreen) {
                return Vector2.Zero;
            }
            var size = GetSize(context);
            var viewport = context.Graphics.GraphicsDevice.Viewport;
            var vector = new Vector2(viewport.Width, viewport.Height);
            switch (_screenPos) {
                default:
                    return Vector2.Zero;
                case Position.TopMiddle:
                    return new Vector2((vector.X / 2) - (size.X / 2), 0);
                case Position.TopRight:
                    return new Vector2(vector.X - size.X);
                case Position.MiddleLeft:
                    return new Vector2(0, (vector.Y / 2) - (size.Y / 2));
                case Position.MiddleMiddle:
                    return new Vector2((vector.X / 2) - (size.X / 2), (vector.Y / 2) - (size.Y / 2));
                case Position.MiddleRight:
                    return new Vector2(vector.X - size.X, (vector.Y / 2) - (size.Y / 2));
                case Position.BottomLeft:
                    return new Vector2(0, vector.Y - size.Y);
                case Position.BottomMiddle:
                    return new Vector2((vector.X / 2) - (size.X / 2), vector.Y - size.Y);
                case Position.BottomRight:
                    return new Vector2(vector.X - size.X, vector.Y - size.Y);
            }
        }

        public void SetMargin(int top = 0, int left = 0, int bottom = 0, int right = 0) {
            _marginTopLeft = new Vector2(left, top);
            _marginBottomRight = new Vector2(right, bottom);
        }

        public void Show() {
            if (!Visible) {
                Visible = true;
                OnShow?.Invoke();
            }
        }

        public void Hide() {
            if (Visible) {
                Visible = false;
                OnHide?.Invoke();
            }
        }
    }
}
