﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UIRow : UIElement {
        public bool StretchToParent;
        /// <inheritdoc />
        public UIRow(Position elementPosition = Position.TopLeft) : base(elementPosition) { }

        protected override void UpdateOffsets(Vector2 size, Position elementPosition) {
            switch (elementPosition) {
                default:
                    Offsets.Top.Left.X += size.X;
                    break;
                case Position.TopMiddle:
                    Offsets.Top.Middle.X += size.X;
                    break;
                case Position.TopRight:
                    Offsets.Top.Right.X += size.X;
                    break;
                case Position.MiddleLeft:
                    Offsets.Middle.Left.X += size.X;
                    break;
                case Position.MiddleMiddle:
                    Offsets.Middle.Middle.X += size.X;
                    break;
                case Position.MiddleRight:
                    Offsets.Middle.Right.X += size.X;
                    break;
                case Position.BottomLeft:
                    Offsets.Bottom.Left.X += size.X;
                    break;
                case Position.BottomMiddle:
                    Offsets.Bottom.Middle.X += size.X;
                    break;
                case Position.BottomRight:
                    Offsets.Bottom.Right.X += size.X;
                    break;
            }
        }

        public override Vector2 GetSize(DeviceContext context) {
            var size = Vector2.Zero;

            foreach (var child in ChildElements) {
                if (child.Window != Window || child.Parent != this) {
                    continue;
                }
                var childSize = child.GetSize(context);

                if (childSize.Y > size.Y) {
                    size.Y = childSize.Y;
                }

                size.X += childSize.X;
            }

            if (StretchToParent) {
                if (Parent.Bounds.BottomRight.X != 0) {
                    size.X = Parent.Bounds.BottomRight.X;
                }
            }

            return size;
        }
    }
}
