﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Staxel;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UIButton : UISelectable {
        private string _hoverSound = "staxel.sounds.ui.Button.focus";
        private string _clickSound = "staxel.sounds.ui.Button.press";
        /// <inheritdoc />
        public UIButton(Position elementPosition) : base(elementPosition) {
            SetBackground();
            OnHover += () => {
                if (GameContext.ResourcesInitialised) {
                    ClientContext.SoundManager.PlaySound(_hoverSound);
                }
            };
            OnClick += () => {
                if (GameContext.ResourcesInitialised) {
                    ClientContext.SoundManager.PlaySound(_clickSound);
                }
            };
        }

        public static void SetBasicColors(UISelectable button) {
            button.BackgroundHoverColor = new Color(247, 233, 110);
            button.BackgroundPressColor = new Color(255, 133, 46);

            button.TextHoverColor = new Color(255, 133, 46);
            button.TextPressColor = new Color(247, 233, 110);
        }

        public void SetClickSound(string code) {
            if (GameContext.SoundDatabase.TryGetSoundDefinitions(code, out _)) {
                _clickSound = code;
            }
        }

        public void SetHoverSound(string code) {
            if (GameContext.SoundDatabase.TryGetSoundDefinitions(code, out _)) {
                _hoverSound = code;
            }
        }
    }
}
