﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Staxel;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public sealed class UIPicture : UIElement {
        private readonly Func<DeviceContext, Texture2D> _init;
        private Texture2D _texture;
        private Vector2 _override = Vector2.Zero;
        public bool StretchToParent = false;
        public bool ManualRender = false;
        private readonly UIPicture _cacheImage;
        public UIPicture(Func<DeviceContext, Texture2D> init, Position elementPosition = Position.TopLeft) : base(elementPosition) {
            _init = init;
        }

        public UIPicture(string resource, Position elementPosition = Position.TopLeft) : base(elementPosition) {
            _cacheImage = UIManager.FetchImage(resource);
        }

        public UIPicture(string resource, string stencil, Position elementPosition = Position.TopLeft) : base(elementPosition) {
            _init = context => {
                var image = UIManager.FetchImage(resource).GetTexture(context);
                var stencilImage = UIManager.FetchImage(stencil).GetTexture(context);

                var imageColors = new Color[image.Height * image.Width];
                image.GetData(imageColors);
                var stencilColors = new Color[stencilImage.Height * stencilImage.Width];
                stencilImage.GetData(stencilColors);

                if (imageColors.Length != stencilColors.Length) {
                    throw new ArgumentException("resource image and the stencil image must be the same size");
                }

                for (var i = 0; i < imageColors.Length; i++) {
                    imageColors[i].A = stencilColors[i].A;
                }

                var texture = new Texture2D(context.Graphics.GraphicsDevice, image.Width, image.Height);

                texture.SetData(imageColors);

                return texture;
            };
        }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor, Color? color = null) {
            if (ManualRender) {
                return;
            }

            var col = color ?? Color.White;

            if (_cacheImage != null) {
                if (StretchToParent) {
                    spriteBatch.Draw(_cacheImage.GetTexture(context), bounds.ToRectangle(), col);
                    return;
                }

                spriteBatch.Draw(_cacheImage.GetTexture(context), Bounds.ToRectangle(), col);
                return;
            }

            UpdateDrawing(context);

            if (StretchToParent) {
                spriteBatch.Draw(_texture, bounds.ToRectangle(), col);
                return;
            }

            spriteBatch.Draw(_texture, Bounds.ToRectangle(), col);
        }

        private void UpdateDrawing(DeviceContext context, bool swapRedBlue = false) {
            if (_cacheImage != null) {
                _cacheImage.UpdateDrawing(context);
                return;
            }

            if (_texture == null || _texture?.IsDisposed == true) {
                _texture = _init(context);

                if (swapRedBlue) {
                    var colors = new Color[_texture.Width * _texture.Height];

                    _texture.GetData(colors);

                    for (var i = 0; i < colors.Length; i++) {
                        colors[i] = colors[i].SwapRedAndBlueChannels();
                    }

                    _texture.SetData(colors);
                }
            }
        }

        /// <inheritdoc />
        public override void Initialize(DeviceContext context) {
            UpdateDrawing(context);
        }

        public void SetSize(float width, float height) {
            _override.X = width;
            _override.Y = height;
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            if (StretchToParent) {
                return Vector2.Zero;
            }

            Vector2 size;

            if (_cacheImage != null) {
                size = _cacheImage.GetSize(context);
            } else {
                UpdateDrawing(context);

                size = new Vector2(_texture.Width, _texture.Height);
            }

            if (_override.X > 0) {
                size.X = _override.X;
            }

            if (_override.Y > 0) {
                size.Y = _override.Y;
            }

            return size;
        }

        /// <inheritdoc />
        public override void AddChild(UIElement element) {
            
        }

        /// <inheritdoc />
        public override void RemoveChild(UIElement element) {
            
        }

        /// <param name="swapRedBlue">BUG: The rendering swaps the R and B values when rendering in a 3D space for a strange reason. This is a temp fix</param>
        public Texture2D GetTexture(DeviceContext context, bool swapRedBlue = false) {
            if (_cacheImage != null) {
                return _cacheImage.GetTexture(context);
            }

            UpdateDrawing(context, swapRedBlue);

            return _texture;
        }

        /// <inheritdoc />
        public override void Dispose() {
            if (_texture?.IsDisposed == false) {
                _texture?.Dispose();
            }
        }
    }
}
