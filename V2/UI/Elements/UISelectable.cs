﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V2;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Staxel;
using Staxel.Draw;
using Staxel.Input;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UISelectable : UIElement {
        /// <inheritdoc />
        public UISelectable(Position elementPosition) : base(elementPosition) { }
        private string _background;
        protected bool Selected;

        public Color BackgroundColor = Color.White;
        public Color BackgroundHoverColor = Color.White;
        public Color BackgroundPressColor = Color.White;

        public Color TextColor = Color.Black;
        public Color TextHoverColor = Color.Black;
        public Color TextPressColor = Color.Black;

        private Vector2 _minSize = Vector2.Zero;

        public Color CurrentColor { get; private set; } = Color.White;
        public Color CurrentTextColor { get; private set; } = Color.White;
        public bool IgnoreMainWindow = false;

        public event Action OnHover;
        public event Action OnLeave; 
        public event Action OnClick;
        public event Action OnDeselect;
        public event Action OnSelect;

        /// <inheritdoc />
        public override void Initialize(DeviceContext context) {
            CurrentColor = BackgroundColor;
            CurrentTextColor = TextColor;
            base.Initialize(context);
        }

        public void SetBackground(string code = Constants.Backgrounds.MenuButton) {
            _background = code;
        }

        protected bool Hover { get; private set; }
        private bool _entered;
        protected bool Clicked { get; private set; }

        /// <inheritdoc />
        public override void Update(GameTime gameTime, KeyboardState keyboard, MouseState mouse, UIRectangle bounds, DeviceContext context) {
            Hover = false;

            if (!context.IsActive()) {
                return;
            }
            
            if (Window.IsActive || IgnoreMainWindow) {
                if (Bounds.Contains(mouse.ToVector2(), bounds.ToRectangle())) {
                    Hover = true;
                }
            }

            if (Hover) {
                if (!_entered) {
                    if (!Selected) {
                        OnHover?.Invoke();
                        _entered = true;
                    }
                }
                CurrentColor = BackgroundHoverColor;
                CurrentTextColor = TextHoverColor;

                var mouseClick = mouse.LeftButton == ButtonState.Pressed || mouse.RightButton == ButtonState.Pressed;

                if (!Clicked) {
                    if (mouseClick) {
                        Clicked = true;
                    }
                } else {
                    CurrentColor = BackgroundPressColor;
                    CurrentTextColor = TextPressColor;
                    if (!mouseClick) {
                        Clicked = false;
                        if (!Selected) {
                            OnClick?.Invoke();
                        }
                    }
                }
            } else {
                if (_entered) {
                    if (!Selected) {
                        OnLeave?.Invoke();
                    }
                }
                Clicked = false;
                _entered = false;
                CurrentColor = BackgroundColor;
                CurrentTextColor = TextColor;
            }

            base.Update(gameTime, keyboard, mouse, bounds, context);
        }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor,
            Color? color = null) {
            
            if (UIManager.TryFetchBackground(_background, out var background)) {
                background.Draw(context, Bounds.ToRectangle(), spriteBatch, CurrentColor);
            }

            base.Draw(context, spriteBatch, Bounds, scissor, color);
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            var size = base.GetSize(context);

            if (size.X < _minSize.X) {
                size.X = _minSize.X;
            }

            if (size.Y < _minSize.Y) {
                size.Y = _minSize.Y;
            }

            return size;
        }

        public void SetMinSize(float width = 0, float height = 0) {
            _minSize.X = width;
            _minSize.Y = height;
        }

        public void Deselect() {
            Selected = false;
            OnDeselect?.Invoke();
            OnLeave?.Invoke();
        }

        public void Select() {
            Selected = true;
            OnSelect?.Invoke();
        }
    }
}
