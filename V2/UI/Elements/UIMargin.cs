﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UIMargin : UIElement {
        private bool _vertical;
        private int _thickness;
        private Color _color;
        private UIPicture _texture;
        /// <inheritdoc />
        public UIMargin(Position elementPosition, int thickness, Color color, Alignment alignment = Alignment.Vertical) : base(elementPosition) {
            _vertical = alignment == Alignment.Vertical;
            _thickness = thickness;
            _color = color;

            _texture = new UIPicture(context => {
                var texture = new Texture2D(context.Graphics.GraphicsDevice, 1, 1);

                var col = new [] {Color.White};

                texture.SetData(col);

                return texture;
            });
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            return _vertical ? new Vector2(_thickness, 0)
                : new Vector2(0, _thickness);
        }

        /// <inheritdoc />
        public override void AddChild(UIElement element) { }

        /// <inheritdoc />
        public override void RemoveChild(UIElement element) { }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor, Color? color = null) {
            var col = color ?? Color.White;
            _color.A = col.A;
            var parentBounds = Parent.Bounds.ToRectangle();
            var bound = Bounds.ToRectangle();
            spriteBatch.Draw(_texture.GetTexture(context),
                new Rectangle(bound.X, bound.Y, _vertical ? _thickness : parentBounds.Width,
                    _vertical ? parentBounds.Height : _thickness), _color);
        }
    }
}
