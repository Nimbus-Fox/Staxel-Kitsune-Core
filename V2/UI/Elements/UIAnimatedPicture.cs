﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Staxel;
using Staxel.Draw;
using NimbusFox.KitsuneCore.V2.UI;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Color = Microsoft.Xna.Framework.Color;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UIAnimatedPicture : UIElement {
        private int _index;
        private readonly (int, UIPicture)[] _frames;
        private Vector2 _size;
        private TimeSpan _elapsedTime = TimeSpan.Zero;

        public UIAnimatedPicture(string resource, Color? transparencyKey = null) : base(Position.TopLeft) {
            var col = transparencyKey ?? Color.Transparent;
            var image = Image.FromStream(GameContext.ContentLoader.ReadStream(resource));
            var dimension = new FrameDimension(image.FrameDimensionsList[0]);
            var frameCount = image.GetFrameCount(dimension);

            _frames = new (int, UIPicture)[frameCount];

            for (var i = 0; i < _frames.Length; i++) {
                image.SelectActiveFrame(dimension, i);
                var ms = new MemoryStream();
                image.Save(ms, ImageFormat.Png);
                ms.Seek(0L, SeekOrigin.Begin);
                var item = image.GetPropertyItem(0x5100);
                _frames[i] = ((item.Value[0] + item.Value[1] * 256) * 10,
                    new UIPicture(context => {
                        var texture = Texture2D.FromStream(context.Graphics.GraphicsDevice, ms);

                        var colors = new Color[texture.Width * texture.Height];

                        texture.GetData(colors);

                        for (var j = 0; j < colors.Length; j++) {
                            if (colors[j] == col) {
                                colors[j] = Color.Transparent;
                            }
                        }

                        texture.SetData(colors);

                        return texture;
                    }));
            }

            _size = new Vector2(image.Size.Width, image.Size.Height);
        }

        /// <inheritdoc />
        public override void Dispose() {
            foreach (var image in _frames) {
                image.Item2.Dispose();
            }
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            return _size;
        }

        /// <inheritdoc />
        public override void Update(GameTime gameTime, KeyboardState keyboard, MouseState mouse, UIRectangle bounds, DeviceContext context) {
            Offsets.Reset();

            _elapsedTime += gameTime.ElapsedGameTime;

            if (_elapsedTime.Milliseconds > _frames[_index].Item1) {
                _elapsedTime -= new TimeSpan(0, 0, 0, 0, _frames[_index].Item1);
                _index++;
                if (_index == _frames.Length) {
                    _index = 0;
                }
            }

            Bounds = GetBounds(context);
        }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor,
            Color? color = null) {
            var col = color ?? Color.White;

            spriteBatch.Draw(_frames[_index].Item2.GetTexture(context), Bounds.ToRectangle(), col);
        }
    }
}
