﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V2;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Plukit.Base;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UIContainer : UIElement {
        private Vector2 _size;
        public Vector2 MaxSize = Vector2.Zero;
        private string _background = "";
        public bool StretchSelectablesToWidth = false;
        private UIScrollBar _horizontalScrollBar;
        private UIScrollBar _verticalScrollBar;
        public Vector2 MinSize = Vector2.Zero;

        /// <inheritdoc />
        internal UIContainer(UIWindow window) : base(Position.TopLeft) {
            SetWindow(window);
            BaseOffSetReset = false;
        }

        public UIContainer(Position elementPosition) : base(elementPosition) {
            BaseOffSetReset = false;
        }

        /// <inheritdoc />
        public override void Initialize(DeviceContext context) {
            if (Parent == null) {
                if (Window.FullScreen) {
                    _size = Window.GetSize(context);
                }
            }

            base.Initialize(context);
        }

        /// <inheritdoc />
        public override void Update(GameTime gameTime, KeyboardState keyboard, MouseState mouse, UIRectangle bounds,
            DeviceContext context) {
            Offsets.Reset();
            if (Parent == null) {
                if (Window.FullScreen) {
                    _size = Window.GetSize(context);
                }
            }

            _verticalScrollBar?.Update(gameTime, keyboard, mouse, Bounds, context);
            _horizontalScrollBar?.Update(gameTime, keyboard, mouse, Bounds, context);

            if (UIManager.TryFetchBackground(_background, out var background)) {
                Offsets.Top.Left += background.TopLeftOffset;
                Offsets.Top.Middle.Y += background.TopLeftOffset.Y;
                Offsets.Top.Right += new Vector2(background.BottomRightOffset.X, background.TopLeftOffset.Y);
                Offsets.Middle.Left.X += background.TopLeftOffset.X;
                Offsets.Middle.Right.X += background.BottomRightOffset.X;
                Offsets.Bottom.Left += new Vector2(background.TopLeftOffset.X, background.BottomRightOffset.Y);
                Offsets.Bottom.Right += background.BottomRightOffset;
            }

            if (StretchSelectablesToWidth) {
                var width = GetWidth(context);
                foreach (var child in ChildElements) {
                    if (child is UISelectable selectable) {
                        selectable.SetMinSize(width);
                    }
                }
            }

            base.Update(gameTime, keyboard, mouse, bounds, context);
        }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor,
            Color? color = null) {
            var cut = scissor;

            if (MaxSize.X > 0 || MaxSize.Y > 0) {
                cut = Bounds.ToRectangle();
                spriteBatch.End();
                spriteBatch.GraphicsDevice.ScissorRectangle = cut;
                spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
            }

            if (UIManager.TryFetchBackground(_background, out var background)) {
                background.Draw(context, Bounds.ToRectangle(), spriteBatch);
            }

            base.Draw(context, spriteBatch, bounds, cut, color);

            if (MaxSize.X > 0 || MaxSize.Y > 0) {
                spriteBatch.End();
                spriteBatch.GraphicsDevice.ScissorRectangle = scissor;
                spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
            }

            _verticalScrollBar?.Draw(context, spriteBatch, Bounds, scissor, color);
            _horizontalScrollBar?.Draw(context, spriteBatch, Bounds, scissor, color);
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            return GetSize(context, false);
        }

        public Vector2 GetSize(DeviceContext context, bool ignoreMaxSize) {
            var size = Parent == null && Window.FullScreen ? _size : base.GetSize(context);

            if (UIManager.TryFetchBackground(_background, out var background)) {
                size = size + background.TopLeftOffset + background.BottomRightOffset;
                var minSize = background.GetMinSize(context);

                if (size.X < minSize.X) {
                    size.X = minSize.X;
                }

                if (size.Y < minSize.Y) {
                    size.Y = minSize.Y;
                }
            }

            if (MaxSize.X > 0) {
                if (size.X > MaxSize.X) {
                    if (!ignoreMaxSize) {
                        size.X = MaxSize.X;
                    }

                    if (_horizontalScrollBar == null) {
                        _horizontalScrollBar = new UIScrollBar(this, Alignment.Horizontal);
                    }
                } else {
                    _horizontalScrollBar?.Dispose();
                    _horizontalScrollBar = null;
                }
            } else {
                _horizontalScrollBar?.Dispose();
                _horizontalScrollBar = null;
            }

            if (MaxSize.Y > 0) {
                if (size.Y > MaxSize.Y) {
                    if (!ignoreMaxSize) {
                        size.Y = MaxSize.Y;
                    }

                    if (_verticalScrollBar == null) {
                        _verticalScrollBar = new UIScrollBar(this);
                    }
                } else {
                    _verticalScrollBar?.Dispose();
                    _verticalScrollBar = null;
                }
            } else {
                _verticalScrollBar?.Dispose();
                _verticalScrollBar = null;
            }

            if (_verticalScrollBar != null) {
                size.X += _verticalScrollBar.GetSize(context).X;
            }

            if (_horizontalScrollBar != null) {
                size.Y += _horizontalScrollBar.GetSize(context).Y;
            }

            if (MinSize.X > 0) {
                if (size.X < MinSize.X) {
                    size.X = MinSize.X;
                }
            }

            if (MinSize.Y > 0) {
                if (size.Y < MinSize.Y) {
                    size.Y = MinSize.Y;
                }
            }

            return size;
        }

        private Vector2 GetSizeNoOffSets(DeviceContext context) {
            var size = Parent == null && Window.FullScreen ? _size : base.GetSize(context);

            if (UIManager.TryFetchBackground(_background, out var background)) {
                var minSize = background.GetMinSize(context);

                if (size.X < minSize.X) {
                    size.X = minSize.X;
                }

                if (size.Y < minSize.Y) {
                    size.Y = minSize.Y;
                }
            }
            
            if (MinSize.X > 0) {
                if (size.X < MinSize.X) {
                    size.X = MinSize.X;
                }
            }

            return size;
        }

        internal Vector2 GetWindowSize(DeviceContext context) {
            return GetSize(context);
        }

        public void SetBackground([NotNull] string code = Constants.Backgrounds.Dark) {
            _background = code;
        }

        private float GetWidth(DeviceContext context) {
            return GetSizeNoOffSets(context).X;
        }
    }
}