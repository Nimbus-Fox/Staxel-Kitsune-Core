﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Staxel;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public sealed class UISocialMediaButton : UIButton {
        private UIPicture _border;
        private UIPicture _icon;
        private bool _showBorder = false;

        /// <inheritdoc />
        public UISocialMediaButton(Position elementPosition) : base(elementPosition) {
            OnHover += () => { _showBorder = true; };
            OnLeave += () => { _showBorder = false; };
        }

        public void SetIcon(string resource) {
            _border = new UIPicture(resource + "Border.png");

            _border.Parent = this;

            _border.SetSize(100, 100);

            _icon = new UIPicture(resource + "Colour.png");

            _icon.Parent = this;

            _icon.SetSize(100, 100);
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            if (_icon == null) {
                return Vector2.Zero;
            }

            return _icon.GetSize(context);
        }

        /// <inheritdoc />
        public override void Update(GameTime gameTime, KeyboardState keyboard, MouseState mouse, UIRectangle bounds, DeviceContext context) {
            _icon.Update(gameTime, keyboard, mouse, bounds, context);
            _border.Update(gameTime, keyboard, mouse, bounds, context);
            base.Update(gameTime, keyboard, mouse, bounds, context);
        }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor, Color? color = null) {
            _icon.Draw(context, spriteBatch, Bounds, scissor);

            if (_showBorder) {
                _border.Draw(context, spriteBatch, Bounds, scissor);
            }
        }
    }
}
