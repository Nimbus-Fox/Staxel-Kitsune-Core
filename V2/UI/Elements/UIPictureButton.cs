﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UIPictureButton : UIButton {
        private UIPicture _buttonImage;
        private Rectangle _region;
        private Color _renderColor;
        private Vector2? _size;

        public UIPictureButton(string resource, int height, int width, Position elementPosition) :
            this(resource, height, width, Color.White, elementPosition) { }

        public UIPictureButton(string resource, int height, int width, Color color, Position elementPosition) :
            this(height, width, color, elementPosition) {
            _buttonImage = new UIPicture(resource);
            _buttonImage.ManualRender = true;
        }

        private UIPictureButton(int height, int width, Color color, Position elementPosition) :
            base(elementPosition) {
            _region = new Rectangle(0, 0, width, height);
            OnLeave += () => { _region.X = width; };
            OnHover += () => { _region.X = width * 2; };
            OnDeselect += () => { _region.X = width; };
            OnSelect += () => {
                _region.X = 0;
                Selected = true;
            };
            _renderColor = color;
        }

        public UIPictureButton(string resource, string stencil, int height, int width, Position elementPosition) :
            this(resource, stencil, height, width, Color.White, elementPosition) { }

        public UIPictureButton(string resource, string stencil, int height, int width, Color color,
            Position elementPosition) :
            this(height, width, color, elementPosition) {
            _buttonImage = new UIPicture(resource, stencil);
            _buttonImage.ManualRender = true;
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            return _size ?? new Vector2(_region.Width, _region.Height);
        }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor, Color? color = null) {
            var col = color ?? Color.White;
            _renderColor.A = col.A;
            spriteBatch.Draw(_buttonImage.GetTexture(context), Bounds.ToRectangle(), _region, _renderColor);
        }

        public void SetSize(int width, int height) {
            if (width <= 0 || height <= 0) {
                _size = null;
                return;
            }

            _size = new Vector2(width, height);
        }
    }
}
