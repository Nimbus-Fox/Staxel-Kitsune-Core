﻿using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V2;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Plukit.Base;
using Staxel.Draw;
using Color = Microsoft.Xna.Framework.Color;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UILabel : UIElement {
        private string _font;
        private float _size;
        private string _text = "";
        private string _renderText = "";
        public Color Color = Color.White;
        private Vector2 _renderSize = Vector2.Zero;
        private int _maxWidth = 0;
        private readonly List<UIPicture> _renders = new List<UIPicture>();

        /// <inheritdoc />
        public UILabel(Position elementPosition) : base(elementPosition) {
            SetFont();
        }

        public void SetFont(string fontName = Constants.Fonts.MyFirstCrush, float fontSize = Constants.Fonts.DefaultSize) {
            if (UIManager.TryFetchFont(fontName, out _, fontSize)) {
                SetMaxWidth(_maxWidth);
            }
            _font = fontName;
            _size = fontSize;
        }

        public void SetText([NotNull] string text) {
            _text = text;
            while (_text.Contains("  ")) {
                _text = text.Replace("  ", " ");
            }
            SetMaxWidth(_maxWidth);
        }

        public void SetMaxWidth(int width) {
            if (width >= 0) {
                _maxWidth = width;
            }

            if (_maxWidth == 0) {
                _renderText = _text;
                GenerateRenders();
                return;
            }

            if (UIManager.TryFetchFont(_font, out var font, _size)) {
                var line = 0f;
                var sb = new StringBuilder();

                foreach (var word in _text.Split(' ')) {
                    var size = font.WordWidth(" " + word);
                    if (line + size > _maxWidth) {
                        sb.Append("\n" + word);
                        line = word.Length;
                        continue;
                    }

                    if (line != 0) {
                        sb.Append(' ');
                    }

                    sb.Append(word);
                    line += font.WordWidth(" " + word);
                }

                _renderText = sb.ToString();
            } else {
                _renderText = _text;
            }

            GenerateRenders();
        }

        private void GenerateRenders() {
            foreach (var render in _renders) {
                render.Dispose();
            }

            _renders.Clear();

            _renderSize = Vector2.Zero;

            foreach (var line in _renderText.Split('\n')) {
                _renders.Add(new UIPicture(context => {
                    var image = new Bitmap(1, 1);
                    if (UIManager.TryFetchFont(_font, out var font, _size)) {
                        image = font.RenderString(line);
                    }

                    var render = image.ToTexture2D(context);
                    
                    if (_renderSize.X < render.Width) {
                        _renderSize.X = render.Width;
                    }

                    _renderSize.Y += render.Height;

                    return render;
                }));
            }
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            return _renderSize;
        }

        /// <inheritdoc />
        public override void Update(GameTime gameTime, KeyboardState keyboard, MouseState mouse, UIRectangle bounds,
            DeviceContext context) {
            var parentButton = Parent;

            if (_renders.Count == 0) {
                GenerateRenders();
            }

            while (parentButton is UIButton == false) {
                if (parentButton.Parent == null) {
                    break;
                }

                parentButton = parentButton.Parent;
            }

            if (parentButton is UIButton button) {
                Color = button.CurrentTextColor;
            }

            Bounds = GetBounds(context);
        }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor,
            Color? color = null) {
            var col = color ?? Color.White;
            Color.A = col.A;

            var offset = Vector2.Zero;

            foreach (var render in _renders) {
                spriteBatch.Draw(render.GetTexture(context), Bounds.TopLeft + offset, Color);
                offset.Y += render.GetSize(context).Y;
            }

            //spriteBatch.Draw(_renderedFont.GetTexture(context), Bounds.TopLeft, Color);
        }

        /// <inheritdoc />
        public override void AddChild(UIElement element) {
        }

        /// <inheritdoc />
        public override void RemoveChild(UIElement element) {
        }
    }
}
