﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V2;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public class UIScrollBar : UIElement {
        public new UIContainer Parent { get; }
        private int _previousWheelValue;
        private float _speed;
        private bool _vertical;
        private UIBackground _scrollBar;
        private UIPicture _scrollLine;
        private Color _scrollLineColor = Color.Black;
        private float _fixedWidthHeight;
        private UIRectangle _scrollBounds;
        private Vector2? _scrollClick = null;
        /// <inheritdoc />
        /// <param name="scrollLineColor">Default color is Black</param>
        public UIScrollBar(UIContainer parent, Alignment alignment = Alignment.Vertical, string buttonBackground = Constants.Backgrounds.ScrollButton, Color? scrollLineColor = null, float fixedWidthHeight = 15) : base(Position.TopLeft) {
            Parent = parent;
            Window = parent.Window;
            _previousWheelValue = ClientContext.InputSource.ScrollWheelValue();
            _vertical = alignment == Alignment.Vertical;

            if (UIManager.TryFetchBackground(buttonBackground, out var background)) {
                _scrollBar = background;
            }

            if (scrollLineColor != null) {
                _scrollLineColor = scrollLineColor.Value;
            }

            _fixedWidthHeight = fixedWidthHeight;

            _scrollLine = new UIPicture(context => {
                var scrollSize = Vector2I.Round(GetSize(context).ToVector2F());
                var texture = new Texture2D(context.Graphics.GraphicsDevice, scrollSize.X, scrollSize.Y);

                var part = (_vertical ? scrollSize.X : scrollSize.Y) / 2;

                var start = new Vector2I(part, part);
                var end = new Vector2I(texture.Width - part, texture.Height - part);

                var colors = new Color[texture.Width * texture.Height];

                Helpers.VectorLoop(start, end, vec => {
                    colors.SetColor(scrollSize, vec, _scrollLineColor);
                });

                texture.SetData(colors);

                return texture;
            });
        }

        /// <inheritdoc />
        public override void Update(GameTime gameTime, KeyboardState keyboard, MouseState mouse, UIRectangle bounds, DeviceContext context) {
            if (!Window.IsActive) {
                return;
            }

            var newValue = ClientContext.InputSource.ScrollWheelValue();

            Bounds = GetBounds(context);

            var allBounds = bounds + new Vector2(_vertical ? Bounds.Size.X : 0, !_vertical ? Bounds.Size.Y : 0);

            var inWindow = allBounds.Contains(mouse.ToVector2(), allBounds.ToRectangle());

            var fullSize = Parent.GetSize(context, true);
            var maxSize = Parent.GetSize(context, false);

            if (_vertical) {
                var percentage = Helpers.CalculatePercentage(Parent.ScrollOffset.Y, fullSize.Y);
                var scrollBounds = new Vector2(Bounds.TopLeft.X, Bounds.TopLeft.Y +
                    Helpers.GetPercentageOf(percentage, Bounds.Size.Y));
                var height = Helpers.GetPercentageOf(Helpers.CalculatePercentage(maxSize.Y, fullSize.Y), maxSize.Y);

                if (height < _fixedWidthHeight) {
                    height = _fixedWidthHeight;
                }

                _scrollBounds = new UIRectangle(scrollBounds, new Vector2(_fixedWidthHeight, height));
            } else {
                var percentage = Helpers.CalculatePercentage(Parent.ScrollOffset.X, fullSize.X);
                var scrollBounds = new Vector2(Bounds.TopLeft.X + Helpers.GetPercentageOf(percentage, Bounds.Size.X), Bounds.BottomRight.Y);
                var width = Helpers.GetPercentageOf(Helpers.CalculatePercentage(maxSize.X, fullSize.X), maxSize.X);

                if (width < _fixedWidthHeight) {
                    width = _fixedWidthHeight;
                }

                _scrollBounds = new UIRectangle(scrollBounds, new Vector2(width, _fixedWidthHeight));
            }

            UpdateSpeed(newValue, inWindow);

            if (_vertical && !(keyboard.IsKeyDown(Keys.LeftAlt) || keyboard.IsKeyDown(Keys.RightAlt))) {
                Parent.ScrollOffset.Y += _speed;

                if (Parent.ScrollOffset.Y < 0) {
                    Parent.ScrollOffset.Y = 0;
                }

                var maxScroll = fullSize.Y - maxSize.Y;

                if (Parent.ScrollOffset.Y > maxScroll) {
                    Parent.ScrollOffset.Y = maxScroll;
                }
            }

            if (!_vertical && (keyboard.IsKeyDown(Keys.LeftAlt) || keyboard.IsKeyDown(Keys.RightAlt))) {
                Parent.ScrollOffset.X += _speed;

                if (Parent.ScrollOffset.X < 0) {
                    Parent.ScrollOffset.X = 0;
                }

                var maxScroll = fullSize.X - maxSize.X;

                if (Parent.ScrollOffset.X > maxScroll) {
                    Parent.ScrollOffset.X = maxScroll;
                }
            }

            _previousWheelValue = newValue;

            if (Bounds.Contains(mouse.ToVector2(), Window.Bounds.ToRectangle()) &&
                (mouse.LeftButton == ButtonState.Pressed || mouse.RightButton == ButtonState.Pressed) &&
                context.IsActive()) {
                _scrollClick = mouse.ToVector2();
            }

            if (_scrollClick.HasValue) {
                if (_vertical) {
                    var down = _scrollBounds.TopLeft.Y < _scrollClick.Value.Y;
                    var percentage = Helpers.CalculatePercentage(_scrollClick.Value.Y - Bounds.TopLeft.Y, Bounds.Size.Y);
                    var newPos = Helpers.GetPercentageOf(percentage, fullSize.Y - maxSize.Y);

                    if (down) {
                        _speed = (newPos - Parent.ScrollOffset.Y) / Helpers.CalculatePercentage(maxSize.Y, fullSize.Y);
                    } else {
                        _speed = -(Parent.ScrollOffset.Y - newPos) / Helpers.CalculatePercentage(maxSize.Y, fullSize.Y);
                    }
                } else {
                    var down = _scrollBounds.TopLeft.X < _scrollClick.Value.X;
                    var percentage = Helpers.CalculatePercentage(_scrollClick.Value.X - Bounds.TopLeft.X, Bounds.Size.X);
                    var newPos = Helpers.GetPercentageOf(percentage, fullSize.X - maxSize.X);

                    if (down) {
                        _speed = (newPos - Parent.ScrollOffset.X) / Helpers.CalculatePercentage(maxSize.X, fullSize.X);
                    } else {
                        _speed = -(Parent.ScrollOffset.X - newPos) / Helpers.CalculatePercentage(maxSize.X, fullSize.X);
                    }
                }

                if (_speed == 0) {
                    _scrollClick = null;
                }
            }
        }

        /// <inheritdoc />
        public override void Draw(DeviceContext context, SpriteBatch spriteBatch, UIRectangle bounds, Rectangle scissor, Color? color = null) {
            spriteBatch.Draw(_scrollLine.GetTexture(context), Bounds.ToRectangle(), Color.White);
            _scrollBar.Draw(context, _scrollBounds.ToRectangle(), spriteBatch, Color.Orange);
        }

        /// <inheritdoc />
        public override void AddChild(UIElement element) { }

        /// <inheritdoc />
        public override void RemoveChild(UIElement element) { }

        private void UpdateSpeed(int scrollVal, bool inWindow) {
            if (scrollVal < _previousWheelValue && inWindow) {
                if (_speed < 0) {
                    _speed = 0;
                }

                _speed += 1;
                _scrollClick = null;
            } else if (scrollVal > _previousWheelValue && inWindow) {
                if (_speed > 0) {
                    _speed = 0;
                }

                _speed -= 1;
                _scrollClick = null;
            } else {
                if (_speed < 0) {
                    _speed += 0.10f;
                    if (_speed > 0) {
                        _speed = 0;
                    }
                } else if (_speed > 0) {
                    _speed -= 0.10f;
                    if (_speed < 0) {
                        _speed = 0;
                    }
                }
            }
        }

        /// <inheritdoc />
        public override UIRectangle GetBounds(DeviceContext context) {
            return _vertical
                ? new UIRectangle(new Vector2(Parent.Bounds.BottomRight.X, Parent.Bounds.TopLeft.Y), GetSize(context))
                : new UIRectangle(new Vector2(Parent.Bounds.TopLeft.X, Parent.Bounds.BottomRight.Y), GetSize(context));
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            return _vertical
                ? new Vector2(_fixedWidthHeight, Parent.Bounds.Size.Y)
                : new Vector2(Parent.Bounds.Size.X, _fixedWidthHeight);
        }
    }
}
