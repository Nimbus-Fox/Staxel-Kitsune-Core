﻿using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V2;
using NimbusFox.KitsuneCore.V2.UI.Enums;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V2.UI.Elements {
    public sealed class UISpacer : UIElement {
        private Vector2 _size;
        private bool _percent;
        /// <inheritdoc />
        public UISpacer() : base(Position.TopLeft) { }

        private void SetSize(int width = 10, int height = 10) {
            _size.X = width;
            _size.Y = height;
        }

        public static UISpacer Create(int width = 10, int height = 10, Position position = Position.TopLeft, bool percent = false) {
            var spacer = new UISpacer();
            spacer.SetSize(width, height);
            spacer.ElementPosition = position;
            spacer._percent = percent;
            return spacer;
        }

        /// <inheritdoc />
        public override Vector2 GetSize(DeviceContext context) {
            if (!_percent) {
                return _size;
            }

            var viewPort = context.Graphics.GraphicsDevice.Viewport;

            return new Vector2(Helpers.CalculatePercentage(_size.X, viewPort.Width),
                Helpers.CalculatePercentage(_size.Y, viewPort.Height));
        }

        /// <inheritdoc />
        public override void AddChild(UIElement element) {

        }

        /// <inheritdoc />
        public override void RemoveChild(UIElement element) {
        }
    }
}
