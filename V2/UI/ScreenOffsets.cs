﻿using Microsoft.Xna.Framework;

namespace NimbusFox.KitsuneCore.V2.UI {
    public class ScreenOffsets {
        public Partition Top = new Partition();
        public Partition Middle = new Partition();
        public Partition Bottom = new Partition();

        public void Reset() {
            Top.Left.X = 0;
            Top.Left.Y = 0;
            Top.Middle.X = 0;
            Top.Middle.Y = 0;
            Top.Right.X = 0;
            Top.Right.Y = 0;

            Middle.Left.X = 0;
            Middle.Left.Y = 0;
            Middle.Middle.X = 0;
            Middle.Middle.Y = 0;
            Middle.Right.X = 0;
            Middle.Right.Y = 0;

            Bottom.Left.X = 0;
            Bottom.Left.Y = 0;
            Bottom.Middle.X = 0;
            Bottom.Middle.Y = 0;
            Bottom.Right.X = 0;
            Bottom.Right.Y = 0;
        }
    }

    public class Partition {
        public Vector2 Left;
        public Vector2 Middle;
        public Vector2 Right;
    }
}