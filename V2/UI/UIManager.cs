﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Plukit.Base;
using SharpFont;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using NimbusFox.KitsuneCore.V2.UI.SharpFont;

namespace NimbusFox.KitsuneCore.V2.UI {
    public static class UIManager {
        private static readonly FontService FontService = new FontService();

        internal static readonly List<UIWindow> Windows = new List<UIWindow>();

        private static readonly Dictionary<string, Face> Fonts = new Dictionary<string, Face>();

        private static readonly Dictionary<string, UIPicture> Pictures = new Dictionary<string, UIPicture>();

        private static readonly Dictionary<string, UIBackground> Backgrounds = new Dictionary<string, UIBackground>();

        internal static void Update(GameTime gameTime, DeviceContext context) {
            var keyboardState = Keyboard.GetState();
            var mouseState = Mouse.GetState();

            Windows.RemoveAll(x => x.Closed);

            foreach (var window in Windows) {
                if (!window.Initialized) {
                    window.Initialize(context);
                }
            }

            var activeWindow = Windows.FindLast(x => x.Visible);

            foreach (var window in Windows) {
                if (window.Closed) {
                    continue;
                }

                window.IsActive = activeWindow == window;
                window.Update(keyboardState, mouseState, gameTime, context);
            }
        }

        internal static void Draw(DeviceContext context) {
            foreach (var window in Windows) {
                if (!window.Closed) {
                    window.Draw(context);
                }
            }
        }

        internal static void Initialize() {
            foreach (var font in GameContext.AssetBundleManager.FindByExtension(".ttf")) {
                LoadFont(font);
            }

            foreach (var font in ClientContext.LanguageDatabase.GetActiveFontPaths()) {
                LoadFont(font);
            }

            foreach (var background in GameContext.AssetBundleManager.FindByExtension(".stxlBackground")) {
                var blob = GameContext.Resources.FetchBlob(background);
                GameContext.Patches.PatchFile(background, blob);
                if (!Backgrounds.ContainsKey(blob.GetString("code"))) {
                    Backgrounds.Add(blob.GetString("code"), new UIBackground(blob));
                }
            }
        }

        private static void LoadFont(string resource) {
            var fontName = Path.GetFileNameWithoutExtension(resource);
            if (!Fonts.ContainsKey(fontName)) {
                Fonts.Add(fontName,
                    new Face(FontService.Lib, GameContext.AssetBundleManager.DiskFilePath(resource)));
            }
        }

        public static bool TryFetchFont([NotNull] string fontName, out FontService fontService,
            float size = Constants.Fonts.DefaultSize) {
            fontService = null;
            if (Fonts.ContainsKey(fontName)) {
                FontService.SetFont(Fonts[fontName]);

                FontService.SetSize(size);

                fontService = FontService;

                return true;
            }

            return false;
        }

        public static bool TryFetchBackground(string code, out UIBackground background) {
            background = null;

            if (Backgrounds.ContainsKey(code)) {
                background = Backgrounds[code];
                return true;
            }

            return false;
        }

        internal static UIPicture FetchImage(string resource) {
            if (!Pictures.ContainsKey(resource)) {
                Pictures.Add(resource,
                    new UIPicture(context => Texture2D.FromStream(context.Graphics.GraphicsDevice,
                        GameContext.ContentLoader.ReadStream(resource))));
                Pictures[resource].ManualRender = true;
            }

            return Pictures[resource];
        }
    }
}