﻿namespace NimbusFox.KitsuneCore.V2.UI.Enums {
    public enum Alignment {
        Vertical,
        Horizontal
    }
}