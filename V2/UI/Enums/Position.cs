﻿namespace NimbusFox.KitsuneCore.V2.UI.Enums {
    public enum Position {
        TopLeft,
        TopMiddle,
        TopRight,
        MiddleLeft,
        MiddleMiddle,
        MiddleRight,
        BottomLeft,
        BottomMiddle,
        BottomRight
    }
}