﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Plukit.Base;
using Staxel;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore {
    public partial class Helpers {
        public static void VectorLoop(Vector2I start, Vector2I end, Action<int, int> loopAction) {
            VectorLoop(start, end, (coord) => { loopAction(coord.X, coord.Y); });
        }

        public static void VectorLoop(Vector2I start, Vector2I end, Action<Vector2I> loopAction) {
            for (var x = start.X; x <= end.X; x++) {
                for (var y = start.Y; y <= end.Y; y++) {
                    loopAction(new Vector2I(x, y));
                }
            }
        }

        public static void VectorLoop(Vector2I start, Vector2I end, Action<Vector2I, Vector2I> loopAction) {
            var xPlus = 0;
            var yPlus = 0;
            for (var x = start.X; x <= end.X; x++) {
                for (var y = start.Y; y <= end.Y; y++) {
                    loopAction(new Vector2I(x, y), new Vector2I(xPlus, yPlus));
                    yPlus++;
                }

                yPlus = 0;
                xPlus++;
            }
        }

        public static float CalculatePercentage(float value, float maximum) {
            return value / maximum * 100;
        }

        public static float GetPercentageOf(float percentage, float value) {
            return (percentage / 100) * value;
        }

        public static TextureVertexDrawable CreateRectangleDrawable(Vector2 size, Color color, Vector4F textureRegion) {
            var vertex = new TextureVertex[4];
            
            var scaleSize = new Vector2(size.X / 100, size.Y / 100);

            var halfX = scaleSize.X / 2;
            vertex[0].Position = new Vector3F(-halfX, 0, 0);
            vertex[1].Position = new Vector3F(-halfX, scaleSize.Y, 0);
            vertex[2].Position = new Vector3F(halfX, scaleSize.Y, 0);
            vertex[3].Position = new Vector3F(halfX, 0, 0);

            vertex[0].Color = color;
            vertex[1].Color = color;
            vertex[2].Color = color;
            vertex[3].Color = color;

            vertex[0].Texture = new Vector2F(textureRegion.X, textureRegion.W);
            vertex[1].Texture = new Vector2F(textureRegion.X, textureRegion.Y);
            vertex[2].Texture = new Vector2F(textureRegion.Z, textureRegion.Y);
            vertex[3].Texture = new Vector2F(textureRegion.Z, textureRegion.W);

            return new TextureVertexDrawable(vertex, vertex.Length);
        }

        public static Color RandomColor => new Color((byte)GameContext.RandomSource.Next(0, 255),
            (byte)GameContext.RandomSource.Next(0, 255), (byte)GameContext.RandomSource.Next(0, 255));

        public static float GetTimedRotation(float speed) {
            return (float)((DateTime.UtcNow - new DateTime(0L)).TotalMilliseconds * speed % (2 * Math.PI));
        }
        
        public static byte GetNewByte(byte orig, byte remove) {
            if (remove >= orig) {
                return 0;
            }

            return (byte) (orig - remove);
        }

        public static byte[] Hash(Stream data) {
            using (var hasher = new System.Security.Cryptography.SHA256Managed()) {
                data.Seek(0L, SeekOrigin.Begin);
                return hasher.ComputeHash(data);
            }
        }

        public static string StringHash(Stream data) {
            return BitConverter.ToString(Hash(data));
        }
    }
}
