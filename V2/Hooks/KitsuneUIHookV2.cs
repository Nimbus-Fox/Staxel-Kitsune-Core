﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V2.Patches;
using NimbusFox.KitsuneCore.V2.UI;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;
using UIManager = NimbusFox.KitsuneCore.V3.UI.UIManager;

namespace NimbusFox.KitsuneCore.V2.Hooks {
    public class KitsuneUIHookV2 : IModHookV4 {
        static KitsuneUIHookV2() {
            UniverseRendererPatches.Initialize();
        }

        public KitsuneUIHookV2() {
        }
        /// <inheritdoc />
        public void Dispose() { }

        /// <inheritdoc />
        public void GameContextInitializeInit() { }

        /// <inheritdoc />
        public void GameContextInitializeBefore() { }

        /// <inheritdoc />
        public void GameContextInitializeAfter() {
            if (!Helpers.IsServer()) {
                UIManager.Initialize();
            }
        }

        /// <inheritdoc />
        public void GameContextDeinitialize() { }

        /// <inheritdoc />
        public void GameContextReloadBefore() { }

        /// <inheritdoc />
        public void GameContextReloadAfter() { }

        /// <inheritdoc />
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }

        /// <inheritdoc />
        public void UniverseUpdateAfter() { }

        /// <inheritdoc />
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        /// <inheritdoc />
        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        /// <inheritdoc />
        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        /// <inheritdoc />
        public void ClientContextInitializeInit() { }

        /// <inheritdoc />
        public void ClientContextInitializeBefore() { }

        /// <inheritdoc />
        public void ClientContextInitializeAfter() {
        }

        /// <inheritdoc />
        public void ClientContextDeinitialize() { }

        /// <inheritdoc />
        public void ClientContextReloadBefore() { }

        /// <inheritdoc />
        public void ClientContextReloadAfter() { }

        /// <inheritdoc />
        public void CleanupOldSession() { }

        /// <inheritdoc />
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        /// <inheritdoc />
        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
