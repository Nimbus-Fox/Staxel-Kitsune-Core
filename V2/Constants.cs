﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NimbusFox.KitsuneCore.V2 {
    public static partial class Constants {
        public partial struct Fonts {
            public const float DefaultSize = 20;
            public const string MyFirstCrush = "MyFirstCrush";
        }

        public partial struct Backgrounds {
            public const string Dark = "KitsuneCore.V2.background.dark";
            public const string Light = "KitsuneCore.V2.background.light";
            public const string MenuButton = "KitsuneCore.V2.background.menuButton";
            public const string ScrollButton = "KitsuneCore.V2.background.scrollButton";
        }

        public partial struct Stencils {
            public const string ThreeStageWide = "mods/Kitsune Core/V2/Staxel/UI/Stencils/3StageWideStencil.png";
            public const string ThreeStageSquare = "mods/Kitsune Core/V2/Staxel/UI/Stencils/3StageSquareStencil.png";
        }
    }
}
