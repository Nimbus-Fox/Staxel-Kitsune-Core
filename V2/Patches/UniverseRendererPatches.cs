﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Hooks;
using NimbusFox.KitsuneCore.V1.KeyBinds;
using NimbusFox.KitsuneCore.V1.UI.Classes;
using NimbusFox.KitsuneCore.V2.UI;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Input;
using Staxel.Lod;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.KitsuneCore.V2.Patches {
    public static class UniverseRendererPatches {
        private static bool _init = false;
        private static DeviceContext _context;

        internal static void Initialize() {
            KitsuneUIHook.PatchController.Add(typeof(UniverseRenderer), "Draw", null, null,
                typeof(UniverseRendererPatches), "Draw");
            KitsuneUIHook.PatchController.Add(typeof(UniverseRenderer), "Update", typeof(UniverseRendererPatches),
                "Update");
        }

        private static GameTime _gameTime;
        private static DateTime _time = DateTime.Now;

        private static void Update(Timestep step, Universe universe, AvatarController avatarController) {
            _gameTime = new GameTime(new TimeSpan(step.Step), DateTime.Now - _time);
            _time = DateTime.Now;

            if (_context == null) {
                return;
            }

            if (!_init) {
                UIManager.Initialize();
                ModKeyBindManager.Load();
                _init = true;
            }

            UIManager.Update(_gameTime, _context);
            V3.UI.UIManager.Update(_gameTime, _context);
            ModKeyBindManager.Update(_gameTime, _context);
        }

        private static void Draw(DeviceContext graphics,
            ref Matrix4F matrix,
            Universe universe,
            bool rubberBanding,
            Heading cameraheading,
            AvatarController avatarController,
            Timestep renderTimestep,
            LodClient lodClient) {
            if (_context == null) {
                _context = graphics;
            }

            UIManager.Draw(graphics);
            V3.UI.UIManager.Draw(graphics);
        }
    }
}