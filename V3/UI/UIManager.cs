﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore.V2;
using NimbusFox.KitsuneCore.V2.UI;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using NimbusFox.KitsuneCore.V2.UI.SharpFont;
using Plukit.Base;
using SharpFont;
using Staxel;
using Staxel.Draw;

namespace NimbusFox.KitsuneCore.V3.UI {
    internal static class UIManager {
        private static readonly FontService FontService = new FontService();

        private static readonly Dictionary<string, UIWindow> Windows = new Dictionary<string, UIWindow>();

        private static readonly Dictionary<string, IUIElementBuilder> UIBuilders =
            new Dictionary<string, IUIElementBuilder>();

        internal static void Initialize() {
            foreach (var instance in Helpers.GetTypesUsingBase<IUIElementBuilder>()) {
                var builder = (IUIElementBuilder)Activator.CreateInstance(instance);

                UIBuilders.Add(builder.Kind, builder);
            }


            foreach (var uiFile in GameContext.AssetBundleManager.FindByExtension(".ksuiv3")) {
                var blob = BlobAllocator.Blob(true);

                blob.ReadJson(GameContext.ContentLoader.ReadStream(uiFile).ReadAllText());

                blob.HandleInherits(true);

                blob.HandleCustomInherits(true, "__elements");

                var window = new UIWindow();

                

                Windows.Add(blob.GetString("code"), window);

                Blob.Deallocate(ref blob);
            }
        }

        internal static void Update(GameTime gameTime, DeviceContext context) { }

        internal static void Draw(DeviceContext context) { }

        private static void HandleContainer(UIWindow window, UIElement element, Blob elementBlobs) {

        }

        public static bool TryGetWindow(string code, out UIWindow window) {
            window = null;

            if (!Windows.ContainsKey(code)) {
                return false;
            }

            window = Windows[code];
            return true;
        }

        public static bool TryFetchFont([NotNull] string fontName, out FontService fontService,
            float size = Constants.Fonts.DefaultSize) {
            return V2.UI.UIManager.TryFetchFont(fontName, out fontService, size);
        }

        public static bool TryFetchBackground(string code, out UIBackground background) {
            return V2.UI.UIManager.TryFetchBackground(code, out background);
        }

        public static bool TryFetchPicture(string resource, out UIPicture picture) {
            picture = null;

            try {
                picture = V2.UI.UIManager.FetchImage(resource);
                return true;
            } catch {
                return false;
            }
        }
    }
}
