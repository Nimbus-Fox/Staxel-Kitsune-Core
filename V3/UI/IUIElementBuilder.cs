﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V3.UI.Elements;
using Plukit.Base;

namespace NimbusFox.KitsuneCore.V3.UI {
    public interface IUIElementBuilder {
        string Kind { get; }

        UIElement BuildElement(UIWindow window, UIElement parent, Blob blob);
    }
}
